from django import forms
from django.contrib import admin
# from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField

from .models import Specialization, Subject, Group, User, Course, RecordBook

#
# class SubjectAdmin(admin.ModelAdmin):
#     filter_horizontal = ('subject',)


class UserCreationForm(forms.ModelForm):
    #A form for creating new users. Includes all the required
    #fields, plus a repeated password.
    password = forms.CharField(label='Password', widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ('email', 'password','user_type','first_name','second_name','middle_name','gender','education',)

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])
        if commit:
            user.save()
        return user


class UserChangeForm(forms.ModelForm):
    #A form for updating users. Includes all the fields on
    #the user, but replaces the password field with admin's
    #password hash display field.

    password = ReadOnlyPasswordHashField()

    class Meta:
        model = User
        fields = ('email', 'password','user_type','first_name','second_name','middle_name','avatar','gender','birthday','education','phone_number','group','job_position','active','staff','admin','superuser',)

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]


class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('email', 'active', 'staff')
    list_filter = ('user_type','group')
    fieldsets = (
        (None, {'fields': ('email', 'password', 'user_type','first_name','second_name','middle_name','avatar','gender','birthday','education','phone_number','group','job_position','active',)}),
        ('Permissions', {'fields': ('staff','admin','superuser',)}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password','user_type','first_name','second_name','middle_name','avatar','gender','birthday','education','phone_number','group','job_position','active','staff','admin','superuser',)
                }
         ),
    )
    search_fields = ('email',)
    ordering = ('email',)
    filter_horizontal = ()


# Меняем заголовки админки
admin.site.site_header = "Администрирование"
admin.site.site_title = "Страница администрирования"

# Register your models here.
admin.site.register(Specialization)
admin.site.register(Subject)
admin.site.register(Course)
admin.site.register(Group)
admin.site.register(RecordBook)
admin.site.register(User, UserAdmin)
# admin.site.register(User, SubjectAdmin)
