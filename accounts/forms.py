from django.db import models
from django import forms

from accounts.models import *
from .CustomAuthBackend import MyAuthBackend


# Класс полей формы добавления нового студента
class StudentRegisterForm(forms.ModelForm):
    first_name = forms.CharField(label='Имя')
    second_name = forms.CharField(label='Фамилия')
    middle_name = forms.CharField(label='Отчество')
    email = forms.EmailField(label='Электронная почта', help_text='example@mail.com')
    password = forms.CharField(label='Пароль', widget=forms.PasswordInput())
    group = forms.ModelChoiceField(
        queryset=Group.objects.all(),
        label='Группа',
        empty_label="Выберите группу"
    )

    # Определение стилей для виджетов (элментов форм)
    first_name.widget.attrs.update({'class': 'form-control'})
    second_name.widget.attrs.update({'class': 'form-control'})
    middle_name.widget.attrs.update({'class': 'form-control'})
    email.widget.attrs.update({'class': 'form-control'})
    password.widget.attrs.update({'class': 'form-control'})

    group.widget.attrs.update({'class': 'form-control'})
    group.widget.attrs.update({'id': 'register-group-dropdown'})

    class Meta:
        model = User
        fields = ['first_name', 'second_name', 'middle_name', 'email', 'password', 'group']

    # переопределение метода решистрации пользователя
    def save(self, commit=True):
        user = super(StudentRegisterForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password'])
        if commit:
            user.save()
        return user


# Форма авторизации пользователя
class UserAuthForm(forms.ModelForm):
    email = forms.EmailField(label='Электронная почта', help_text='example@mail.com')
    password = forms.CharField(label='Пароль', widget=forms.PasswordInput())

    email.widget.attrs.update({'class': 'form-control'})
    password.widget.attrs.update({'class': 'form-control'})

    class Meta:
        model = User
        fields = ['email', 'password']

    def clean(self, *args, **kwargs):
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')

        if email and password:
            user = MyAuthBackend.authenticate(self, email=email, password=password)
            if not user:
                raise forms.ValidationError('Пользователя не существует')
            if not user.is_active:
                raise forms.ValidationError('Этот пользователь заблокирован')
            return super(UserAuthForm, self).clean(*args, **kwargs)


# Форма редактирования ФИО
class FullnameEditAccountForm(forms.ModelForm):
    second_name = forms.CharField(label="Фамилия")
    first_name = forms.CharField(label="Имя")
    middle_name = forms.CharField(label="Отчество")

    # Запрещаем редактирование
    second_name.widget.attrs['readonly'] = True
    first_name.widget.attrs['readonly'] = True
    middle_name.widget.attrs['readonly'] = True

    # устанавливаем стили полям
    second_name.widget.attrs.update({'class': 'form-control'})
    first_name.widget.attrs.update({'class': 'form-control'})
    middle_name.widget.attrs.update({'class': 'form-control'})

    class Meta:
        model = User
        fields = ['second_name', 'first_name', 'middle_name']


# Форма редактирования электронной почты
class EmailEditAccountForm(forms.ModelForm):
    email = forms.EmailField(label='Ваша почта')

    # устанавливаем стили полям
    email.widget.attrs.update({'class': 'form-control'})

    class Meta:
        model = User
        fields = ['email']


# Форма редактирования пароля
class PasswordEditAccountForm(forms.ModelForm):
    password = forms.CharField(label='Старый пароль', widget=forms.PasswordInput())
    password_new1 = forms.CharField(label='Новый пароль', widget=forms.PasswordInput())
    password_new2 = forms.CharField(label='Повторите пароль', widget=forms.PasswordInput())

    # устанавливаем стили полям
    password.widget.attrs.update({'class': 'form-control'})
    password_new1.widget.attrs.update({'class': 'form-control'})
    password_new2.widget.attrs.update({'class': 'form-control'})

    class Meta:
        model = User
        fields = ['password']

    def clean(self):
        password = self.cleaned_data.get('password')
        password_new1=self.cleaned_data.get('password_new1')
        password_new2=self.cleaned_data.get('password_new2')
        #similarly old_password
        if password_new1 and password_new2 and password_new1!=password_new2 or password_new1==password:
            #raise error
            raise forms.ValidationError('Пароль введен не верно')
        else:
            current_user = User.objects.filter(id=self.instance.id).first() # НЕ ТРОГАТЬ!
            current_password = current_user.password

            if password != current_password and password_new1 != current_password:
                current_user = super(PasswordEditAccountForm, self).save(commit=False)
                print(f"current_password: {current_password}")
                current_user.set_password(self.cleaned_data['password_new1'])
                print(f"current_user: {current_user}")
                print(f"current_user.password: {current_user.password}")
                current_user.save()
                return self.cleaned_data

        # get the user object and check from old_password list if any one matches with the new password raise error(read whole answer you would know)
        return self.cleaned_data #don't forget this.
