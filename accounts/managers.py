from . import models
from django.db import models
from .models import *
from django.contrib.auth.base_user import BaseUserManager

from datetime import datetime
from calendar import monthrange
from datetime import datetime, timedelta


class UserManager(BaseUserManager):
    """
    Менеджер управления пользователями
    """
    use_in_migrations = True

    # Создает нового пользователя при регистрации
    def create_user(self, email, first_name, second_name, middle_name, user_type, gender, birthday, education,
                    phone_number,  # subject=None,
                    group=None, password=None, job_position=None,
                    data_joined=datetime.now(), last_login=None, is_active=True, is_staff=False, is_admin=False,
                    is_superuser=False, commit=True,
                    **extra_fields):
        if not email:
            raise ValueError('Укажите электронную почту')
        if not first_name:
            raise ValueError('Укажите ваше имя')
        if not second_name:
            raise ValueError('Укажите вашу фамилию')
        if not middle_name:
            raise ValueError('Укажите отчество')

        # print(type(subject))
        # print(f"type(subject) == None: {subject is None}")
        # if subject is None:
        #     subject=None
        # else:
        #     subject=subject.set()

        # приводим данные в нормальный вид для БД
        # Создаем нового пользователя в БД
        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            second_name=second_name,
            middle_name=middle_name,
            user_type=user_type,
            gender=gender,
            birthday=birthday,
            education=education,
            phone_number=phone_number,
            # технические поля
            data_joined=data_joined,
            last_login=last_login,
            # Поля студента
            group=group,
            # Поля преподавателя
            job_position=job_position,
            # subject=subject
        )
        # Шифруем пароль для пользователя
        user.set_password(password)
        user.active = is_active
        user.staff = is_staff
        user.admin = is_admin
        user.superuser = is_superuser
        # Сохраняем нормализованные данные в БД
        if commit:
            user.save(using=self._db)
        return user

    def create_studentuser(self, email, first_name, second_name, middle_name, gender, birthday, user_type, education,
                           phone_number, group, job_position, password=None,
                           active=True, staff=False, admin=False, superuser=False, **extra_fields):
        user = self.create_user(
            email,
            first_name,
            second_name,
            middle_name,
            user_type,
            password=password,
            gender=gender,
            birthday=birthday,
            education=education,
            phone_number=phone_number,
            # Поля студента
            group=group,
            # Поля преподавателя
            job_position=job_position,
        )
        return user

    def create_educatoruser(self, email, first_name, second_name, middle_name, gender, birthday, user_type, education,
                            phone_number, group, job_position, password=None, **extra_fields):
        user = self.create_user(
            email,
            first_name,
            second_name,
            middle_name,
            user_type,
            password=password,
            gender=gender,
            birthday=birthday,
            education=education,
            phone_number=phone_number,
            # Поля преподавателя
            job_position=job_position,
        )
        user.staff = True
        return user

    def create_superuser(self, email, password=None, **extra_fields):
        user = self.create_user(
            email,
            password=password,
            user_type=2,
            first_name='Admin',
            second_name='Admin',
            middle_name='Admin',
            gender='other',
            birthday=None,
            education=None,
            phone_number='',
            commit=False
        )
        user.set_password(password)
        user.staff = True
        user.admin = True
        user.superuser = True
        # Сохраняем нормализованные данные в БД
        user.save(using=self._db)
        return user


class SubjectsManager(models.Manager):
    """
    Данный менеджер предоставляет набор рабочих запросов системы, отображающий
    логику запросов предметов для системы
    """

    def subjects_for_user(self, user):
        """
        Возвращает список предметов, которые ведет пользователь
        :param user:
        :return:
        """
        return self.filter(lector=user)

    def subjects_for_group(self, group):
        """
        Запрашиваем предметы, которые ведутся для конкретной группы
        :param group:
        :return:
        """
        # Узнаем, какой у них год обучения
        date_enrollment = group.course_id.date_enrollment
        specialization_model = group.course_id.specialization_id

        # Указываем годовой предел искомого диапозона
        today = datetime.today().date()
        course_studying = 1

        # получаем на каком сейчас курсе обучения находится поток
        if today.month < 9:
            # в первом семестре уменьшаем поисковый диапазон даты на единицу
            course_studying = today.year - date_enrollment.year + 1
        else:
            course_studying = today.year - date_enrollment.year

        # Получаем предметы, которые читаются для данного курса, по данной специальности
        return self.filter(specialization=specialization_model, course_of_teaching=course_studying)
