from calendar import monthrange
from datetime import timedelta

from django.db import models
from django.core.validators import RegexValidator

from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.utils.datetime_safe import datetime
from django.utils.translation import gettext_lazy as _

# Пользовательские вызовы
from .choices import *
from .managers import UserManager, SubjectsManager


"""
Менеджеры создания продвинутых запросов к моделям
"""


class CoursesManager(models.Manager):
    """
    Данный менеджер предоставляет набор рабочих запросов системы, отображающий
    логику его запросов
    """

    def current_courses(self):
        """
        Возвращает набор курсов, которые обучаются в текущий момент
        """

        # Указываем годовой предел искомого диапозона
        today = datetime.today().date()
        last_date = datetime.today().date()
        last_date = last_date.replace(day=1, month=9, year=today.year)

        # Проверяем дату, когда производится выборка. Она будет влиять на то, будет ли
        # вычиляться год с поправкой на год выборки или нет.
        if today.month < 9:
            # в первом семестре уменьшаем поисковый диапазон даты на единицу
            first_date = today.replace(day=1, month=9, year=today.year - 13)
        else:
            first_date = today.replace(day=1, month=9, year=today.year - 12)

        # Сама выборка курсов
        return self.filter(date_enrollment__range=(first_date, last_date)).order_by('date_enrollment')

    def actual_course_by_number(self, course_number: int):
        """
        Возвращате курс, который сответствует номеру актуального курса,
        который обучается в данный момент. Не зависимо от того, к какой специализации
        относится он.

        :param course_number:
        :param actual:
        :return:
        """

        # Указываем годовой предел искомого диапозона
        today = datetime.today().date()
        last_date = datetime.today().date()
        last_date = last_date.replace(day=1, month=9, year=today.year)

        # проверяем какой сейчас идет год и на основании этого либо делаем поправку, либо нет
        if today.month >= 9:
            # делаем поправку
            first_date = today.replace(day=1, month=9, year=today.year - course_number - 1)
        else:
            # не делаем поправку
            first_date = today.replace(day=1, month=9, year=today.year - course_number)

        # Запрашиваем необходимый курс
        return self.filter(date_enrollment=first_date).order_by('date_enrollment')

    def actual_courses_for_user(self, user):
        """
        Возвращает актуальные курсы, которые находятся на обучении и им преподает
        переданный пользователь

        :param user:
        :return: []
        """

        # Собираем предметы, которые ведет пользователь
        subjects = Subject.objects.subjects_for_user(user=user)

        courses = self.none()
        # хранит набор Querysetов, которые будут возвращены пользователю
        if len(subjects) > 0:
            # Получаем набор курсов, которые он ведет вместе со специализациям
            for subject in subjects:
                specialization = subject.specialization_id
                founded_courses = self.actual_course_by_number(subject.course_of_teaching)
                print(f"founded_courses: {founded_courses}")

                # отсеиваем те, которые будут подходить по специализации
                # берем один, т.к. поток создается один на год
                courses = (courses | founded_courses.filter(specialization_id=specialization))

        return courses

"""
Модели, которые используются в БД
"""


# Модель БД Специализации
class Specialization(models.Model):
    # Код специальности
    specialty_code = models.CharField(
        verbose_name='Номер специальности',
        max_length=50,
        unique=True
    )
    # название специальности
    specialty_name = models.CharField(
        verbose_name='Наименование специальности',
        max_length=100,
        unique=True
    )
    # TODO: обговороить уровень квалификации

    class Meta:
        # Читабельное имя для модели
        verbose_name = "Специализация"
        verbose_name_plural = 'Специализации'
        # Сортировка по умолчанию
        ordering = ['specialty_name']

    def first_letters(self):
        """
        Возвращает первые буквы специализации
        :return:
        """
        name = self.specialty_name.split()
        acronym = ""
        for n in name:
            acronym += n[0].upper()
        return acronym

    def __str__(self):
        return self.specialty_name


# Хранится дата набора курса
class Course(models.Model):
    # Год набора курса
    date_enrollment = models.DateField()
    # Ссылка на специализацию созданного курса
    specialization_id = models.ForeignKey(
        Specialization,
        verbose_name='Специализация',
        on_delete=models.CASCADE
    )

    # Менеджер управления курсами
    objects = CoursesManager()

    class Meta:
        # Читабельное имя для модели
        verbose_name = "Курс"
        verbose_name_plural = 'Курсы'
        # Сортировка по умолчанию
        ordering = ['date_enrollment']

    def on_course_now(self):
        """
        Возвращает информацию о том, на каком курсе, неходится данный поток
        :return:
        """
        # Указываем годовой предел искомого диапозона
        today = datetime.today().date()
        current_course = 1

        # проверяем какой сейчас идет год и на основании этого либо делаем поправку, либо нет
        if today.month >= 9:
            # делаем поправку
            current_course = today.year - self.date_enrollment.year + 1
        else:
            # не делаем поправку
            current_course = today.year - self.date_enrollment.year
        return current_course

    def __str__(self):
        return self.specialization_id.specialty_name[0] + str(self.date_enrollment.year - 2000)


# Модель БД группы
class Group(models.Model):
    # номер группы
    number = models.IntegerField(
        verbose_name='Номер группы'
    )
    # Дата набора группы
    date_of_creation = models.DateField(
        auto_now=True,
        verbose_name='Дата набора группы'
    )
    # связь с курсом
    course_id = models.ForeignKey(
        Course,
        on_delete=models.DO_NOTHING,
        verbose_name='Поток'
    )

    class Meta:
        # Читабельное имя для модели
        verbose_name = "Группа"
        verbose_name_plural = 'Группы'
        # Сортировка по умолчанию
        ordering = ['date_of_creation']

    def only_course(self):
        """
        Возвращает только курс, без номера группы
        :return:
        """
        return str(self.course_id)

    def __str__(self):
        return str(self.course_id) + "." + str(self.number)


# Модель пользователей системы
class User(AbstractBaseUser, PermissionsMixin):
    """
                    Общая часть
    Общая часть данных которая удовлетворяет требования к
    студентам, администрации и преподавателям
    """

    # Определение роли пользователя
    user_type = models.IntegerField(
        verbose_name='Роль в системе',
        choices=USER_TYPE_ROLES,
        default=0
    )
    email = models.EmailField(
        verbose_name='Электронная почта',
        max_length=60,
        unique=True
    )
    first_name = models.CharField(
        verbose_name='Имя',
        max_length=50
    )
    second_name = models.CharField(
        verbose_name='Фамилия',
        max_length=50
    )
    middle_name = models.CharField(
        verbose_name='Отчество',
        max_length=50
    )
    avatar = models.ImageField(
        upload_to='images/users',
        verbose_name='Аватарка пользователя',
        null=True,
        blank=True
    )
    gender = models.CharField(
        verbose_name='Пол',
        max_length=20,
        choices=SEX_LIST,
        default=0
    )
    # День рождения
    birthday = models.DateField(
        verbose_name='День рождения',
        null=True,
        blank=True
    )
    # Уровень образования
    education = models.IntegerField(
        verbose_name='Образование',
        choices=EDUCATION_LEVEL,
        null=True,
        default=2
    )
    # Номер телефона
    phone_regex = RegexValidator(
        regex=r'^\+?380?\d{10,12}$',
        message="Номер телефона должен быть в формате +380YYXXXXXXX. Содержать 12 символов"
    )
    phone_number = models.CharField(
        verbose_name='Контактный номер',
        validators=[phone_regex],
        max_length=12,
        blank=True
    )
    # Дата регистрации участника
    data_joined = models.DateTimeField(
        verbose_name='Дата регистрации',
        auto_now_add=True
    )
    # Дата последней авторизации в системе
    last_login = models.DateField(
        verbose_name='Дата последней авторизации',
        auto_now=True
    )

    '''
    Студенческая часть
    '''

    # Номер группы в которой находится студент
    group = models.ForeignKey(
        Group,
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )

    '''
                Преподавательская часть
    '''

    # Должность в заведении
    job_position = models.CharField(
        verbose_name='Должность',
        max_length=90,
        null=True,
        blank=True
    )

    '''
                   Техническая часть
    '''

    # стандартные состояния
    active = models.BooleanField(
        _('активный'),
        default=True,
        help_text=_(
            'Designates whether this user should be treated as active. '
            'Unselect this instead of deleting accounts.'
        ),
    )
    staff = models.BooleanField(
        _('персонал'),
        default=False,
        help_text=_(
            'Designates whether the user can log into this admin site.'
        ),
    )
    admin = models.BooleanField(
        _('админ'),
        default=False,
        help_text=_(
            'Designates whether the user can log into this admin site.'
        ),
    )
    superuser = models.BooleanField(
        _('суперпользователь'),
        default=False,
        help_text=_(
            'Designates whether the user can log into this admin site.'
        ),
    )

    # Определеяем идентификатор модели
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    # менеджер управление пользователями
    objects = UserManager()

    class Meta:
        # Читабельное имя для модели
        verbose_name = "Пользователь"
        verbose_name_plural = 'Пользователи'
        # Сортировка по умолчанию
        ordering = ['phone_number', 'second_name', 'first_name']

    def __str__(self):
        return self.get_full_name()

    def full_first_small_other(self):
        """
        Возвращает строку ФИО, в формате Фамилия И.О
        :return:
        """
        return self.second_name + ' ' + self.first_name[0] + '.' + self.middle_name[0]

    def name_first_letter(self):
        return self.first_name[0]

    def get_short_name(self):
        return self.first_name + ' ' + self.second_name

    def get_full_name(self):
        return self.second_name + ' ' + self.first_name + ' ' + self.middle_name

    def get_readable_user_type(self):
        result = ''
        if self.user_type == 0:
            result = 'Студент'
        elif self.user_type == 1:
            result = 'Преподаватель'
        else:
            result = 'Администратор'
        return result
    
    def get_current_course(self):
        """
        Возвращает курс, на котором сейчас обучается студент
        :return:
        """
        return self.group.course_id.on_course_now()

    def get_current_semestr(self):
        """
        Получаем семестр, на котором находится пользователь
        :return:
        """
        today = datetime.today().date()
        course_date_enrollment = self.group.course_id.date_enrollment

        semestr = 1
        while True:
            mdays = monthrange(course_date_enrollment.year, course_date_enrollment.month)[1]
            course_date_enrollment += timedelta(days=mdays)
            if course_date_enrollment.month == 9 or course_date_enrollment.month == 1:
                semestr += 1
            elif course_date_enrollment >= today:
                break
        return semestr

    def has_perm(self, perm, obj=None):
        return self.is_admin

    #TODO: Разобраться с этим
    def has_module_perms(self, app_label):
        return True

    @property
    def is_active(self):
        return self.active

    @property
    def is_staff(self):
        return self.staff

    @property
    def is_admin(self):
        return self.admin

    @property
    def is_superuser(self):
        return self.superuser

    @property
    def is_local_admin(self):
        """
        Метод, который возвращает состояние того, является ли пользователь администратором учебного заведения или нет
        :return:
        """
        if self.user_type == 2 or self.user_type == "Администратор":
            return True
        return False

    @property
    def is_lecturer(self):
        """
        Метод, который возвращает состояние того, является ли пользователь преподавателем или нет
        :return:
        """
        if self.user_type == 1 or self.user_type == "Преподаватель":
            return True
        return False

    @property
    def is_student(self):
        """
        Метод, который возвращает состояние того, является ли пользователь студентом или нет
        :return:
        """
        if self.user_type == 0 or self.user_type == "Студент":
            return True
        return False


# Модель предметов которые ведут
class Subject(models.Model):
    # Название предмета
    name = models.CharField(
        verbose_name='Наименование',
        max_length=50,
        null=False
    )
    # Связь со специализацией
    specialization = models.ForeignKey(
        Specialization,
        verbose_name='Специализация',
        on_delete=models.DO_NOTHING
    )
    # Преподаватель который читает
    lector = models.ForeignKey(
        User,
        verbose_name='Ведет',
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True
    )
    # Семестр в котором читается
    semestr_academic = models.IntegerField(
        verbose_name='Семестр чтения предмета'
    )
    # Курс на котором читается
    course_of_teaching = models.IntegerField(
        verbose_name='Курс на котором читается',
        choices=LEGAL_COURSES,
        default=1
    )

    objects = SubjectsManager()

    class Meta:
        # Читабельное имя для модели
        verbose_name = "Предмет"
        verbose_name_plural = 'Предметы'
        # Сортировка по умолчанию
        ordering = ['name']

    def __str__(self):
        return self.name


# Записная книжка студента
class RecordBook(models.Model):
    # ссылка на студента
    student = models.ForeignKey(
        User,
        on_delete=models.DO_NOTHING,
        null=True,
        blank=True,
        verbose_name='Владелец зачетки'
    )
    # номер зачетной книжки (год_зачисления + id_студента)
    number = models.IntegerField(
        verbose_name="Номер зачетной книжки"
    )

    # ссылка на специальность
    specialization = models.ForeignKey(
        Specialization,
        on_delete=models.DO_NOTHING,
        verbose_name='Специальность'
    )
    # Дата выдачи
    issue_date = models.DateField(auto_now=True)

    class Meta:
        # Читабельное имя для модели
        verbose_name = "Зачетная книжка"
        verbose_name_plural = 'Зачетные книжки'

    def __str__(self):
        return str(self.number)
