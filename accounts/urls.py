from django.conf.urls import url
from django.contrib.auth import views
from django.contrib.auth.urls import *
from django.urls import path

from .views import *

urlpatterns = [
    path('register', StudentCreate.as_view(), name='register'),
    url(r'^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        activate_account, name='activate'),
    path('login', LoginUserView.as_view(), name='login'),
    path('logout', views.LogoutView.as_view(), name='logout'),
    path('settings/', edit_user_settings, name='settings'),
    path('marks/', get_marks, name='marks'),
]
