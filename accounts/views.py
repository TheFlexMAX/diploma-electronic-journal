from django.conf import settings
from django.contrib import messages
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render
from django.shortcuts import redirect
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.views.generic import View
from accounts.models import Group
from django.contrib.auth import (authenticate, login, logout, get_user_model, update_session_auth_hash)
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse

# Методы для отправки почты
from django.core.mail import send_mail
from django.core.mail import EmailMultiAlternatives
from django.core.mail import EmailMessage
from django.template.loader import get_template
from django.template import Context

from .forms import (
    StudentRegisterForm,
    UserAuthForm,
    FullnameEditAccountForm,
    EmailEditAccountForm,
    PasswordEditAccountForm
)
from .models import User
from ejournal.views import get_journal, get_welcome

from .token_generator import account_activation_token


# Create your views here.

# метод для получение данных о текущем пользователе системы
# current_user = get_user_model()

# Логика обработки формы создания нового студента из формы


#                       регистрация
class StudentCreate(View):
    # Запрос формы и страницы регистрации
    def get(self, request):
        form = StudentRegisterForm()
        return render(request, 'register.html', context={'form': form})

    # Обработка данных отправленных пользователем
    def post(self, request):
        bound_form = StudentRegisterForm(request.POST)
        if bound_form.is_valid():
            new_student = bound_form.save()
            # часть отвечающая за отправку токена подтверждения регистрации
            # new_student.active = False
            # new_student.save()
            current_site = get_current_site(request)
            email_subject = 'Активация Вашего аккаунта'
            # message = render_to_string('email/email_reg.html', {
            #     'user': new_student,
            #     'domain': current_site.domain,
            #     'uid': urlsafe_base64_encode(force_bytes(new_student.pk)),
            #     'token': account_activation_token.make_token(new_student),
            # })
            to_email = bound_form.cleaned_data.get('email')
            from_email = settings.EMAIL_HOST_USER
            # send_mail(subject=email_subject,
            #           from_email=from_email,
            #           recipient_list=[to_email],
            #           message=message,
            #           html_message=message,
            #           fail_silently=False)

            data = {
                'user': new_student,
                'domain': current_site.domain,
                'uid': urlsafe_base64_encode(force_bytes(new_student.pk)),
                'token': account_activation_token.make_token(new_student),
            }
            message = get_template('email/email_reg.html').render(data)
            msg = EmailMessage(
                email_subject,
                message,
                from_email,
                [to_email]
            )
            msg.content_subtype = 'html'
            msg.send()
            return render(request, 'email_send_success.html', {})
        return render(request, 'register.html', context={'form': bound_form})


def activate_account(request, uidb64, token):
    """ Функция активации зарегистрированного акаунта пользователм """
    try:
        uid = force_bytes(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.active = True
        user.save()
        login(request, user, backend='django.contrib.auth.backends.ModelBackend')  # Не трожь, КОСТЫЛИ
        return render(request, 'email_activate_success.html', {})
    else:
        return HttpResponse('Токен активации не верен!')


# Авторизация
class LoginUserView(View):
    def get(self, request):
        form = UserAuthForm()
        return render(request, 'auth.html', context={'form': form})

    # low level but, using AuthenticationForm.clean for authentication
    def post(self, request):
        form = UserAuthForm(request.POST or None)
        email = request.POST['email']
        password = request.POST['password']
        user = authenticate(request, email=email, password=password)
        if user is not None:
            form = login(request, user)
            return redirect(get_welcome)

        return render(request, 'auth.html', {'form': form})
    # if form.is_valid():
    #     try:
    #         email = form.cleaned_data.get('email')
    #         password = form.cleaned_data.get('password')
    #         user = authenticate(email=email, password=password)
    #         if user is not None:
    #             if user.is_active:
    #                 login(request, user)
    #                 return HttpResponse('Успешно залогинился :)')
    #
    #     except ValidationError:
    #         return render(
    #             request,
    #             'login.html',
    #             { 'form': form, 'invalid_creds': True }
    #         )
    #
    #     return redirect(get_journal)
    #
    # return render(request, 'auth.html', { 'form': form })


def get_settings(request):
    return render(request, 'settings.html', {})


def get_marks(request):
    return render(request, 'marks.html', {})


def get_email_success(request):
    """
    Возвращает страницу успешной отправки письма
    :param request:
    :return:
    """
    return render(request, 'email_send_success.html', {})


def get_email_activated(request):
    """
    Возвращает страницу успешной активации акаунта
    :param request:
    :return:
    """
    return render(request, 'email_activate_success.html', {})


# Страница редактирования профиля пользователя
@login_required
def edit_user_settings(request):
    # Объявляем переменные форм
    fullname_form = None
    email_form = None
    password_form = None

    # Обработка смены электронной почты
    if request.method == 'POST' and "changeEmail" in request.POST:
        email_form = EmailEditAccountForm(instance=request.user,
                                          data=request.POST)
        password_form = PasswordEditAccountForm(instance=request.user)
        fullname_form = FullnameEditAccountForm(instance=request.user)

        if email_form.is_valid():
            email_form.save()
            messages.success(request, 'Email успешно изменен')
        else:
            messages.error(request, 'Email сменить не удалось')

    # Обработка смены пароля
    elif request.method == 'POST' and "changePassword" in request.POST:
        password_form = PasswordEditAccountForm(instance=request.user,
                                                data=request.POST)
        email_form = EmailEditAccountForm(instance=request.user)
        fullname_form = FullnameEditAccountForm(instance=request.user)

        # проверяем валидность пароля
        if password_form.is_valid():
            password_form.save()
            update_session_auth_hash(request, password_form.instance)

            messages.success(request, 'Пароль успешно изменен')
        elif password_form.errors:
            print(password_form.errors)
        else:
            messages.error(request, 'Пароль сменить не удалось')

    else:
        fullname_form = FullnameEditAccountForm(instance=request.user)
        email_form = EmailEditAccountForm(instance=request.user)
        password_form = PasswordEditAccountForm(instance=request.user)

    return render(request,
                  'settings.html',
                  {'fullname_form': fullname_form,
                   'email_form': email_form,
                   'password_form': password_form})

    # really low level
    # def post(self, request):
    #     form = AuthenticationForm(request, data=request.POST)
    #     if form.is_valid():
    #         user = authenticate(
    #             request,
    #             username=form.cleaned_data.get('username'),
    #             password=form.cleaned_data.get('password')
    #         )
    #
    #         if user is None:
    #             return render(
    #                 request,
    #                 'survey/login.html',
    #                 { 'form': form, 'invalid_creds': True }
    #             )
    #
    #         try:
    #             form.confirm_login_allowed(user)
    #         except ValidationError:
    #             return render(
    #                 request,
    #                 'survey/login.html',
    #                 { 'form': form, 'invalid_creds': True }
    #             )
    #         login(request, user)
    #
    #         return redirect(reverse('profile'))
    #
    #     return render(request, 'survey/login.html', { 'form': form })

# Альтернативный вариант логики авторизации пользователя
# def login_page(request):
#     form = LoginForm(request.POST or None)
#     context = {
#         "form": form
#     }
#     next_ = request.GET.get('next')
#     next_post = request.POST.get('next')
#     redirect_path = next_ or next_post or None
#     if form.is_valid():
#         username  = form.cleaned_data.get("username")
#         password  = form.cleaned_data.get("password")
#         user = authenticate(request, username=username, password=password)
#         if user is not None:
#             login(request, user)
#             try:
#                 del request.session['guest_email_id']
#             except:
#                 pass
#             if is_safe_url(redirect_path, request.get_host()):
#                 return redirect(redirect_path)
#             else:
#                 return redirect("/")
#         else:
#             # Return an 'invalid login' error message.
#             print("Error")
#     return render(request, "accounts/login.html", context)

# class UserAuth(View):
#     def get(self, request):
#         form = UserAuthForm()
#         return render(request, 'auth.html', context={'form': form})
