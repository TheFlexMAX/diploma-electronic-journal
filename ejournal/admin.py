from django.contrib import admin
from .models import Mark, Attendance, Topic, Control_Type, RequiredJournal


# Меняем заголовки админки
admin.site.site_header = "Администрирование"
admin.site.site_title = "Страница администрирования"

# Register your models here.
admin.site.register(Mark)
admin.site.register(Attendance)
admin.site.register(Topic)
admin.site.register(Control_Type)
admin.site.register(RequiredJournal)
