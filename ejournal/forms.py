from django.db import models
from django import forms
from django.core.exceptions import ObjectDoesNotExist

from accounts.models import Subject, Course, Group
from django.forms import widgets

from .models import *

from .choices import *


class ModelChoiceFieldNoValidation(forms.ModelChoiceField):
    """
    Виджет, созданный для того, чтобы игнорировать поля на валидацию
    """
    def validate(self, value):
        pass


def no_validate(value):
    """
    Пропускает всё, что угодно для виджета
    :param value:
    :return:
    """
    pass


class StatementsFinalByCourse(forms.Form):
    """
    Форма для полученния сводной оценки по курсу
    """
    # Позволяет выбрать курс, которому создается таблица
    course_number = forms.ChoiceField(
        label="Курс",
        choices=COURSES_CHOICES
    )
    course_number.widget.attrs.update({'class': 'form-control'})
    course_number.widget.attrs.update({'id': 'state_2_form_1'})

    # Позволяет выбрать, за какой семестр выбирать оценки
    semestr_half = forms.ChoiceField(
        label="Половина семестра",
        choices=(
            ("first", "Первая"),
            ("second", "Вторая"),
        )
    )
    semestr_half.widget.attrs.update({'class': 'form-control'})
    semestr_half.widget.attrs.update({'id': 'state_2_form_2'})


class StatementsFinalBySpec(forms.Form):
    """
    Форма для полученния сводной оценки по специализации
    """
    # Позволяет выбрать курс, которому создается таблица
    course_number = forms.ChoiceField(
        label="Курс",
        choices=COURSES_CHOICES
    )
    course_number.widget.attrs.update({'class': 'form-control'})
    course_number.widget.attrs.update({'id': 'state_2_form_1'})

    # Позволяет выбрать, за какой семестр выбирать оценки
    semestr_half = forms.ChoiceField(
        label="Половина семестра",
        choices=(
            ("first", "Первая"),
            ("second", "Вторая"),
        )
    )
    semestr_half.widget.attrs.update({'class': 'form-control'})
    semestr_half.widget.attrs.update({'id': 'state_2_form_2'})


class StatementsCoursesExam(forms.Form):
    """
    Обрабатывает форму запроса генерации таблицы проведения экзаменов для курса
    """
    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user', None)
        super(StatementsCoursesExam, self).__init__(*args, **kwargs)
        # делаем запросы с параметром, которые невозможно сделать ниже
        if self.user:
            self.fields['course_number'].queryset = Course.objects.actual_courses_for_user(user=self.user)
            course_id = self.fields['course_number'].initial or self.initial.get('course_number')
            if course_id:
                groups = Group.objects.filter(course_id__id=course_id)
                self.fields['group'].queryset = groups

                # Получаем набор предметов для валидации
                group_id = self.fields['group'].initial or self.initial.get('group')
                if group_id:
                    group = Group.objects.get(id=group_id)
                    subjects = Subject.objects.subjects_for_group(group)
                    self.fields['subject'].queryset = subjects

    # Позволяет выбрать курс, которому создается таблица
    course_number = ModelChoiceFieldNoValidation(
        label="Курс",
        empty_label="Выберите курс",
        queryset=Course.objects.all(),
        validators=[no_validate]
    )
    course_number.widget.attrs.update({'class': 'form-control'})
    course_number.widget.attrs.update({'id': 'state_1_form_1'})

    # Позволяет выбрать группу, для которой будет генерироваться таблица
    group = ModelChoiceFieldNoValidation(
        label="Группа",
        empty_label="Выберите группу",
        queryset=Group.objects.all(),
        required=False,
        validators=[no_validate]
    )
    group.widget.attrs.update({'class': 'form-control'})
    group.widget.attrs.update({'id': 'state_1_form_2'})

    # В зависимости от выбора
    subject = ModelChoiceFieldNoValidation(
        widget=widgets.Select,
        label="Предмет",
        empty_label="Выберите предмет",
        queryset=Subject.objects.all(),
        validators=[no_validate]
    )
    subject.widget.attrs.update({'class': 'form-control'})
    subject.widget.attrs.update({'id': 'state_1_form_3'})

    # Позволяет выбрать, за какой семестр выбирать оценки
    semestr_half = forms.ChoiceField(
        label="Половина семестра",
        choices=(
            ("first", "Первая"),
            ("second", "Вторая"),
        )
    )
    semestr_half.widget.attrs.update({'class': 'form-control'})
    semestr_half.widget.attrs.update({'id': 'state_1_form_7'})

    # Позволяет выбать форму контроля (От этого параметра будет сильно
    # зависить выборка оценки)
    control_type = forms.ChoiceField(
        label="Тип контроля",
        choices=CONTROL_TYPES
    )
    control_type.widget.attrs.update({'class': 'form-control'})
    control_type.widget.attrs.update({'id': 'state_1_form_4'})

    # Строка списка экзаменаторов
    examiners = forms.CharField(
        label="Экзаменаторы",
        help_text="Введите экзаменаторов через запятую"
    )
    examiners.widget.attrs.update({'class': 'form-control'})
    examiners.widget.attrs.update({'id': 'state_1_form_5'})

    # Дата проведения экзамена\зачета
    event_date = forms.DateField(
        label="Дата проведення екзамену, заліку",
        help_text="ДД.ММ.ГГГГ",
        input_formats=('%d.%m.%Y', )
    )
    event_date.widget.attrs.update({'class': 'form-control datepicker-input'})
    event_date.widget.attrs.update({'id': 'state_1_form_6'})

    class Meta:
        """
        Добавляем исключение проверки полей, т.к. на момент 07.05.2020 не понятно как их правильно запршивать или
        валидировать
        #TODO: сделать валидацию этих полей
        """
        exclude = ('group', 'subject',)

    def clean_subject(self):
        """
        Определяет валидацию указанного в форме предмета
        :return:
        """
        data = self.cleaned_data.get("subject")
        try:
            sbj = Subject.objects.get(id=data.id)
        except ObjectDoesNotExist:
            raise forms.ValidationError('Указанного вами предмета не существует')
        return data

    def clean_group(self):
        """
        Определяет валидацию указанной в форме группы
        :return:
        """
        data = self.cleaned_data.get("group")
        try:
            sbj = Group.objects.get(id=data.id)
        except ObjectDoesNotExist:
            raise forms.ValidationError('Указанной вами группы не существует')
        return data

