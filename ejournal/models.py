from accounts.models import *
from django.db import models
from django.utils.timezone import now

from accounts.models import Course, Group, Subject

from datetime import date


# Create your models here.
# Хранятся модели отображающие таблицы БД

# Прикрепляется к теме когда необходимо
class Control_Type(models.Model):
    # Название контроля
    title = models.TextField(null=False)

    class Meta:
        # Читабельное имя для модели
        verbose_name = "Тип контроля"
        verbose_name_plural = 'Типы контроля'

    def __str__(self):
        return str(self.title)


# Модель хранящая тему лекции/оценки
class Topic(models.Model):
    # Предмет к которому привязали тему
    subject = models.ForeignKey(Subject, on_delete=models.DO_NOTHING, null=True)
    # Название темы
    title = models.TextField(null=False)
    # Описание темы
    description = models.TextField(null=True, blank=True)
    # Тема работ на проверку контроля
    control_type = models.ForeignKey(Control_Type, null=True, on_delete=models.DO_NOTHING)
    # Дата добавили тему, поумолчанию ставит сегодня
    date_added = models.DateTimeField(default=now)
    # Ссылка на то, к какому преподавателю принадлежит данная тема
    added_user = models.ForeignKey(User, on_delete=models.DO_NOTHING, null=True)
    # Ссылка какой группе будет читаться эта тема
    linked_group = models.ForeignKey(Group, on_delete=models.DO_NOTHING, null=True)
    # дата проведения работы
    date_provide = models.DateTimeField(default=now, null=True)
    # Запланированное занятие или нет
    is_scheduled = models.BooleanField(default=True)
    # проведенная работа или нет
    is_performed = models.BooleanField(null=False, default=False)


    class Meta:
        # Читабельное имя для модели
        verbose_name = "Тема"
        verbose_name_plural = 'Темы'
        # Сортировка по умолчанию
        ordering = ['date_added']

    def __str__(self):
        return str(self.title)


# Модель оценок которые получает студент
class Mark(models.Model):
    # Указывает на студента которому принадлжеит оценка
    user = models.ForeignKey(
        User,
        on_delete=models.CASCADE,
        verbose_name='Студент'
    )
    # Указывает на предмет на котором поставили оценку
    subject = models.ForeignKey(
        Subject,
        on_delete=models.DO_NOTHING,
        verbose_name='Предмет'
    )
    # Указывает по какой теме поставили оценку
    topic = models.ForeignKey(
        Topic,
        null=True,
        on_delete=models.DO_NOTHING,
        verbose_name='Тема'
    )
    # Список возможных оценок
    MARKS_LIST = (
        (1, '1'),
        (2, '2'),
        (3, '3'),
        (4, '4'),
        (5, '5')
    )
    # Укзывает оценку которую поставили студенту
    mark = models.IntegerField(
        choices=MARKS_LIST,
        default=1,
        null=True,
        verbose_name='Балл'
    )
    # Дата когда поставили оценку, поумолчанию ставит сегодня
    date_added = models.DateField(
        default=date.today,
        verbose_name='Дата проведения'
    )

    class Meta:
        # Читабельное имя для модели
        verbose_name = "Оценка"
        verbose_name_plural = 'Оценки'
        # Сортировка по умолчанию
        ordering = ['date_added']

    def national_identify(self):
        """
        Определяет критерий оценивания по национальному стандарту Украины на 06.05.2020
        :return:
        """
        m = self.mark
        if m >= 4.5:
            return "відмінно"
        elif 4.5 > m >= 3.5:
            return "добре"
        elif 3.5 > m >= 2.5:
            return "задовільно"
        elif 2.5 > m >= 2:
            return "незадовільно (з можливістю повторного складання)"
        else:
            return "незадовільно (з обов'язковим повторним вивченням)"

    def ects_identify(self):
        """
        Возвращает оценку по формату системы оценивания ECTS на момент 06.05.2020
        :return:
        """
        m = self.mark
        if m >= 4.5:
            return "A"
        elif 4.5 > m >= 4:
            return "B"
        elif 4 > m >= 3.5:
            return "C"
        elif 3.5 > m >= 3:
            return "D"
        elif 3 > m >= 2.5:
            return "E"
        elif 2.5 > m >= 2:
            return "FX"
        else:
            return "F"

    def __str__(self):
        return str(self.mark)


# Отвечает за сохранене информации о посещении
class Attendance(models.Model):
    date_added = models.DateTimeField(
        default=now,
        verbose_name='Дата проведения'
    )
    # Ссылка на студента
    student = models.ForeignKey(
        User,
        on_delete=models.DO_NOTHING,
        verbose_name='Студент'
    )
    # Ссылка на предемет
    subject = models.ForeignKey(
        Subject,
        on_delete=models.DO_NOTHING,
        verbose_name='Предмет'
    )
    # отвечает за значение был/не был
    presence = models.BooleanField(
        default=True,
        verbose_name='Присутствие на лекции'
    )

    class Meta:
        # Читабельное имя для модели
        verbose_name = "Посещение"
        verbose_name_plural = 'Посещения'
        # Сортировка по умолчанию
        ordering = ['date_added']

    def __str__(self):
        return str(self.student)


class RequiredJournal(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.DO_NOTHING,
        null=False,
        verbose_name='Преподаватель')
    course = models.ForeignKey(
        Course,
        on_delete=models.DO_NOTHING,
        null=False,
        verbose_name='Курс'
    )
    group = models.ForeignKey(
        Group,
        on_delete=models.DO_NOTHING,
        null=False,
        verbose_name='Группа'
    )
    subject = models.ForeignKey(
        Subject,
        on_delete=models.DO_NOTHING,
        null=False,
        verbose_name='Предмет'
    )
    date_request = models.DateTimeField(
        default=now,
        verbose_name='Дата запроса журнала'
    )

    def getCourseID(self):
        return self.course.id

    def getGroupID(self):
        return self.group.id

    def subjectID(self):
        return self.subject.id
