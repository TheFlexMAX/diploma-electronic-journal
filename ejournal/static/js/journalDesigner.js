/*
* Модальное окно
* */
// Get the modal
let modalPlanner = document.getElementById("modalTestPlanner");

// Get the <span> element that closes the modal
let spanPlanner = document.getElementsByClassName("close")[0];


// When the user clicks on <span> (x), close the modal
spanPlanner.onclick = function() {
    modalPlanner.style.display = "none";
    planner.clearTickets();
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modal) {
        modalPlanner.style.display = "none";
        planner.clearTickets();
    }
};

// Сокрытие модального окна при нажатии кнопки ESC
document.addEventListener('keydown', event => {
    if (event.key === 'Escape' || event.keyCode === 27) {
        modal.style.display = "none";
        planner.clearTickets();
    }
});


// Логическая работа модального окна конструктора журнала
class TestPlanner {
    constructor(subject, courseID = null, groupID = null) {
        this.currentSubject = subject;
        this.currentCourseID = courseID;
        this.currentGroupID = groupID;
        this.tickets = [];
        this.ticketsContainer = document.getElementById('testList-ticketContainer');
    }

    // глубинное копирование переданных тикетов
    set setTickets(extTickets) {
        let CurrentClass = this;
        extTickets.forEach(function (ticket) {
            CurrentClass.push(JSON.parse(JSON.stringify(ticket)));
        })
    }

    set courseID(extCourseID) {
        this.currentCourseID = extCourseID;
    }

    set groupID(extGroupID) {
        this.currentGroupID = extGroupID;
    }

    get getGroupID() {
        return this.currentGroupID;
    }

    get getCourseID() {
        return this.currentCourseID;
    }

    get getSubjectID() {
        return this.currentSubject.pk;
    }

    getTicketByID(id) {
        this.tickets.forEach(function (ticket) {
            if (ticket.topic_pk == id) {
                return ticket;
            }
        })
    }

    // Удаляет один тикет по индентификатору
    deleteTicketByID(id) {
        let index = 0;
        this.tickets.forEach(function (ticket, i) {
            if (ticket.topic_pk == id) {
                index = i;
            }
        });
        console.log('До удаления элемента');
        console.log('this.tickets');
        console.log(this.tickets);
        this.tickets.splice(index, 1);
        console.log('После удаления элемента');
        console.log('this.tickets');
        console.log(this.tickets);
    }

    // Запрашивает запланированные работы из back-end
    requireTickets(subjectID) {
        if (this.currentGroupID != null) {
            let CurrentClass = this;
            console.log('this.currentSubject.pk');
            console.log(this.currentSubject.pk);
            $.ajax({
                url: 'ajax/require_tickets/',
                datatype: 'json',
                async: false,
                data: {
                    'subjectID': this.currentSubject.pk,
                    'groupID': this.currentGroupID
                },
                success: function (data) {
                    CurrentClass.tickets = JSON.parse(JSON.stringify(data));
                    // получили - отрисовали новые тикеты
                    CurrentClass.clearTicketsContainer();
                    CurrentClass.updateTicketsContainer();
                }
            })
        }
    }

    // Визуальная отрисовка запланированных работ
    updateTicketsContainer() {
        let tickets = this.tickets;
        let CurrentClass = this;
        console.log('Tickets');
        tickets.forEach(function (ticket) {
            console.log(ticket);
            CurrentClass.appendGUITicket(ticket.title, ticket.control_type, ticket.topic_pk);
        })
    }

    // Визуальное затирание запланированных работ
    clearTicketsContainer() {
        this.ticketsContainer.innerHTML = null;
    }

    // устанавливает новый текущий объект
    setCurrentSubject() {

    }

    // Возвращает установленный текущий объект
    getCurrentSubject() {

    }

    // Создает новый элемент запланированной работы
    createNewTicket() {
        // Получить данные из инпутов
        let inputContainer = document.getElementsByClassName('modal-body-costructor-controller')[0];
        let titleInput = inputContainer.getElementsByTagName('input')[0];
        let controlTypeInput = inputContainer.getElementsByTagName('input')[1];
        let CurrentClass = this;
        let description = '';
        let isScheduled = true;

        // проверить заполнены ли поля
        if (titleInput.value != '' && controlTypeInput.value != '') {
            $.ajax({
                url: 'ajax/create_new_ticket/',
                type: 'POST',
                data: {
                    'groupID': this.getGroupID,
                    'subjectID': this.getSubjectID,
                    'description': description,
                    'title': titleInput.value,
                    'controlType': controlTypeInput.value,
                    'isScheduled': isScheduled
                },
                success: function () {
                    CurrentClass.appendGUITicket(titleInput.value, controlTypeInput.value);
                    CurrentClass.appendTicket(titleInput.value, controlTypeInput.value);
                    titleInput.value = "";
                    controlTypeInput.value = "";
                }
            });
        }
        // TODO: вывести сообщение о просьбе заполнить поля
        else {

        }
    }

    // добавляет успешно добавленное заланированное мероприятие в визуальный интерфейс
    appendGUITicket(titleStr='', controlTypeStr='', pk = 0) {
        let container = this.ticketsContainer;

        let ticketContainer = document.createElement('div'),
            ticketInfo = document.createElement('div'),
            ticketID = document.createElement("span");
        let closeBtn = document.createElement('span');

        let title = document.createElement('p'),
            controlType = document.createElement('p');
        title.textContent = titleStr;
        controlType.textContent = controlTypeStr;
        ticketID.textContent = pk;
        closeBtn.textContent = '×';

        ticketInfo.appendChild(ticketID);
        ticketInfo.appendChild(title);
        ticketInfo.appendChild(controlType);
        ticketContainer.appendChild(ticketInfo);
        ticketContainer.appendChild(closeBtn);

        ticketID.className = 'card-detail-id-hide';
        ticketContainer.className = 'modal-body-testList-ticket';
        ticketInfo.className = 'testList-ticket-info';
        closeBtn.className = 'testList-ticket-delete';

        container.appendChild(ticketContainer);
    }

    // добавляет логически ticket в конец
    appendTicket(titleStr='', controlTypeStr='', description=null) {
        this.tickets.push({
            "title": titleStr,
            "description": description,
            "control_type": controlTypeStr
        })
    }

    // очитска предыдущих тикетов после запроса
    clearTickets() {
        let container = this.ticketsContainer;
        container.innerHTML = '';
        this.tickets = [];
    }
}


// Предметы которые ведет преподаватель
class Subject {
    constructor(extPk = 0, extName = "", extSemestr_academic= 0, extCourse_of_teaching = 0, spec = "") {
        this.pk = extPk;
        this.name = extName;
        this.semestr_academic = extSemestr_academic;
        this.course_of_teaching = extCourse_of_teaching;
        this.specialization = spec;
    }
}


// Таблица предметов в конструкторе журнала
class TableConstructor {
    constructor(extSubjects = []) {
        this.table = document.getElementById("tableSubjectsConstructor");
        if (extSubjects.length != 0) {
            this.subjects = extSubjects;
        } else {
            this.subjects = extSubjects;
        }
    }

    pushSubject(subject) {
        this.subjects.push(subject);
    }

    delSubject() {
        this.subjects.pop()
    }

    getSubject(index) {
        return this.subjects[index];
    }

    addRow(course, btnText, semestr, spec) {
        let newRow = this.table.insertRow();
        let newCellBtn = newRow.insertCell(),
            newCellCourse = newRow.insertCell(),
            newCellSemestr = newRow.insertCell(),
            newCellSpecialization = newRow.insertCell();
        // Создаем ячейки
        this.createCell(newCellSpecialization, spec);
        this.createCell(newCellCourse, course);
        this.createCellBtn(newCellBtn, btnText);
        this.createCell(newCellSemestr, semestr);
        newRow.appendChild(newCellSpecialization);
        newRow.appendChild(newCellCourse);
        newRow.appendChild(newCellBtn);
        newRow.appendChild(newCellSemestr);
    }

    // определяет то, что будет внутри клеточки
    createCellBtn(cell, text) {
        let newText = document.createTextNode(text);
        // let link = document.createElement('a');
        // link.appendChild(newText);
        let btn = document.createElement('button');
        btn.innerHTML = text
        btn.setAttribute('class', 'btn btn-primary btn-lg btn-block subjectsBtn'); // set DIV class attribute
        // cell.appendChild(link);
        cell.appendChild(btn)
    }

    // Создает ячейку таблицы
    createCell(cell, text) {
        let newText = document.createTextNode(text);
        let p = document.createElement('p');
        p.innerHTML = text;
        p.setAttribute('class', 'list-component-table-semestr'); // set DIV class attribute
        cell.appendChild(p);
    }

    // генерировать таблицу предметов которые читает преподаватель
    generateTable() {
        let CurrentClass = this;
        this.subjects.forEach(function (subject) {
            CurrentClass.addRow(subject.course_of_teaching, subject.name, subject.semestr_academic, subject.specialization);
        });
    }
}


// Запрос и прорисовка предметов преподавателя
document.addEventListener('DOMContentLoaded', function (event) {
    // Запришиваем список предетов которые ведет преподаватель
    $.ajax({
        url: 'ajax/require_teachers_subjects/',
        datatype: 'json',
        async: false,
        success: function (data) {
            let data_obj = JSON.parse(JSON.stringify(data));

            data_obj.forEach(function(subject) {
                let newSubject = new Subject(subject.pk, subject.name, subject.semestr_academic, subject.course_of_teaching, subject.specialization);
                teacherSubjectConstructor.pushSubject(newSubject);
            })
            teacherSubjectConstructor.generateTable();
        },
        error: function () {
            // TODO: вывести соообщение о том, что что-то пошло не так
        }
    });
});


function generateGroupsInModalWindow(groups) {

}

// Делегат событий нажатия кнопок в таблице кнопок с предметами
let tableSubjects = document.getElementById('tableSubjectsConstructor');
tableSubjects.addEventListener("click", function (event) {
    if (event.target.tagName == 'BUTTON') {
        // Получить строку и колонку вызова
        let rowNumber = event.target.closest('tr').rowIndex - 2;
        // Узнать кто какой предмет был нажат
        let clickedSubject = teacherSubjectConstructor.subjects[rowNumber];
        console.log('rowNumber');
        console.log(rowNumber);
        console.log('clickedSubject');
        console.log(clickedSubject);

        // вызвать модальное окно планирования оценок на учебный год
        modalPlanner.style.display = "block";
        let groupDropDown = document.getElementById('groupID');
        // запросить группы которые сейчас находится на кликнутом курсе
        $.ajax({
            url: 'ajax/require_groups_on_course/',
            datatype: 'json',
            async: false,
            data: {
                'pk': clickedSubject.pk,
                'required_course': clickedSubject.course_of_teaching,
            },
            success: function (data) {
                // Отрисовать новые группы в модальном окне
                groupDropDown.innerHTML = '<option value="" selected>Группа</option>';
                let groups = JSON.parse(JSON.stringify(data));
                groups.forEach(function (subject) {
                    // заполнить группы значениями
                    let newOption = document.createElement('option');
                    newOption.appendChild(document.createTextNode(subject.number));
                    newOption.value = subject.id;
                    groupDropDown.appendChild(newOption);
                })
            },
            error: function () {
                // TODO: вывести соообщение о том, что что-то пошло не так
            }
        });

        // Обнулить и получить ссылки на карточки когда они будут запрошены
        planner = new TestPlanner(clickedSubject, courseID = clickedSubject.course_of_teaching) ;
    }
})



/*
            Глобальные переменные
*/
// Список элементов которые отображают запланированные срез знаний по предмету с его типом
let tickets = document.querySelectorAll('modal-body-testList-ticket');
let ticketContainer = document.getElementById('testList-ticketContainer');

let subjects = [],
    teacherSubjectConstructor = new TableConstructor(),
    planner;


// Получает номер идентификатора карточки по которой произошло нажатие
function getIdFromTicket(ticketGUI) {
    let id = null;
    id = ticketGUI.getElementsByClassName('card-detail-id-hide')[0].textContent;
    return id;
}

// визуальное удаление запланированных работ из списка
ticketContainer.addEventListener("click", function (event) {
   // если мы нажимаем кнопку удаления тикета
    if (event.target.className == "testList-ticket-delete") {
        let ticketGUI = event.target.closest('.modal-body-testList-ticket');
        let id = getIdFromTicket(ticketGUI),
            subjectID = planner.subjectID,
            courseID = planner.courseID,
            groupID = planner.groupID;
        $.ajax({
            url: 'ajax/delete_ticket/',
            method: 'POST',
            data: {
                "topic_pk": id,
                "subjectID": subjectID,
                "courseID": courseID,
                "groupID": groupID,
            },
            datatype: 'json',
            success: function () {
                // TODO: удалить элемент логически и удалить визуально
                planner.deleteTicketByID(id);
                // Визуально удаляет элемент
                ticketGUI.remove();
            },
            error: function () {
                // TODO: ничего с ним не делать
                // Сообщить, что удаление не произошло
            }
        });
   }
});

// Связанные выпадающие списки запрос тикетов
document.addEventListener('DOMContentLoaded', function (event){
    let groupDropDown = document.getElementById('groupID');

    // Для списка групп
    groupDropDown.addEventListener('change', function () {
        let groupID = parseInt(this.value);
        console.log('groupID');
        console.log(groupID);
        if (!isNaN(groupID)) {
            planner.groupID = groupID;
            planner.requireTickets();
            // Запрашиваем группы
            // $.ajax({
            //     url: 'ajax/get_groups_by_course/',
            //     data: {
            //         'courseID': courseID,
            //     },
            //     success: function (data) {
            //         groupDropDown.innerHTML = '<option value="" selected>Группа</option>';
            //         let subjects = JSON.parse(JSON.stringify(data));
            //         subjects.forEach(function (subject) {
            //             // заполнить группы значениями
            //             let newOption = document.createElement('option');
            //             newOption.appendChild(document.createTextNode(subject.number));
            //             newOption.value = subject.pk;
            //             groupDropDown.appendChild(newOption);
            //         })
            //     }
            // });
        }
    })
});


// привязать к кнопке отправления новой запланированной работы события
document.addEventListener('DOMContentLoaded', function () {
    let btn = document.getElementById('ticketBtnAdd');
    btn.addEventListener('click', function () {
        planner.createNewTicket();
    })
});