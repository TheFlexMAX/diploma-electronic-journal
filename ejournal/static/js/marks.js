// Работа с карточками
class Card {
    constructor(subjectName = 'Имя предмета', marks=[]) {
        this.subjectID = subjectName.id;
        this.subjectName = subjectName.name;
        this.marks = marks;
        this.avgMark = this.calculateAvg();
    }

    // Подсчитывает среднюю оценку по предмету студента
    calculateAvg() {
        let avg = 0;
        if (this.marks.length == 0) {
            return "Нет данных";
        } else {
            for (let i = 0; i < this.marks.length; i++) {
                avg += this.marks[i].mark;
            }
            avg /= this.marks.length;
            return avg.toFixed(2);
        }
    }
}

// Класс для работы с тайм лайном всех оценок в подробности
class TimeLine {
    constructor() {
        this.container = document.getElementById("marksList");
    }

    // Добавляет html элемент в конец контейнер
    addToConstructor(element) {
        this.container.appendChild(element);
    }

    // Проскроливает тайм лайн максимально вниз
    scrollToMaxDown() {
        this.container.scrollTop = this.container.scrollHeight;
    }

    // Создание всех подробных оценок как таймлайна
    createElements(marks = []) {
        // Проверяем карточки на пустоту
        if (marks.length == 0) {
            // TODO: заглушка для таймлайна
        } else {
            // формируем элементы таймлайна
            let currentClass = this;
            marks.forEach(function (mark) {
                let tlItem = document.createElement('div'),
                    markDiv = document.createElement('div'),
                    markDivValue = document.createElement('h1'),

                    themeDiv = document.createElement('div'),
                    themeTitle = document.createElement('h4'),
                    themeControlType = document.createElement('h5');

                // присваиваем элемнтам необходимые классы оформления
                tlItem.className = 'timeline-item';
                markDiv.className = 'mark';
                themeDiv.className = 'theme';
                themeTitle.className = 'title';
                themeControlType.className = 'controlType';

                // добавляем необходимые данные
                tlItem.setAttribute('date-is', mark.date_added);
                if (mark.mark) {
                    markDivValue.innerText = mark.mark;
                } else {
                    markDivValue.innerText = "-";
                }
                themeTitle.innerText = mark.topic;
                themeControlType.innerText = mark.control_type;

                // Вкладываем элементы в необходимом порядке
                markDiv.appendChild(markDivValue);

                themeDiv.appendChild(themeTitle);
                themeDiv.appendChild(themeControlType);

                tlItem.appendChild(markDiv);
                tlItem.appendChild(themeDiv);

                // Добавляем структуру в конец таймлайна
                currentClass.addToConstructor(tlItem);
            })
        }
    }

    // Очистка элементов контейнеров
    сlean() {
        this.container.innerHTML = '';
    }
}

// Возвращает набор оценок с темами по идентификатору карточки\предмета
function marksByID(id, cards = []) {
    let marks = [];

    if (cards.length != 0) {
        cards.forEach(function (card) {
            if (card.subjectID == id) {
                marks = [...card.marks];
                return marks;
            }
        })
    }

    return marks;
}

// очистка заголовка предмета модального окна
function clearTitleModalMarkTimeline() {
    let modalHead = document.getElementById('modalMarksHeader');
    modalHead.textContent = "Все оценки по ";
}

// записываем название предмета в модальное окно
function createTitleModalMarkTimeline(title = "") {
    // делаем первую букву маленькой, если первая строка не пуста
    // if (title.length != 0) {
    //     title = title[0].toLowerCase() + title.slice(1);
    // }

    let modalHead = document.getElementById('modalMarksHeader');
    modalHead.textContent = "Все оценки по " + title;
}

// Отрисовывает карточки в интерфейсе пользователя
function drawMarksCards(cards = []) {
    cardColumn = document.getElementsByClassName("card-columns")[0];
    if (cards.length != 0) {
        for (let i = 0; i < cards.length; i++) {
            // Создаем базоваые элементы для карточки
            let card = document.createElement('div'),
                cardID = document.createElement('span'),
                cardChartContainer = document.createElement('div'),
                cardChart = document.createElement('canvas'),
                cardBody = document.createElement('div');

            // Присваимваем классы элементам карточки
            card.className = 'card upToDownShadow';
            cardID.className = 'cardID hidden';
            cardBody.className = 'card-body';
            cardChart.className = 'card-chart';
            cardChartContainer.className = 'card-chartContainer';

            // Верхняя часть карточки
            let textAvgMark = document.createElement('h3');
            if (typeof(cards[i].avgMark) == "number") {
                textAvgMark.className = 'card-text subject-avg-mark';
                // создаем фигуру с данными
                createChart(0, cardChart);
            } else {
                // создаем фигуру с пустыми данными
                createChart(cards[i].avgMark, cardChart);
                textAvgMark.className = 'card-text subject-none-mark';
            }
            textAvgMark.innerText = cards[i].avgMark;
            cardChartContainer.appendChild(cardChart);
            cardID.innerText = cards[i].subjectID;

            // Нижняя часть карты
            let subjectName = document.createElement('h3');
            subjectName.innerText = cards[i].subjectName;
            subjectName.className = 'card-title subject-name';
            let btn = document.createElement('button');
            btn.className = 'btn btn-secondary btn-block';
            btn.innerHTML = 'Подробнее';

            // повесить событие вызова создания таймлайна в модальном окне
            btn.addEventListener('click', function () {
                clearTitleModalMarkTimeline(); // очищаем название предмета
                timeline.сlean() // очищаем содержимое экрана
                showModalMarkTimeline(); // отображаем окно
                createTitleModalMarkTimeline(cards[i].subjectName) // прописываем название предмета в заголовке
                let marks = marksByID(cardID.innerText, cards); // Получаем набор оценок, который нужно отобразить

                timeline.createElements(marks) // Отображаем в окне таймлайн с оценками
                timeline.scrollToMaxDown(); // пролистываем скролл вниз
            });
            // добавляем элемент в карточку
            cardBody.appendChild(subjectName);
            cardBody.appendChild(btn);

            // Добавляем эелементы карточки в карту
            card.appendChild(cardID);
            card.appendChild(cardChartContainer);
            card.appendChild(cardBody);

            // Добавляем карточку к остальным картам
            cardColumn.appendChild(card);
        }
    } else {
        // TODO: вывести заглушку
    }
}

// Выбирает цвета и формирует данные в необходимом формате для графика оценки
function createChart(avgMark, canvas) {
    // определяем максимальный диапазон значения графика
    let max = (5 - avgMark).toFixed(2);

    // Определяем цвет области
    let color = '#FFFFFF';
    if (avgMark <= 2) {
        color = "#A10705";
    } else if (avgMark <= 3.99) {
        color = "#ffe16b";
    } else if (avgMark >= 4) {
       color = "#3a9104";
    } else {
        color = "#4d4d4d";
    }

    // Оформляем данные, присваиваем нужный цвет
    let data = {
        labels: [],
        datasets: [
            {
                data: [avgMark, max],
                backgroundColor: [
                    color,
                    "#FFFFFF",
                ],
                hoverBackgroundColor: [
                    color,
                    "#FFFFFF",
                ]
            }]
    };
    // формируем сам график полу-окружностью
    let doughnutChart = new Chart(canvas, {
        type: 'doughnut',
        data: data,
        options: {
            tooltips: {
                enabled: false
            },
            rotation: 1 * Math.PI,
            circumference: 1 * Math.PI,
            elements: {
                center: {
                    text: avgMark,
                    color: color, // Default is #000000
                    fontStyle: 'Arial', // Default is Arial
                    sidePadding: 20, // Default is 20 (as a percentage)
                    minFontSize: 25, // Default is 20 (in px), set to false and text will not wrap.
                    lineHeight: 25 // Default is 25 (in px), used for when text wraps
                }
            },
            cutoutPercentage: 80
        }
    });

    // Производим изменения плагина ChartJS, чтобы отображать результат оценки по середина графика
    Chart.pluginService.register({
        beforeDraw: function(chart) {
            if (chart.config.options.elements.center) {
                // Get ctx from string
                let ctx = chart.chart.ctx;

                // Get options from the center object in options
                let centerConfig = chart.config.options.elements.center;
                let fontStyle = centerConfig.fontStyle || 'Arial';
                let txt = centerConfig.text;
                let color = centerConfig.color || '#000';
                let maxFontSize = centerConfig.maxFontSize || 75;
                let sidePadding = centerConfig.sidePadding || 20;
                let sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
                // Start with a base font of 30px
                ctx.font = "30px " + fontStyle;

                // Get the width of the string and also the width of the element minus 10 to give it 5px side padding
                let stringWidth = ctx.measureText(txt).width;
                let elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

                // Find out how much the font can grow in width.
                let widthRatio = elementWidth / stringWidth;
                let newFontSize = Math.floor(30 * widthRatio);
                let elementHeight = (chart.innerRadius * 2);

                // Pick a new font size so it will not be larger than the height of label.
                let fontSizeToUse = Math.min(newFontSize, elementHeight, maxFontSize);
                let minFontSize = centerConfig.minFontSize;
                let lineHeight = centerConfig.lineHeight || 25;
                let wrapText = false;

                if (minFontSize === undefined) {
                    minFontSize = 20;
                }

                if (minFontSize && fontSizeToUse < minFontSize) {
                    fontSizeToUse = minFontSize;
                    wrapText = true;
                }

                // Set font settings to draw it correctly.
                ctx.textAlign = 'center';
                ctx.textBaseline = 'middle';
                let centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
                let centerY = chart.chartArea.bottom * 0.85;
                ctx.font = fontSizeToUse + "px " + fontStyle;
                ctx.fillStyle = color;

                if (!wrapText) {
                    ctx.fillText(txt, centerX, centerY);
                    return;
                }

                let words = txt.split(' ');
                let line = '';
                let lines = [];

                // Break words up into multiple lines if necessary
                for (let n = 0; n < words.length; n++) {
                    let testLine = line + words[n] + ' ';
                    let metrics = ctx.measureText(testLine);
                    let testWidth = metrics.width;
                    if (testWidth > elementWidth && n > 0) {
                        lines.push(line);
                        line = words[n] + ' ';
                    } else {
                        line = testLine;
                    }
                }

                // Move the center up depending on line height and number of lines
                centerY -= (lines.length / 2) * lineHeight * 2;

                for (let n = 0; n < lines.length; n++) {
                    ctx.fillText(lines[n], centerX, centerY);
                    centerY += lineHeight;
                }
                //Draw text in center
                ctx.fillText(line, centerX, centerY);
            }
        }
    });
}

// Очищает карточки в интерфейсе пользователя
function clearMarksCards() {
    cardColumn = document.getElementsByClassName("card-columns");
    cardColumn.innerHTML = "";
}


/*
* Глобальные переменные
* */
let cards = Array();
let timeline = new TimeLine();

// список предметов
let subjectsWithMarks = [];
$.ajax({
    url: '/journal/ajax/get_subjects_for_student/',
    data: {},
    datatype: 'json',
    success: function (data) {
        cards = [];
        data_marks = JSON.parse(JSON.stringify(data));
        // Записываем список предметов
        for (let i = 0; i < data_marks.length; i++) {
            cards.push(new Card(data_marks[i].subject, data_marks[i].marks))
        }

        // Отрисовать карточки в интерфейсе
        drawMarksCards(cards);
    }
});


/* Особенности поведения модального окна в данном разделе */
/*
* Модальное окно
* */
// Get the modal
let modalTimeLine = document.getElementById("modalMarksTimeline");

// Get the <span> element that closes the modal
let span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
    modalTimeLine.style.display = "none";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modalTimeLine) {
        modalTimeLine.style.display = "none";
    }
};

// отображение модального окна
function showModalMarkTimeline() {
    modalTimeLine.style.display = 'block';
}

// Функция скрытия модального окна
function hideModalMarkTimeline() {
    modalTimeLine.style.display = 'none';
}

// Скрытие модального окна на кнопки и нажатие esc
document.addEventListener('DOMContentLoaded', function () {
    // кнопке снизу
    let closeBtnFooter = document.getElementById('closeButtonFooter');
    closeBtnFooter.addEventListener('click', hideModalMarkTimeline);

    // при нажатии на ESC
    // Сокрытие модального окна при нажатии кнопки ESC
    document.addEventListener('keydown', event => {
        if (event.key === 'Escape' || event.keyCode === 27) {
            hideModalMarkTimeline();
        }
    });
})


/*
* Живой поиск необходимых предметов
* */
document.querySelector('#searchCardInMarks').oninput = function () {
    let subjName = this.value.trim(); // Обрезаем пробелы, которые пользователь мог случайно оставить
    let subjNameLower = subjName.toLowerCase();
    let subjCards = document.querySelectorAll(".card");

    if (subjName != '') {
        subjCards.forEach(function (card) {
            // Достать заголовок у карточки
            let cardText = card.getElementsByTagName('h3')[0].innerText;
            let cardTextLower = cardText.toLowerCase();

            // если подстрока в строку не входит - скрываем элемент
            if (cardTextLower.search(subjNameLower) == -1) {
                card.style.display = 'none';
                let str = card.getElementsByTagName('h3')[0].innerText;
                // Убираем выделение строки в названии (очищаем от тега mark)
                card.getElementsByTagName('h3')[0].innerText = str.replace(/<\/?(mark)\b[^<>]*>/g, "");
            } else {
                card.style.display = 'inline-block';
                // Подсвечиваем найденный текст
                card.getElementsByTagName('h3')[0].innerHTML = insertMark(cardText, cardTextLower.search(subjNameLower), subjName.length)
            }
        })
    }
    // Если же пользователь очистил строку - показываем все скрытые элементы
    else {
        subjCards.forEach(function (card) {
            // Достать заголовок у карточки
            let str = card.getElementsByTagName('h3')[0].innerText;
            card.style.display = 'inline-block';
            // Убираем выделение строки в названии (очищаем от тега mark)
            card.getElementsByTagName('h3')[0].innerText = str.replace(/<\/?(mark)\b[^<>]*>/g, "");
        })
    }
}
    
// Подсвечивание того, что ввел пользователь в названиях карточек
function insertMark(subjName, pos, len) {
    return subjName.slice(0, pos) + "<mark>" + subjName.slice(pos, pos + len) + "</mark>" + subjName.slice(pos + len);
}
