// Глобальные переменные
let navBar=document.getElementsByClassName('navbar-primary')[0];
let navBarName = document.getElementsByClassName('navbar-primary-name')[0];


// Увеличение боковой навигационной панели
let btnExpandCollapse = document.getElementsByClassName('btn-expand-collapse')[0];
btnExpandCollapse.addEventListener('click', function (event) {
    if (navBar.classList.contains('collapsed')) {
        navBar.classList.remove('collapsed');
        navBarName.classList.remove('hidden');
    } else {
        navBar.classList.add('collapsed');
        navBarName.classList.add('hidden');
    }
});

// Скрытие навигационной панели при нажатии вне него
document.addEventListener('click', function(event) {
    if (!navBar.contains(event.target)) {
        if (!(navBar.classList.contains('collapsed'))) {
            navBar.classList.add('collapsed');
            navBarName.classList.add('hidden');
        }
    }
});
