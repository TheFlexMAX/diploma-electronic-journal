// закрытие полученных уведомлений
let notificationContainer = document.getElementsByClassName('notifications-pop-up-container')[0];
notificationContainer.addEventListener('click', function (event) {
    if (event.target.tagName === 'SPAN') {
        let notification = event.target.closest('.notification ');
        notification.remove();
    }
});

// Логическое хранение данных уведомления
class notification {
    constructor(title = "title", message = "msg") {
        this.title = title;
        this.message = message;
    }
}

// На будущее
class Notifications {
    constructor() {
        // графический контейнер уведомлений
        this.container = document.getElementsByClassName('notifications-pop-up-container')[0];
        this.notifications = []; // сами уведомления

        // Визуальное удаление уведомления
        this.container.addEventListener('click', function (event) {
            if (event.target.tagName === 'SPAN') {
                let notification = event.target.closest('.notification ');
                notification.remove();
            }
        })
    }

    removeNotification(notification) {
        notification.remove();
    }

    // Устанавливает таймер на удаление всплывающее уведомления
    setTimerForRemove(notification) {
        setTimeout(this.removeNotification, 3000, notification);
    }

    // создает новое уведомление графически
    createNotification (title = "title", message = "msg") {
        // создаем графические элементы уведомления
        let notification = document.createElement('div'),
        notificationContainer = document.createElement('div'),
        notificationContainerIMG = document.createElement('div'),
        notificationContainerMSG = document.createElement('div'),
        closeBtn = document.createElement('span'),
        msgTitle = document.createElement('div'),
        msgTitleP = document.createElement('p'),
        msgText = document.createElement('div'),
        msgTextP = document.createElement('p');

        // даем классы оформления
        notification.className = 'notification round-lite liteShadow';
        notificationContainer.className = 'notification-container';
        notificationContainerIMG.className = 'notification-container-img';
        notificationContainerMSG.className = 'notification-container-message';
        closeBtn.className = 'close notification-close';
        msgTitle.className = 'notification-container-message-title';
        msgText.className = 'notification-container-message-text';

        // Устанавливаем текст
        msgTitleP.textContent = title;
        msgTextP.textContent = message;

        // Создаем структуру дома уведомления от обратного
        msgText.appendChild(msgTextP); // текст сообщения в контейнер сообщения
        msgTitle.appendChild(msgTitleP);

        // добавляем текстовые элементы в уведомление вместе с кнопкой
        notificationContainerMSG.appendChild(closeBtn);
        notificationContainerMSG.appendChild(msgTitle);
        notificationContainerMSG.appendChild(msgText);

        // Добавляем элементы изображения и текста в тело уведомления
        notificationContainer.appendChild(notificationContainerIMG);
        notificationContainer.appendChild(notificationContainerMSG);

        // Формируем уведомление и возвращаем его
        notification.appendChild(notificationContainer);

        return notification;
    }

    // выводит созданное уведомление
    showNotification(title = "title", message = "message") {
        // Вывести его на экран
        let newNotification = this.createNotification(title, message);
        this.container.appendChild(newNotification);
        console.log('newNotification');
        console.log(newNotification);

        // повесить событие автоматического удаления уведомления
        this.setTimerForRemove(newNotification);
    }
}

// Global variables
let notifications = new Notifications();