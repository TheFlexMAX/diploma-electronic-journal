// Скрытие номера зачетной книги
function hideRB() {
    let rb = document.getElementById("record_book-number");
    console.log("rb")
    console.log(rb)
    rb.classList.add("hide");
    rb.style.display = "none";
}

// Отображение номера зачетки
function showRB() {
    let rb = document.getElementById("record_book-number");
    rb.classList.remove("hide");
    rb.style.display = "block";
}

// Скрытие стандартной заглушки для зачеток
function hidePlugRB() {
    let plug = document.getElementsByClassName("record_book-plug")[0];
    plug.classList.add('hide');
    plug.style.display = "none";
}

// Отобржание стандартной заглушки для зачеток
function showPlugRB() {
    let plug = document.getElementsByClassName("record_book-plug")[0];
    plug.classList.remove('hide');
    plug.style.display = "flex";
}

// Делаем проверку, сгенерировались ли пользователю таблицы из зачетки
document.addEventListener('DOMContentLoaded', function () {
    // Проверяем, созданы ли таблицы
    let rbTables = document.getElementsByClassName('rbCourseBlock');
    console.log('rbTables.length ');
    console.log(rbTables.length );
    if (rbTables.length === 0) {
        // Если не созданы - выводим заглушку
        showPlugRB();
        hideRB();
    }
})
