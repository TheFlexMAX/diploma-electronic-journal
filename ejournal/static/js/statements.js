
// класс для работы связанных форм таблицы курсовых ведомостей
class CoursesExamStatements {

    constructor() {
        // Ссылка на html контент самой формы
        this.form = document.getElementById("state_1_form");
        // Ссылки на html теги содержимого документа
        this.course = document.getElementById("state_1_form_1");
        this.group = document.getElementById("state_1_form_2");
        this.subject = document.getElementById("state_1_form_3");
        this.controlType = document.getElementById("state_1_form_4");
        this.examinations = document.getElementById("state_1_form_5");
        this.exam_date = document.getElementById("state_1_form_6");
        this.semestr_half = document.getElementById("state_1_form_7");

        // Создание связанного списка
        this.initChainedForm();
        // Вешаем событие отправки формы асинхронно
        this.initAsyncSubmit();

        // Логическое содержимое класса
        this.data = {};
    }

    // Возврашает данные о студентах
    get resMarks() {
        if (this.data) return this.data.res_marks;
        return {};
    }

    // Возвращает данные о результирующих оценках
    get students() {
        if (this.data) return this.data.students;
        return {};
    }

    // Очищает данные из логики
    clear() {
        this.data = {};
    }

    // Запрашивает группы, по выбранному курсу
    requireGroups() {
        let groups = this.group,
            subjects = this.subject;
        let courseID = parseInt(this.course.value);
        console.log('this.course.value');
        console.log(this.course.value);

        // проверяем, выбран ли какой-либо другой выпадающий список
        if (!isNaN(courseID)) {
            // Запрашиваем группы
            $.ajax({
                url: 'ajax/get_groups_by_courseID/',
                data: {
                    courseID: courseID
                },
                async: false,
                dataType: 'json',
                success: function (data) {
                    groups.innerHTML = '<option value="" selected>Выберите группу</option>';
                    let subjects = JSON.parse(JSON.stringify(data));

                    subjects.forEach(function (subject) {
                        // заполнить группы значениями
                        let newOption = document.createElement('option');
                        newOption.appendChild(document.createTextNode(subject.number));
                        newOption.value = subject.pk;
                        groups.appendChild(newOption);
                    })
                },
                error: function (data) {
                    //TODO: вызвать уведомление с ошибкой
                }
            });
        } else {
            // Удаляем группы
            groups.innerHTML = '<option value="" selected>Выберите группу</option>';
            // Удаляем предметы
            subjects.innerHTML = '<option value="" selected>Выберите предмет</option>';
        }
    }

    // Запрашивает предметы по выбранной группе
    requireSubjects() {

    }

    // Запрашивает результирующие данные с сервера (когда введены все нужные данные)
    requireData() {
        $.ajax({
            url: 'ajax/get_subjects/',
            data: { groupId: this.groupID },
            dataType: 'json',
            async: true,
            success: function (data) {

            },
            error: function (data) {

            }
        })
    }

    // Инициализирует связанный список
    initChainedForm() {
        let currentClass = this;

        let course = currentClass.course,
            group = currentClass.group,
            subject = currentClass.subject;

        // Для групп
        course.addEventListener('change', function () {
            let groups = currentClass.group,
                subjects = currentClass.subject;
            let courseID = parseInt(currentClass.course.value);

            // проверяем, выбран ли какой-либо другой выпадающий список
            if (!isNaN(courseID)) {
                // Запрашиваем группы
                $.ajax({
                    url: 'ajax/get_groups_by_courseID/',
                    data: {
                        courseID: courseID
                    },
                    async: false,
                    dataType: 'json',
                    success: function (data) {
                        groups.innerHTML = '<option value="" selected>Выберите группу</option>';
                        let subjects = JSON.parse(JSON.stringify(data));

                        subjects.forEach(function (subject) {
                            // заполнить группы значениями
                            let newOption = document.createElement('option');
                            newOption.appendChild(document.createTextNode(subject.number));
                            newOption.value = subject.pk;
                            groups.appendChild(newOption);
                        })
                    },
                    error: function (data) {
                        //TODO: вызвать уведомление с ошибкой
                    }
                });
            } else {
                // Удаляем группы
                groups.innerHTML = '<option value="" selected>Выберите группу</option>';
                // Удаляем предметы
                subjects.innerHTML = '<option value="" selected>Выберите предмет</option>';
            }
        });

        // Для предметов
        group.addEventListener('change', function () {
            let groups = currentClass.group,
                subjectDropdown = currentClass.subject;
            let courseID = parseInt(currentClass.course.value),
                groupID = parseInt(currentClass.group.value);

            // проверяем, выбран ли какой-либо другой выпадающий список
            if (!isNaN(courseID) && !isNaN(groupID)) {
                // Очищаем набор предметов
                subjectDropdown.innerHTML = '<option value="" selected>Выберите предмет</option>';
                // Запрашиваем группы
                $.ajax({
                    url: 'ajax/get_subjects_by_groupID/',
                    data: {
                        'groupID': groupID
                    },
                    async: false,
                    success: function (data) {
                        let subjects = JSON.parse(JSON.stringify(data));
                        subjects.forEach(function (subject) {
                            // заполнить группы значениями
                            let newOption = document.createElement('option');
                            newOption.appendChild(document.createTextNode(subject.name));
                            newOption.value = subject.pk;
                            subjectDropdown.appendChild(newOption);
                        })
                    },
                    error: function (data) {
                        //TODO: вызвать уведомление с ошибкой
                    }
                });
            } else {
                // Удаляем предметы
                subjectDropdown.innerHTML = '<option value="" selected>Выберите предмет</option>';
            }
        })

        // Для предметов
        // subject.addEventListener('change', );


    }

    // Инициализирует асинхронную отправку данных в обработку на сервер
    initAsyncSubmit() {
        let currentClass = this;
        $(currentClass.form).on('submit', function(event){
            event.preventDefault();
            $.ajax({
                url: currentClass.form.getAttribute("data-url"),
                type: currentClass.form.getAttribute("method"),
                dataType: "json",
                data: {
                    "control_type": currentClass.controlType.value,
                    "subject": currentClass.subject.value,
                    "group": currentClass.group.value,
                    "course_number": currentClass.course.value,
                    "event_date": currentClass.exam_date.value,
                    "examiners": currentClass.examinations.value,
                    "semestr_half": currentClass.semestr_half.value,
                    "form_name": "CoursesExamStatements"
                },
                async: false,
                success: function (data) {
                    let extData = JSON.parse(JSON.stringify(data));
                    // Генерировать таблицу и скрыть форму
                    currentClass.hideStatementsForm("state_1_form"); // скрываем форму
                    // Генерируем содержимое таблицы
                    console.log('statements');
                    console.log(statements);
                    statements.stateTableCourses.data = extData;
                    statements.stateTableCourses.generateTable();
                    // отображаем таблицу
                    statements.stateTableCourses.showStatementsTable();
                },
                error: function (data) {
                    //TODO: Вывести уведомление об ошибке

                }
            })
        });
        // Определяем то, как кнопка submit будет отправлять данные на сервер
    }

    // Визуально скрывает форму
    hideStatementsForm() {
        this.form.classList.add('hide');
    }

    // Визуально отображает форму
    showStatementsForm() {
        this.form.classList.remove('hide');
    }
}

let controlTypeDataByCourse_data = {};
let controlTypeDataByCourse_chooser = document.getElementById("controlTypeDataByCourse-chooser");
controlTypeDataByCourse_chooser.addEventListener('change', function () {

    // создать новую в зависимости от выбора
    let layout = document.getElementById("layoutState_2_formRes");
    layout.style.position = 'absolute'
    let closeBtn = document.getElementById('closeState_2_formRes'),
        closeBtnContainer = document.getElementById('closeState_2_formRes_container');
    closeBtnContainer.classList.remove('hide');

    closeBtn.addEventListener('click', function (event) {
        let tableForDelName = "table-result-statement-course"
        for (let i = 0; i <= 50; i++) {
            let tmpName = tableForDelName + i;
            let table = document.getElementById(tmpName);
            if (table) table.remove();
        }
        closeBtnContainer.classList.add('hide');
        layout.style.position = 'relative';
        controlTypeDataByCourse_data = {}; // Очищаем глобальную переменную
    })

    // Пробуем генерировать данные в таблице получив ее
    let tableContainer = document.getElementById("table-statements-container");
    let tableTemplate = document.getElementById('table-template');
    let tableNameCounter = 1;
    // Очищаем от ненужных таблиц
    let tables = tableContainer.getElementsByClassName("table table-bordered table-responsive");

    let tableForDelName = "table-result-statement-course"
    for (let i = 0; i <= 50; i++) {
        let tmpName = tableForDelName + i;
        let table = document.getElementById(tmpName);
        if (table) table.remove();
    }

    let currentChoose = controlTypeDataByCourse_chooser.value;
    let modCurrentChoose = currentChoose.split(' ')[0].trim(); // обрезка до цифр
    // заполняем новыми данными
    if (currentChoose != "") {
        console.log('controlTypeDataByCourse_data')
        console.log(controlTypeDataByCourse_data)
        // Делаем проерку на то, не выбрана ли выборка по экзамену\аттестации
        if (!(currentChoose === "Залік\\Екзамен")) {
            controlTypeDataByCourse_data.types_of_check_knowledge.forEach(function (data) {

                if (data.control_type === modCurrentChoose) {
                    data.data_by_specs.forEach(function (specData) {
                        let table = tableTemplate.cloneNode(true);
                        table.classList.remove('hide');

                        tableContainer.appendChild(table);
                        let newTableName = "table-result-statement-course" + tableNameCounter++;
                        table.id = newTableName;
                        table.getElementsByClassName("spec-header-controlType")[0].innerText = specData.spec_name;
                        table.getElementsByClassName("spec-header-courseNumber")[0].innerText = "Курс " + controlTypeDataByCourse_data.course_number;
                        table.getElementsByClassName("spec-header-specCode")[0].innerText = "Cпеціальність "+specData.spec_number;

                        let tbody = table.getElementsByTagName("TBODY")[0];
                        console.log('specData')
                        console.log(specData)

                        // Заполняем студентами
                        specData.studs.forEach(function (studs) {
                            let newTr = document.createElement('tr'),
                                newTd = document.createElement('td');
                            newTd.innerText = studs.second_name + " " + studs.first_name;

                            newTr.id = "stud-tr-" + studs.id;

                            newTr.appendChild(newTd);
                            tbody.appendChild(newTr);
                        })

                        // Заполняем предметы
                        specData.subjects.forEach(function (subjData){
                            // Добавлеяем заголовок предмета
                            let trSubjHeader = table.getElementsByClassName('subjects')[0];
                            let newTdSubjHeader = document.createElement('td');
                            console.log('subjData')
                            console.log(subjData)
                            let newP = document.createElement('p');
                            newP.classList = "verticalText";
                            newP.innerText = subjData.subj_name;

                            newTdSubjHeader.appendChild(newP);
                            trSubjHeader.appendChild(newTdSubjHeader);

                            // создаем ячейки и потом их заполняем
                            specData.studs.forEach(function (studs) {
                                let studTrID = "stud-tr-" + studs.id;
                                let tr = document.getElementById(studTrID),
                                    newTd = document.createElement('td');
                                newTd.innerText = '';
                                tr.appendChild(newTd);
                            })

                            // заполняем оценки
                            subjData.topics_and_marks.forEach(function (topAndMark) {
                                if (topAndMark.topics_and_marks.topic == currentChoose) {
                                    console.log('topAndMark')
                                    console.log(topAndMark)
                                    topAndMark.marks.forEach(function (mark) {
                                        let studTrID = "stud-tr-" + mark.mark_owner;
                                        let studTr = document.getElementById(studTrID);
                                        let lastTd = studTr.cells[studTr.cells.length-1];
                                        if (mark.mark == "None") {
                                            lastTd.innerText = "";
                                        } else {
                                            lastTd.innerText = mark.mark;
                                        }
                                    })
                                }
                            })
                        })
                        // подсчитываем среднее по всем предметам для студента
                        let trSubjects = tbody.getElementsByClassName('subjects')[0];
                        let avgHeader = document.createElement('td'),
                            newP = document.createElement('p');
                        newP.innerText = "Середній бал";
                        newP.classList = "verticalText";
                        avgHeader.innerText = "Середній бал";
                        trSubjects.appendChild(avgHeader);
                        // заполняем среднюю оценку
                        let avgMarks = [];
                        // создаем ячейки и потом их заполняем
                        specData.studs.forEach(function (stud) {
                            let studTrID = "stud-tr-" + stud.id;
                            let tr = document.getElementById(studTrID),
                                newTd = document.createElement('td');

                            // прохожусь по сгенерированной строке и считаю среднюю оценку
                            let sum = 0;
                            let count = 0;
                            let cells = tr.cells;
                            for (let i = 0; i < cells.length; i++) {
                                let valOfCell = parseInt(cells[i].innerText);
                                if (!(isNaN(valOfCell))) {
                                    sum += valOfCell;
                                }
                                count++;
                            }
                            count--;
                            if (count != 0 && sum != 0) {
                                newTd.innerText = (sum / count).toFixed(2);
                            } else {
                                newTd.innerText = "0.00";
                            }
                            tr.appendChild(newTd);
                        })
                    })
                }
            })
        } else {

            controlTypeDataByCourse_data.types_of_check_knowledge.forEach(function (data) {
                if (data.control_type === "Залік") {
                    data.data_by_specs.forEach(function (specData) {
                        let table = tableTemplate.cloneNode(true);
                        table.classList.remove('hide');
                        tableContainer.appendChild(table);
                        let newTableName = "table-result-statement-course" + tableNameCounter++;
                        table.id = newTableName;
                        table.getElementsByClassName("spec-header-controlType")[0].innerText = specData.spec_name;
                        table.getElementsByClassName("spec-header-courseNumber")[0].innerText = "Курс " + controlTypeDataByCourse_data.course_number;
                        table.getElementsByClassName("spec-header-specCode")[0].innerText = "Cпеціальність "+specData.spec_number;

                        let tbody = table.getElementsByTagName("TBODY")[0];

                        console.log('Залік')
                        // Заполняем студентами
                        specData.studs.forEach(function (studs) {
                            let newTr = document.createElement('tr'),
                                newTd = document.createElement('td');
                            newTd.innerText = studs.second_name + " " + studs.first_name;
                            newTr.id = "stud-tr-" + studs.id;

                            newTr.appendChild(newTd);
                            tbody.appendChild(newTr);
                        })
                        // создаем ячейки
                        specData.studs.forEach(function (stud) {
                            let studTrID = "stud-tr-" + stud.id;
                            let tr = document.getElementById(studTrID),
                                newTd = document.createElement('td');
                            // создаем ячеек столько же, сколько и прдметов
                            let subjAmount = specData.subjects.length;
                            for (let i = 0; i < subjAmount; i++) {
                                tr.appendChild(document.createElement('td'));
                            }
                        })
                        // Заполняем предметы
                        let colNumber = 1;
                        specData.subjects.forEach(function (subjData){
                            // Добавлеяем заголовок предмета
                            let trSubjHeader = table.getElementsByClassName('subjects')[0];
                            let newTdSubjHeader = document.createElement('td');
                            console.log('subjData')
                            console.log(subjData)
                            let newP = document.createElement('p');
                            newP.classList = "verticalText";
                            newP.innerText = subjData.subj_name;

                            newTdSubjHeader.appendChild(newP);
                            trSubjHeader.appendChild(newTdSubjHeader);

                            // заполняем оценки по строке
                            subjData.topics_and_marks.forEach(function (topAndMark) {
                                console.log("topAndMark.topics_and_marks.topic.indexOf('Залік')");
                                console.log(topAndMark.topics_and_marks.topic.indexOf('Залік') >= 0);
                                if (topAndMark.topics_and_marks.topic.indexOf('Залік') >= 0) {
                                    topAndMark.marks.forEach(function (mark) {
                                        let studTrID = "stud-tr-" + mark.mark_owner;
                                        let studTr = tbody.querySelector(("#"+studTrID));
                                        let lastTd = studTr.cells[colNumber];
                                        console.log("colNumber");
                                        console.log("colNumber");
                                        if (!(mark.mark == "None")) {
                                            lastTd.innerText = mark.mark;
                                        }
                                    })
                                }
                            })
                            colNumber++;
                        })
                    })
                }
                else if (data.control_type === "Екзамен") {
                    let decCounter = tableNameCounter - 1;
                    data.data_by_specs.forEach(function (specData) {
                        let newTableName = "table-result-statement-course" + (tableNameCounter - decCounter).toString();
                        decCounter--;

                        console.log('newTableName')
                        console.log(newTableName)

                        let table = document.getElementById(newTableName);
                        let tbody = table.getElementsByTagName("TBODY")[0];
                        // Заполняем созданные ячейки
                        let colNumber = 1;
                        specData.subjects.forEach(function (subjData){
                            subjData.topics_and_marks.forEach(function (topAndMark) {
                                // Проверяем проводились ли необходимые работы
                                if (topAndMark.topics_and_marks.length != 0) {
                                    if (topAndMark.topics_and_marks.topic.indexOf('Екзамен') >= 0) {
                                        topAndMark.marks.forEach(function (mark) {
                                            let studTrID = "stud-tr-" + mark.mark_owner;
                                            let studTr = tbody.querySelector(("#"+studTrID));
                                            let lastTd = studTr.cells[colNumber];
                                            if (!(mark.mark == "None")) {
                                                lastTd.innerText = mark.mark;
                                            }
                                        })
                                    }
                                }
                                colNumber++;
                            })
                        })
                        // подсчитываем среднее по всем предметам для студента
                        let trSubjects = tbody.getElementsByClassName('subjects')[0];
                        let avgHeader = document.createElement('td');
                        newP = document.createElement('p');
                        newP.innerText = "Середній бал";
                        newP.classList = "verticalText";
                        avgHeader.innerText = "Середній бал";
                        trSubjects.appendChild(avgHeader);
                        // заполняем среднюю оценку
                        let avgMarks = [];
                        // создаем ячейки и потом их заполняем
                        specData.studs.forEach(function (stud) {
                            let studTrID = "stud-tr-" + stud.id;
                            let tr = document.getElementById(studTrID),
                                newTd = document.createElement('td');

                            // прохожусь по сгенерированной строке и считаю среднюю оценку
                            let sum = 0;
                            let count = 0;
                            let cells = tr.cells;
                            for (let i = 0; i < cells.length; i++) {
                                let valOfCell = parseInt(cells[i].innerText);
                                if (!(isNaN(valOfCell))) {
                                    sum += valOfCell;
                                }
                                count++;
                            }
                            count--;
                            if (count != 0 && sum != 0) {
                                newTd.innerText = (sum / count).toFixed(2);
                            } else {
                                newTd.innerText = "0.00";
                            }
                            tr.appendChild(newTd);
                        })
                    })
                }
            })
        }
    }
})

let controlTypeDataByCourse = {};

// Инициализирует асинхронную отправку данных в обработку на сервер
document.addEventListener('DOMContentLoaded', function () {
    let currentClass = document.getElementById("state_2_form"),
        course_number = document.getElementById("state_2_form_1"),
        semestr_half = document.getElementById("state_2_form_2");
    $(currentClass).on('submit', function(event){
        event.preventDefault();
        $.ajax({
            url: currentClass.getAttribute("data-url"),
            type: currentClass.getAttribute("method"),
            dataType: "json",
            data: {
                "course_number": course_number.value,
                "semestr_half": semestr_half.value,
                "form_name": "CoursesFinalStatements"
            },
            async: false,
            success: function (data) {
                // Отображаем все элементы в выпадающем списке
                console.log('controlTypeDataByCourse_chooser.options')
                console.log(controlTypeDataByCourse_chooser.options)
                let options = controlTypeDataByCourse_chooser.options;
                for (let i = 0; i < options.length; i++) {
                    options[i].classList.remove("hide");
                }
                if (semestr_half.value == "first") {
                    for (let i = 0; i < options.length; i++) {
                        let val = options[i].value;
                        if (val == "Атестація 3" || val == "Атестація 4" || val == "ДКР 2") {
                            options[i].classList.add("hide");
                        }
                    }
                } else {
                    for (let i = 0; i < options.length; i++) {
                        let val = options[i].value;
                        if (val == "Атестація 1" || val == "Атестація 2" || val == "ДКР 1") {
                            options[i].classList.add("hide");
                        }
                    }
                }

                let layout = document.getElementById("layoutState_2_formRes");
                layout.style.position = 'absolute'

                let extData = JSON.parse(JSON.stringify(data));
                controlTypeDataByCourse_data = extData; // Закидываем в глобальную переменную
                let closeBtn = document.getElementById('closeState_2_formRes'),
                    closeBtnContainer = document.getElementById('closeState_2_formRes_container');

                closeBtnContainer.classList.remove('hide');
                controlTypeDataByCourse_chooser.classList.remove('hide');

                closeBtn.addEventListener('click', function (event) {
                    let tableForDelName = "table-result-statement-course"
                    for (let i = 0; i <= 50; i++) {
                        let tmpName = tableForDelName + i;
                        let table = document.getElementById(tmpName);
                        if (table) table.remove();
                    }
                    closeBtnContainer.classList.add('hide');
                    controlTypeDataByCourse_chooser.classList.add('hide');

                    layout.style.position = 'relative';
                    controlTypeDataByCourse_data = {}; // Очищаем глобальную переменную
                })

                // Пробуем генерировать данные в таблице получив ее
                let tableContainer = document.getElementById("table-statements-container");
                let tableTemplate = document.getElementById('table-template');
                let tableNameCounter = 1;
                // Очищаем от ненужных таблиц
                let tables = tableContainer.getElementsByClassName("table table-bordered table-responsive");

                let tableForDelName = "table-result-statement-course"
                for (let i = 0; i <= 50; i++) {
                    let tmpName = tableForDelName + i;
                    let table = document.getElementById(tmpName);
                    if (table) table.remove();
                }
                //установить положение выпадающего списка в значение первого не скрытого
                for (let i = 1; i < options.length; i++) {
                    // ищем первый не скрытый элемент и выбираем его
                    if (!(options[i].classList.contains('hide'))) {
                        options[i].selected = 'selected';
                        break;
                    }
                }
                let evt = document.createEvent("HTMLEvents");
                evt.initEvent("change", false, true);
                controlTypeDataByCourse_chooser.dispatchEvent(evt);
            },
            error: function (data) {
                //TODO: Вывести уведомление об ошибке

            }
        })
    });
    // Определяем то, как кнопка submit будет отправлять данные на сервер
})

document.addEventListener('DOMContentLoaded', function () {
    let currentClass = document.getElementById("state_3_form");
    $(currentClass.form).on('submit', function(event){
        event.preventDefault();
        $.ajax({
            url: currentClass.form.getAttribute("data-url"),
            type: currentClass.form.getAttribute("method"),
            dataType: "json",
            data: {
                "control_type": currentClass.controlType.value,
                "subject": currentClass.subject.value,
                "group": currentClass.group.value,
                "course_number": currentClass.course.value,
                "event_date": currentClass.exam_date.value,
                "examiners": currentClass.examinations.value,
                "semestr_half": currentClass.semestr_half.value,
                "form_name": "CoursesExamStatements"
            },
            async: false,
            success: function (data) {
                let extData = JSON.parse(JSON.stringify(data));
                // Генерировать таблицу и скрыть форму
                currentClass.hideStatementsForm("state_1_form"); // скрываем форму
                // Генерируем содержимое таблицы
                console.log('statements');
                console.log(statements);
                statements.stateTableCourses.data = extData;
                statements.stateTableCourses.generateTable();
                // отображаем таблицу
                statements.stateTableCourses.showStatementsTable();
            },
            error: function (data) {
                //TODO: Вывести уведомление об ошибке
            }
        })
    });
    // Определяем то, как кнопка submit будет отправлять данные на сервер
});

plug_data = {
    "students" : [
        {
            "full_name": "Чернышова И.В",
            "record_book" : 20191302,
            "ticket_number" : -1,
            "eval_result" : {
                "ECTS" : 5,
                "national" : 5
            }
        },
        {
            "full_name": "Отрадская Т.В",
            "record_book" : 20191303,
            "ticket_number" : -1,
            "eval_result" : {
                "ECTS" : 4,
                "national" : 4
            }
        },
        {
            "full_name": "Николаева Е.В",
            "record_book" : 20191304,
            "ticket_number" : -1,
            "eval_result" : {
                "ECTS" : 5,
                "national" : 5
            }
        }
    ],
    "res_marks" : [

    ]
};


// Класс отображающий сгенерированные ведомости
class StatementTable {
    // конструктор класса генерации общей ведемости
    constructor() {
        this.container = document.getElementById("state_1");
        this.tableContainer = document.getElementById("table_1_card-body");
        this.body = document.getElementById("state_1_body"); // Тело, в котором будут хранится студенты с данными о них
        this.data = {};

        // кнопка закрытия таблицы
        this.btn_close = document.getElementById("close_btn_state_1");
        // Инициализируем событие закрытие таблицы
        this.initCloseButton();
    }

    // иницилизирует кнопку закрытия таблицы ведомости
    initCloseButton() {
        let currentClass = this;
        this.btn_close.addEventListener('click', function () {
            currentClass.hideStatementsTable();
            // отображаем форму
            statementsLogic.coursesExamStatements.showStatementsForm();
            // Очищаем таблицу от данных
            currentClass.clearAll();
        })
    }


     // Визуально отображает таблицу
     showStatementsTable() {
        this.tableContainer.classList.remove('hide');
    }

    // Скрывает визуально таблицу
    hideStatementsTable() {
        this.tableContainer.classList.add('hide');
    }

    // добавляет строку в верхнюю часть таблицы со студентами
    addRowUpper(stud, number) {
        console.log(stud);

        let newRow = document.createElement("tr");

        // создаем ячейки
        let numberCell = document.createElement("td");
        let fullNameCell = document.createElement("td");
        let recordBookCell = document.createElement("td");
        let ticketNumberCell = document.createElement("td");
        let ectsCell = document.createElement("td");
        let nationalMarkCell = document.createElement("td");
        let signatureCell = document.createElement("td");

        // добавляем ячейкам стили
        fullNameCell.colSpan = "2";

        // добаляем информацию в ячейки
        numberCell.textContent = number;
        fullNameCell.textContent = stud.full_name;
        recordBookCell.textContent = stud.record_book;
        if (stud.ticket_number != -1) {
            ticketNumberCell.textContent = stud.ticket_number;
        }
        ectsCell.textContent = stud.eval_result.ECTS;
        nationalMarkCell.textContent = stud.eval_result.national;

        // добавляем ячейки в строку
        newRow.appendChild(numberCell);
        newRow.appendChild(fullNameCell);
        newRow.appendChild(recordBookCell);
        newRow.appendChild(ticketNumberCell);
        newRow.appendChild(ectsCell);
        newRow.appendChild(nationalMarkCell);
        newRow.appendChild(signatureCell);

        // добавляем строку в таблицу
        this.body.appendChild(newRow);
    }

    // Генерирует таблицу на основе полученных данных
    generateTable() {
        // генерируем верхнюю часть таблицы
        let currentClass = this;
        let students = this.data.students,
            docHeader = this.data.statements_header,
            docSumHeader = this.data.res_marks;
        let number = 1;

        // Генерирует динамическую часть со студентами и их оценками
        students.forEach(function (stud) {
            currentClass.addRowUpper(stud, number);
            number++;
        })

        // Заполняет верхний заголовок
        this.fillHeader(docHeader);
        // Заполняет нижний заголовок
        this.fillHeaderSum(docSumHeader);
    }

    // Заполняет верхний заголовок документа
    fillHeader(headerData = {}) {
        document.getElementById("state_1_header_edu_period").innerText += " " + headerData.edu_period;
        document.getElementById("state_1_header_cotrolType").innerText += " " + headerData.control_type;
        document.getElementById("state_1_header_specialization").innerText += " " + headerData.specialization;
        document.getElementById("state_1_header_group").innerText += " " + headerData.group;
        document.getElementById("state_1_header_course").innerText += " " + headerData.course;
        document.getElementById("state_1_header_subject").innerText += " " + headerData.subject;
        document.getElementById("state_1_header_examiners").innerText += " " + headerData.lectors;
        document.getElementById("state_1_header_eventDate").innerText += " " + headerData.control_type + "у" +
                                                                                  " " + headerData.event_date;
    }

    // Заполняет нижний заголовок документа
    fillHeaderSum(headerSumData = {}) {
        // Заполняем стандарт ECTS
        document.getElementById("state_1_sum_A").innerText = headerSumData.ECTS_all.A;
        document.getElementById("state_1_sum_B").innerText = headerSumData.ECTS_all.B;
        document.getElementById("state_1_sum_C").innerText = headerSumData.ECTS_all.C;
        document.getElementById("state_1_sum_D").innerText = headerSumData.ECTS_all.D;
        document.getElementById("state_1_sum_E").innerText = headerSumData.ECTS_all.E;
        document.getElementById("state_1_sum_FX").innerText = headerSumData.ECTS_all.FX;
        document.getElementById("state_1_sum_F").innerText = headerSumData.ECTS_all.F;

        // Заполняем национальный стандарт
        document.getElementById("state_1_sum_5").innerText = headerSumData.national_all.five;
        document.getElementById("state_1_sum_4").innerText = headerSumData.national_all.four;
        document.getElementById("state_1_sum_3").innerText = headerSumData.national_all.three;
        document.getElementById("state_1_sum_2").innerText = headerSumData.national_all.two;
        document.getElementById("state_1_sum_1").innerText = headerSumData.national_all.one;
    }

    // Запрашивает данные для создания таблицы
    requireData() {

    }

    // Отрисовка таблицы пользователей
    clearUsers() {
        this.body.innerHTML = "";
    }

    // Очистка таблицы верхнего заголовка
    clearHeader() {
        document.getElementById("state_1_header_edu_period").innerText = "";
        document.getElementById("state_1_header_cotrolType").innerText = "Форма контролю -";
        document.getElementById("state_1_header_specialization").innerText = "Спеціальність";
        document.getElementById("state_1_header_group").innerText = "Група";
        document.getElementById("state_1_header_course").innerText = "Курс";
        document.getElementById("state_1_header_subject").innerText = "Дисципліна";
        document.getElementById("state_1_header_examiners").innerText = "Прізвище, ім'я, по батькові екзаменаторів";
        document.getElementById("state_1_header_eventDate").innerText = "Дата проведення";
    }

    // Очистка таблицы нижнего заголовка
    clearSumHeader() {
        // Заполняем стандарт ECTS
        document.getElementById("state_1_sum_A").innerText = "";
        document.getElementById("state_1_sum_B").innerText = "";
        document.getElementById("state_1_sum_C").innerText = "";
        document.getElementById("state_1_sum_D").innerText = "";
        document.getElementById("state_1_sum_E").innerText = "";
        document.getElementById("state_1_sum_FX").innerText = "";
        document.getElementById("state_1_sum_F").innerText = "";

        // Заполняем национальный стандарт
        document.getElementById("state_1_sum_5").innerText = "";
        document.getElementById("state_1_sum_4").innerText = "";
        document.getElementById("state_1_sum_3").innerText = "";
        document.getElementById("state_1_sum_2").innerText = "";
        document.getElementById("state_1_sum_1").innerText = "";
    }

    // Сбрасывает данные о данных, полученных с сервера
    resetData() {
        this.data = {};
    }

    // Очистка всей таблицы
    clearAll() {
        this.clearUsers();
        this.clearHeader();
        this.clearSumHeader();
    }
}


// Класс отвечающий за работу заглуши на странице ведомостей
class StatementsPlug {
    // конструктор класса
    constructor() {
        this.container = document.getElementsByClassName('statements_list-plug')[0];
    }

    // Отображает заглушку для страницы журналов
    show() {
        this.container.style.display = 'flex';
    }

    // Скрывает заглушку для страницы ведомостей
    hide() {
        this.container.style.display = 'none';
    }
}


/*
* глобальные переменные
* */

let plug = new StatementsPlug();

// Словарь таблиц состояний
let statements = {
    stateTableCourses: new StatementTable()
}
// Объект содержащий все генерируемые таблицы
let statementsLogic = {
    coursesExamStatements: new CoursesExamStatements()
};

// События, которые должны обрабатываться при загрузке страницы
document.addEventListener("DOMContentLoaded", function () {
    plug.hide();
});


/*
* Задаем поведение выбору даты в форме
* */
$(document).ready(function() {
    $('#state_1_form_6').datepicker({
        format: "dd.mm.yyyy",
        maxViewMode: 2,
        todayBtn: "linked",
        language: "ru",
        autoclose: true,
        todayHighlight: true
    });
})

