// Абстрактный класс для таблиц с оценками и посещениями студентов
class JournalList {
    constructor(groupID) {
        // Данные списка оценок и студенты
        this.studentsList = [];
        this.requireStudentsByGroup(groupID);
        this.date = this.requireDate();
    }

    /* Свойства возвращающие данные */
    // Возвращает список студентов
    get getStudents() {
        return this.studentsList;
    }

    // append column to the HTML table
    appendColumn(list, data) {
        let i;
        // open loop for each row and append cell
        let timestampHeader = this.date;
        var text = "";
        for (i = 0; i < list.rows.length; i++) {
            // Если шапка - создать дату
            if (i == 0)
            {
                text = timestampHeader;
            }
            // Если не шапка - создать инпут
            else
            {
                text = "";
            }
            this.createCell(list.rows[i].insertCell(list.rows[i].cells.length), text);
        }
    }

    // генерация таблицы студентов при генерации таблицы оценок
    createStudentsListInBody(tableID, data = []) {
        this.deleteListInBody(tableID);
        let tbody = tableID.getElementsByTagName('tbody')[0];
        for (let i = 0; i < data.length; i++) {
            // Insert a row in the table at the last row
            let newRow   = tbody.insertRow();
            // Insert a cell in the row at index 0
            let newCell  = newRow.insertCell(0);
            // Append a text node to the cell
            let newText  = document.createTextNode(data[i]["second_name"] + " " +data[i]["first_name"]);
            newCell.appendChild(newText);
            newRow.appendChild(newCell);
            tbody.appendChild(newRow);
        }
    }
    

    // Удаляет всех студентов со страницы
    deleteListInBody(tableID, deleteHeaders=false) {
        let tbody = tableID.getElementsByTagName('tbody')[0],
            thead = tableID.getElementsByTagName('thead')[0];
        if (deleteHeaders) {
            thead.innerHTML = '';
        }
        tbody.innerHTML = '';
    }

    // create DIV element and append to the table cell
    createCell(cell, text) {
        let div = document.createElement('div'); // create DIV element

        if (text == "") {
            let input = document.createElement('input');
            input.type = 'text';
            // input.setAttribute('class', 'form-control');
            div.appendChild(input);
        } else {
            let txt = document.createTextNode(text); // create DIV element
            div.appendChild(txt); // append text node to the DIV
        }
        div.setAttribute('class', 'table_marks_cell'); // set DIV class attribute
        cell.appendChild(div); // append DIV to the table cell
    }

    // Запросить данные для новой лекции
    createNewLection(groupID, subjectID, studentsIDs) {
        $.ajax( {
            url: '',
            data: {
                "groupID": groupID,
                "subjectIS": subjectID,
                "studentsIDs": studentsIDs,
            },
            dataType: 'json',
            // Полученные данные добавить в логическое представления и добавить в интерфейс
            success: function (data) {
                // TODO: отображение новой лекции в браузере
            }
        });
    }

    addToVisualList() {
        //TODO: сделать метод который добавит в таблицу данные для новой оценке
        // также должна добавлять визуально столбец с данными
    }


    // Запрос сегодняшней даты
    requireDate() {
        let CurrentObj = this;
        $.ajax({
            url: 'ajax/date_today/',
            data: '',
            datatype: 'json',
            success: function (data) {
                // сохранить сегодняшнюю дату в переменную даты
                // CurrentObj.date = JSON.parse(JSON.stringify(data));
                CurrentObj.date = data.date;
            }
        });
    }


    // Запрос списка студентов
    requireStudentsByGroup(groupID) {
        let CurrentObj =  this;
        $.ajax({
            url: 'ajax/require_students_by_group/',
            data: {
                groupID: groupID
            },
            async: false,
            datatype: 'json',
            success: function (data) {
                CurrentObj.studentsList = JSON.parse(JSON.stringify(data));
                return JSON.parse(JSON.stringify(data));
            }
        })
    }
}

// Список посещений
class AttendanceList extends JournalList{
    constructor(groupID, subjectID) {
        super(groupID);
        // Ссылки на объекты в интефейсе браузера
        this.attendanceListTable = document.getElementById('students_attendance');
        this.studentsListTable = document.getElementById('students_list_attendance');

        // Список посещений
        this.attendancesList = [];
        this.requireAttendance(groupID, subjectID);
        this.createStudentsListInBody(this.studentsListTable, this.studentsList);
    }

    convertDates() {
        let uniqDates = this.getUniqDates();
        for (let i = 0; i < uniqDates.length; i++) {
            uniqDates[i] = moment(uniqDates[i]).format("DD/MM");
        }
        this.attendancesList[this.attendancesList.length - 1].unic_dates = uniqDates;
    }

    // Получает массив уникальных дат лекций
    getUniqDates() {
        if (this.attendancesList.length == 0) {
            return;
        }
        return this.attendancesList[this.attendancesList.length - 1].unic_dates;
    }

    // Запрос посещений студента
    requireAttendance(groupID, subjectID) {
        let currentClass = this;
        $.ajax({
            url: 'ajax/require_journal_attendance/',
            async: false,
            data: {
                groupID: groupID,
                subjectID: subjectID
            },
            datatype: 'json',
            success: function (data) {
                currentClass.attendancesList = JSON.parse(JSON.stringify(data));
            }
        });
    }

    // Позиционирует таблицу для удобства пользователю
    scrollToMaxRight() {
        let table = document.getElementsByClassName('students_attendance_wrapper table-responsive')[0];
        table.scrollLeft = table.scrollWidth;
    }

    // TODO: исправить в соответствии с DRY
    // генерирует список посещений предметов студентами
    generateDataList() {
        this.convertDates();
        this.deleteListInBody(this.attendanceListTable, true);
        // append column to the HTML table
        let i;
        // open loop for each row and append cell
        let text = "";

        let tbody = this.attendanceListTable.getElementsByTagName("tbody")[0];
        let thead = this.attendanceListTable.getElementsByTagName("thead")[0];
        tbody.insertRow();
        let newRow = thead.insertRow();
        // создает двумерную таблицу с оценками
        for (let i = 0; i < this.attendancesList[0].length; i++) {
            let rowCounter = 1;
            for (let j = -1; j < this.attendancesList.length - 1; j++) {
                // Если шапка - создать дату
                // console.log("i:" + i + " j:" + j + " this.marksList[i][j]:" + this.marksList[j + 1][i].mark);
                if (j == -1)
                {
                    text = this.attendancesList[this.attendancesList.length - 1].unic_dates[i];
                    let newTextNode  = document.createTextNode(text);
                    let newCell = newRow.insertCell(this.attendanceListTable.rows[0].length);
                    newCell.classList.add("dateJournal");

                    newCell.appendChild(newTextNode);
                    newRow.appendChild(newCell);
                    thead.appendChild(newRow);
                }
                // Если не шапка - создать инпут
                else
                {
                    text = this.attendancesList[j][i].presence;
                    this.attendanceListTable.insertRow();
                    this.createAttendanceCell(this.attendanceListTable.rows[rowCounter].insertCell(this.attendanceListTable.rows[rowCounter].cells.length), text);
                    rowCounter++;
                }
            }
        }
        this.createToolTipForLast();
        this.scrollToMaxRight();
    }

    // создает ячейку в которой прописывается оценка
    createAttendanceCell(cell, isAttend) {
        let div = document.createElement('div');
        let text = "";
        if (isAttend) {
            text = '\xa0';
            div.setAttribute('class', 'cell_attendance');
        }
        else {
            text = "Н";
            div.setAttribute('class', 'cell_attendance_clickable');
        }
        div.appendChild(document.createTextNode(text));
        cell.appendChild(div);
    }

    // Добавляет новую ячейку с оценками
    createNewAttendances() {
        // добавить логически посещения
        let students = [],
            groupID = journalSelector.groupID,
            subjectID = journalSelector.subjectID;

        for (let i = 0; i < this.studentsList.length; i++) {
            students.push(JSON.stringify(this.studentsList[i]));
        }

        let CurrentObj = this;
        $.ajax({
            url: 'ajax/create_new_attendances/',
            type: 'POST',
            async: false,
            data: {
                "students[]": students,
                "groupID": groupID,
                "subjectID": subjectID,
            },
            dataType: 'json',
            success: function(data) {

            },
            error: function () {
                // Вызвать всплывающее сообщение которое сообщит о превышении кол-во пар за день
                notifications.showNotification('Ошибка', 'Вы превысили допустимое кол-во пар');
            }
        });
        this.requireAttendance(groupID, subjectID);
        CurrentObj.generateDataList();
        // Добавить графически посещение
        // open loop for each row and append cell
        // this.requireDate();
        // let text = this.date;
        //
        // let tbody = this.attendanceListTable.getElementsByTagName("tbody")[0];
        // let thead = this.attendanceListTable.getElementsByTagName("thead")[0];
        // let row = thead.getElementsByTagName("tr")[0];
        // // TODO: исправить по принципу DRY
        // // Создает новую колонку оценок
        // let rowCounter = 1;
        // for (let j = -1; j < this.attendancesList.length - 1; j++) {
        //     // Если шапка - создать дату
        //     if (j == -1)
        //     {
        //         let newTextNode  = document.createTextNode(text);
        //         let newCell = row.insertCell(this.attendanceListTable.rows[0].length);
        //
        //         newCell.appendChild(newTextNode);
        //         row.appendChild(newCell);
        //         thead.appendChild(row);
        //     }
        //     // Если не шапка - создать инпут
        //     else
        //     {
        //         text = true;
        //         this.attendanceListTable.insertRow();
        //         this.createAttendanceCell(this.attendanceListTable.rows[rowCounter].insertCell(this.attendanceListTable.rows[rowCounter].cells.length), text);
        //         rowCounter++;
        //     }
        // }
    }

    // Создает всплывающее окно последнему элементу если его можно удалить
    createToolTipForLast() {
        let count = 0;
        let datesHeadersInAttandance = document.getElementsByClassName("dateJournal");
        let arrDatesHeaders = Array.from(datesHeadersInAttandance);
        // создаем элемент tooltip
        $(arrDatesHeaders[arrDatesHeaders.length - 1]).tooltip({
            html: true,
            placement: 'top',
            trigger: 'click',
            sanitize  : false, // here it is
            title: '<button type="button" class="btn btn-danger deleteAttendances"> Удалить </button>',
        });
    }
}


// Служит для создания объекта оценки
class Mark {
    constructor(mark = 1, date='') {
        this.mark = mark;
        this.date = new Date(date);
    }
}


// служит для создания объекта студента
class Student {
    constructor(firstName='', secondName='', marks = []) {
        this.firstName = '';
        this.secondName = '';
        this.marks = [...marks];
    }

    // П
    setMarks(marks) {
        let temp = [];
        for (let i = 0; i < marks.length; i++) {
            temp.push(new Date(marks[i]));
        }

        return temp;
    }
}


// Отвечает за список оценок и студенов для этого списка
class MarkList extends JournalList{
    constructor(groupID, subjectID) {
        super(groupID);
        // Ссылки на объекты в интефейсе браузера
        this.marksListTable = document.getElementById('marks_list_marks');
        this.studentsListTable = document.getElementById('students_list_marks');
        this.marks = [];

        // Список оценок
        this.requireMarks(groupID, subjectID);
        this.createStudentsListInBody(this.studentsListTable, this.studentsList);
    }

    // Свойство возвращающее набор оценок
    get marksList() {
        return this.marks;
    }

    // Запрос оценок из БД
    requireMarks(groupID, subjectID) {
        let CurrentObj = this;
        $.ajax({
            url: 'ajax/require_journal_marks/',
            async: false,
            data: {
                groupID: groupID,
                subjectID: subjectID
            },
            datatype: 'json',
            success: function (data) {
                CurrentObj.marks = JSON.parse(JSON.stringify(data));
            }
        });
    }

    // Выравнивает таблицу вправо при добавлении значений
    scrollToMaxRight() {
        let table = document.getElementsByClassName('students_marks_wrapper table-responsive')[0];
        table.scrollLeft = table.scrollWidth;
    }

    // TODO: исправить в соответствии с DRY
    // Генерировать список данных (Таблицу оценок)
    generateDataList() {
        this.deleteListInBody(this.marksListTable, true);
        let text = "";

        let tbody = this.marksListTable.getElementsByTagName("tbody")[0];
        let thead = this.marksListTable.getElementsByTagName("thead")[0];
        let newRow = thead.insertRow();
        // создает двумерную таблицу с оценками
        let studsAndMarks = this.marksList.students,
            topics = this.marksList.topics;
        let topicNumber = 0;

        // заполняем шапку наименований проведенных работ
        for (let i = 0; i < topics.length; i++) {
            text = topics[i].control_type;
            let newTextNode  = document.createTextNode(text);
            let newCell = thead.rows[0].insertCell(this.marksListTable.rows[0].length);
            newCell.classList.add('dateJournalMarks');

            newCell.appendChild(newTextNode);
            thead.rows[0].appendChild(newCell);
        }

        // Создаем строки соответствующие кол-ву студентов
        for (let i = 0; i < studsAndMarks.length; i++) {
            tbody.insertRow();
        }

        // Перебираем студентов
        for (let i = 0, rowCounter = 1; i < studsAndMarks.length; i++, rowCounter++) {
            let stud = studsAndMarks[i];
            // Перебираем его оценки
            for (let j = 0; j < stud.marks.length; j++) {
                text = stud.marks[j].mark;
                this.createMarkCell(tbody.rows[i].insertCell(j), text);
            }
        }
        this.createToolTipAll(topics);
        this.scrollToMaxRight();
    }

    // создает ячейку в которой прописывается оценка
    createMarkCell(cell, data) {
        let input = document.createElement('select'); // create DIV element
        this.сreateDropdownOptions(input);
        input.value = data;
        input.classList.add('table_marks_cell');
        // div.setAttribute('class', 'table_marks_cell'); // set DIV class attribute
        cell.appendChild(input); // append DIV to the table cell
    }

    // Генерирует набор параметров для выпадающего списка выбора оценки
    сreateDropdownOptions(filler, count = 5) {
        // генерируем оценки, которые можно поставить
        for (let i = 0; i < count; i++) {
            // создаем выборочный элемент
            let opt = document.createElement('option');
            opt.value = count - i;
            opt.innerText = count - i;

            // Добавляем сгенерированный элемент в конец
            filler.appendChild(opt);
        }
        // добавляем элемент очистки оценки
        let emptyOpt = document.createElement('option');
        emptyOpt.value = null;
        emptyOpt.innerText = "Нет оценки";
        filler.appendChild(emptyOpt);
    }

    //
    searchEndSetMark() {

    }

    // Добавляет новую ячейку с оценками
    createNewMarks(extData) {
        let groupID = journalSelector.groupID,
            subjectID = journalSelector.subjectID;
        // Временное решение
        this.requireMarks(groupID, subjectID);
        this.generateDataList();
        // добавить логически оценку
        // FIXME: исправить ticket #12B ради оптимизации. Сейчас - временное решение
        // let CurrentObj = this;
        // $.ajax({
        //     url: 'ajax/require_last_mark_students_in_journalList/',
        //     type: 'GET',
        //     data: {
        //         "groupID": groupID,
        //         "subjectID": subjectID,
        //     },
        //     dataType: 'json',
        //     success: function(data) {
        //         console.log('createNewMarks');
        //         console.log('data');
        //         console.log(data);
        //         // // Добавить оценку логически
        //         // for (let i = 0; i < CurrentObj.marks.students.length; i++) {
        //         //     CurrentObj.marks.students[i].marks.push(data.students[i].marks[0]);
        //         // }
        //         // // Добавляем
        //         // console.log('CurrentObj.marks');
        //         // console.log(CurrentObj.marks);
        //         // CurrentObj.marks.topics.push(data.topics);
        //         //
        //         // // отрисовать информацию в журнале
        //         // // Добавить графически оценку
        //         // let i;
        //         // let text = "";
        //         //
        //         // let tbody = CurrentObj.marksListTable.getElementsByTagName("tbody")[0];
        //         // let thead = CurrentObj.marksListTable.getElementsByTagName("thead")[0];
        //         // // создает двумерную таблицу с оценками
        //         // let studsAndMarks = data.students,
        //         //     topics = data.topics;
        //         // let topicNumber = 0;
        //         //
        //         // // заполняем шапку наименований проведенных работ
        //         // text = topics.control_type;
        //         // let newTextNode  = document.createTextNode(text);
        //         // let newCell = thead.rows[0].insertCell(-1);
        //         //
        //         // newCell.appendChild(newTextNode);
        //         // thead.rows[0].appendChild(newCell);
        //         //
        //         // // Перебираем студентов
        //         // for (let i = 0, rowCounter = 1; i < studsAndMarks.length; i++, rowCounter++) {
        //         //     let stud = studsAndMarks[i];
        //         //     // tbody.insertRow();
        //         //     // Перебираем его оценки
        //         //     text = stud.marks.mark;
        //         //     CurrentObj.createMarkCell(tbody.rows[i].insertCell(stud.marks.length - 1), text);
        //         // }
        //     },
        //     error: function (data) {
        //         //TODO: вывести сообщение об ошибке
        //     }
        // })
    }

    // Создает всплывающее окно последнему элементу если его можно удалить
    createToolTipAll(topics = []) {
        let datesHeadersInAttandance = document.getElementsByClassName("dateJournalMarks");
        let arrDatesHeaders = Array.from(datesHeadersInAttandance);
        let count = 0;
        arrDatesHeaders.forEach(function (element) {
            // делать кнопку и ИД если дата сегодняшняя
            // Создаем с кнопкой удаления
            if (moment(topics[count].date_provide).format('YYYY-MM-DD') == moment().format('YYYY-MM-DD')) {
                $(element).tooltip({
                    html: true,
                    placement: 'top',
                    trigger: 'click',
                    sanitize  : false, // here it is
                    title: '<span class="work_id">' + topics[count].topicID + '</span> <h6>' + topics[count].title + '</h6><p>' + moment(topics[count].date_provide).format("DD/MM/YY")  + '</p><button type="button" class="btn btn-danger deleteWork"> Удалить </button>',
                });
                // создаем без кнопки удаления
            } else {
                $(element).tooltip({
                    html: true,
                    placement: 'top',
                    trigger: 'click',
                    sanitize  : false, // here it is
                    title: '<h6>' + topics[count].title + '</h6><p>' + moment(topics[count].date_provide).format("DD/MM/YY")  + '</p>',
                });
            }
            count++;
        });
    }
}


// Асинхронщина для таблиц журналов
// Верхние инпуты в таблице журналов
class JournalSelectors {
    constructor() {
        this.courseSelector = document.getElementById('inputGroupSelectCourse');
        this.groupSelector = document.getElementById('inputGroupSelectGroup');
        this.subjectSelector = document.getElementById('inputGroupSelectSubject');


        let currentClass = this;
        // Связанные выпадающие списки
        document.addEventListener('DOMContentLoaded', function (event){
            let courseDropDown = currentClass.courseSelector,
                groupDropDown = currentClass.groupSelector,
                subjectDropDown = currentClass.subjectSelector;

            // для списка курсов
            courseDropDown.addEventListener('change', function () {
                let courseID = parseInt(this.value);
                if (!isNaN(courseID)) {
                    // Запрашиваем группы
                    $.ajax({
                        url: 'ajax/get_groups_by_courseID/',
                        data: {
                            courseID: courseID
                        },
                        async: false,
                        dataType: 'json',
                        success: function (data) {
                            groupDropDown.innerHTML = '<option value="" selected>Выберите группу</option>';
                            let subjects = JSON.parse(JSON.stringify(data));
                            subjects.forEach(function (subject) {
                                // заполнить группы значениями
                                let newOption = document.createElement('option');
                                newOption.appendChild(document.createTextNode(subject.number));
                                newOption.value = subject.pk;
                                groupDropDown.appendChild(newOption);
                            })
                        }
                    });
                } else {
                    // Запросить курсы
                    courseDropDown.innerHTML = '<option value="" selected>Курс</option>';
                    groupDropDown.innerHTML = '<option value="" selected>Группа</option>';
                    $.ajax({
                        url: 'ajax/get_educated_courses/',
                        data: {},
                        success: function (data) {
                            let courses = JSON.parse(JSON.stringify(data));
                            courses.forEach(function (course) {
                                // заполнить группы значениями
                                let newOption = document.createElement('option');
                                newOption.appendChild(document.createTextNode(course.name));
                                newOption.value = course.pk;
                                courseDropDown.appendChild(newOption);
                            })
                        }
                    });
                }
            });

            // Для списка групп
            groupDropDown.addEventListener('change', function () {
                // Очистить список предметов
                currentClass.subjectSelector.innerHTML = '<option value="" selected>Выберите предмет</option>';
                let groupID = parseInt(this.value);
                if (!isNaN(groupID)) {
                    $.ajax({
                        url: 'ajax/get_subjects_by_groupID/',
                        data: {
                            'groupID': groupID
                        },
                        success: function (data) {
                            let subjects = JSON.parse(JSON.stringify(data));
                            subjects.forEach(function (subject) {
                                // заполнить группы значениями
                                let newOption = document.createElement('option');
                                newOption.appendChild(document.createTextNode(subject.name));
                                newOption.value = subject.pk;
                                subjectDropDown.appendChild(newOption);
                            })
                        }
                    });
                }
            })

            // Для списка предметов
            subjectDropDown.addEventListener('change', function () {
                // TODO: сделать получение данных для таблицы
                let groupID = journalSelector.groupID,
                    subjectID = journalSelector.subjectID;
                markList = new MarkList(groupID, subjectID);
                attendanceList = new AttendanceList(groupID, subjectID);

                // скрыть заглушку
                hideJournalPlug();
                // Сгенерировать таблицы в графике
                markList.generateDataList();
                attendanceList.generateDataList();

                // Показать журналы
                showJournals();
            })
        });
    }

    // Получение идентификаторов хранимых элементов
    get courseID() {
        this.updateProperies();
        return this.courseSelector.value;
    }

    get groupID() {
        this.updateProperies();
        return this.groupSelector.value;
    }

    get subjectID() {
        this.updateProperies();
        return this.subjectSelector.value;
    }


    // Обновляет свойства класса
    updateProperies() {
        this.courseSelector = document.getElementById('inputGroupSelectCourse');
        this.groupSelector = document.getElementById('inputGroupSelectGroup');
        this.subjectSelector = document.getElementById('inputGroupSelectSubject');
    }

    // Обновляет даннные своих объектов (Свои свойства)
    updateInputData(caller = '') {
        let ClassCaller = this;
        // Когда вызывает "course" - подгрузить группы
        if (caller.toLowerCase() == 'course') {
            // Очистить список предметов
            // Удаляем старые узлы
            let i = 0;
            ClassCaller.subjectSelector.childNodes.forEach(function (node) {
                    if (i > 2){
                        node.remove();
                    }
                    i++;
                }
            )
            $.ajax({
                url: 'ajax/get_groups_by_courseID/',
                data: {
                    courseID: this.courseID
                },
                async: false,
                dataType: 'json',
                success: function (data) {
                    // TODO: Отобразить группы в браузере
                    let groups = "";
                    groups = JSON.parse(data);
                    // Удаляем старые узлы
                    let i = 0;
                    ClassCaller.groupSelector.childNodes.forEach(function (node) {
                            if (i > 2){
                                node.remove();
                            }
                            i++;
                        }
                    );
                    // Создаем новые
                    groups.forEach(function (group) {
                        let opt = document.createElement('option');
                        opt.appendChild(document.createTextNode(group.fields.number));
                        opt.value = group.pk;
                        ClassCaller.groupSelector.appendChild(opt);
                    });
                }
            });
        }
        // Когда вызывает "group" загрузить список предметов
        else if (caller.toLowerCase() == 'group') {
            $.ajax({
                url: 'ajax/get_subjects/',
                data: { groupId: this.groupID },
                dataType: 'json',
                async: false,
                success: function (data) {
                    let subjects = "";
                    subjects = JSON.parse(data);

                    // Удаляем старые узлы
                    let i = 0;
                    ClassCaller.subjectSelector.childNodes.forEach(function (node) {
                            if (i > 2){
                                node.remove();
                            }
                            i++;
                        }
                    )
                    // Создаем новые
                    subjects.forEach(function (subject) {
                        let opt = document.createElement('option');
                        opt.appendChild(document.createTextNode(subject.fields.name));
                        opt.value = subject.pk;
                        ClassCaller.subjectSelector.appendChild(opt);
                    });
                }
            });
        }
        // начать загружать таблицу из БД опираясь на course и group
        else {
            // TODO: сделать получение данных для таблицы
            let groupID = journalSelector.groupID,
                subjectID = journalSelector.subjectID;
            markList = new MarkList(groupID, subjectID);
            attendanceList = new AttendanceList(groupID, subjectID);

            // скрыть заглушку
            hideJournalPlug();
            // Сгенерировать таблицы в графике
            markList.generateDataList();
            attendanceList.generateDataList();

            // Показать журналы
            showJournals();
        }
    }
}

/*
* Глобальные переменные
* */
journalSelector = new JournalSelectors();
groupID = journalSelector.groupID;
subjectID = journalSelector.subjectID;


let markList;
let attendanceList;

// Отображает журналы на странице журналов
function showJournals() {
    let journals = document.getElementsByClassName('journal_list')[0];
    journals.style.display = 'flex';
}

// Отображает заглушку для страницы журналов
function showJournalPlug() {
    let plug = document.getElementsByClassName('journal_list-plug')[0];
    plug.style.display = 'flex';
}

// Скрывает заглушку для страницы журнала
function hideJournalPlug() {
    let plug = document.getElementsByClassName('journal_list-plug')[0];
    plug.style.display = 'none';
}

// Попробовать запросить последний рабочий журнал
document.addEventListener("DOMContentLoaded", function () {
    $.ajax({
        url: 'ajax/get_last_required_journal/',
        type: 'GET',
        datatype: 'json',
        success: function (data) {
            // если запись была найдена - установить все в journalSelector и запросить журналы
            let courseID = data.courseID,
                groupID = data.groupID,
                subjectID = data.subjectID;
            let courseDropDown = journalSelector.courseSelector,
                groupDropDown = journalSelector.groupSelector,
                subjectDropDown = journalSelector.subjectSelector;
            // journalSelector.updateInputData('course');
            // journalSelector.updateInputData('group');
            $.ajax({
                url: 'ajax/get_groups_by_courseID/',
                data: {
                    courseID: courseID
                },
                async: false,
                dataType: 'json',
                success: function (data) {
                    groupDropDown.innerHTML = '<option value="" selected>Выберите группу</option>';
                    let subjects = JSON.parse(JSON.stringify(data));
                    subjects.forEach(function (subject) {
                        // заполнить группы значениями
                        let newOption = document.createElement('option');
                        newOption.appendChild(document.createTextNode(subject.number));
                        newOption.value = subject.pk;
                        groupDropDown.appendChild(newOption);
                    })
                }
            });
            $.ajax({
                url: 'ajax/get_subjects_by_groupID/',
                data: {
                    'groupID': groupID
                },
                async: false,
                success: function (data) {
                    let subjects = JSON.parse(JSON.stringify(data));
                    subjects.forEach(function (subject) {
                        // заполнить группы значениями
                        let newOption = document.createElement('option');
                        newOption.appendChild(document.createTextNode(subject.name));
                        newOption.value = subject.pk;
                        subjectDropDown.appendChild(newOption);
                    })
                }
            });


            journalSelector.courseID = courseID;
            journalSelector.groupID = groupID;
            journalSelector.subjectID = subjectID;

            // установить значения journalSelector"а в значения соответствующими данными
            let journalSelectorCourseLength = journalSelector.courseSelector.options.length,
                journalSelectorGroupLength = journalSelector.groupSelector.options.length,
                journalSelectorSubjectLength = journalSelector.subjectSelector.options.length;

            // Установить значения для курса визуально
            for (let j = 0; j < journalSelectorCourseLength; j++) {
                if (journalSelector.courseSelector.options[j].value == courseID) {
                    journalSelector.courseSelector.options[j].selected = true;
                    break;
                }
            }
            // Установить значения для группы визуально
            for (let j = 0; j < journalSelectorGroupLength; j++) {
                if (journalSelector.groupSelector.options[j].value == groupID) {
                    journalSelector.groupSelector.options[j].selected = true;
                    break;
                }
            }
            // Установить значения для предмета визуально
            for (let j = 0; j < journalSelectorSubjectLength; j++) {
                if (journalSelector.subjectSelector.options[j].value == subjectID) {
                    journalSelector.subjectSelector.options[j].selected = true;
                    break;
                }
            }

            markList = new MarkList(groupID, subjectID);
            attendanceList = new AttendanceList(groupID, subjectID);

            // скрыть заглушку
            hideJournalPlug();

            // Сгенерировать таблицы в графике
            markList.generateDataList();
            attendanceList.generateDataList();

            // Показать журналы
            showJournals();
        },
        error: function () {
            // Если запись не была найдена - отобразить заглушку
            showJournalPlug();
        }
    })
});


// Агрегатор событий для изменения состояния посещения студентов лекций
let attendanceTable = document.getElementById('students_attendance');
attendanceTable.addEventListener('click', function (event) {
    // ишем необходимые нам элементы нажатия
    if (event.target.tagName == 'DIV') {
        // Получить строку и колонку вызова
        let rowNumber = event.target.closest('tr').rowIndex,
            colNumber = parseInt(event.target.closest('td').cellIndex, 10);

        // Узнать кто там находится и какая это дата
        let clickedStudent = attendanceList.studentsList[rowNumber - 1];
        let selectedAttendance = attendanceList.attendancesList[rowNumber - 1][colNumber];

        // Отправить данные не бек и проверить можно ли их изменять
        if (event.target.className == "cell_attendance_clickable") {
            // попробовать изменить посещение студента в эту дату
            // Попробовать изменить состояние
            $.ajax({
                url: 'ajax/change_attendance_status/',
                type: 'POST',
                data: {
                    "attendanceID": selectedAttendance.id,
                    "date": selectedAttendance.date_added,
                    "student": JSON.stringify(clickedStudent),
                    "subjectID": journalSelector.subjectID,
                    "presence": true
                },
                datatype: 'json',
                success: function () {
                    // Если операция успешно прошла - оставить визуализацию как надо
                    event.target.className = "cell_attendance";
                    event.target.textContent = '\xa0';
                },
                error: function () {
                    // Иначе изменить на противоположное состояние
                    event.target.className = "cell_attendance_clickable";
                    event.target.textContent = 'Н';
                }
            })
            // Если изменилось - сменить на противополжные класс
            event.target.className = "cell_attendance";
            event.target.textContent = '\xa0';
        } else if (event.target.className == "cell_attendance") {
            // попробовать изменить посещение студента в эту дату
            // Попробовать изменить
            // Попробовать изменить состояние
            $.ajax({
                url: 'ajax/change_attendance_status/',
                type: 'POST',
                data: {
                    "attendanceID": selectedAttendance.id,
                    "date": selectedAttendance.date_added,
                    "student": JSON.stringify({
                            pk: clickedStudent.pk,
                            first_name: clickedStudent.first_name,
                            second_name: clickedStudent.second_name
                        }),
                    "subjectID": journalSelector.subjectID,
                    "presence": false
                },
                datatype: 'json',
                success: function () {
                    // Если операция успешно прошла - оставить визуализацию как надо
                    event.target.className = "cell_attendance_clickable";
                    event.target.textContent = 'Н';
                },
                error: function () {
                    // Иначе изменить на противоположное состояние
                    event.target.className = "cell_attendance";
                    event.target.textContent = '\xa0';
                }
            })
            // Если изменилось - сменить на противополжные класс
            event.target.className = "cell_attendance_clickable";
            event.target.textContent = 'Н';
        }
    }
});

// Агрегатор событий для изменения оценок студентов к соответствующим лекциям
let marksListTable = document.getElementById('marks_list_marks');
marksListTable.addEventListener('change', function (event) {
    // Получить строку и колонку вызова
    let rowNumber = event.target.closest('tr').rowIndex,
        colNumber = parseInt(event.target.closest('td').cellIndex, 10);
    // Узнать кто там находится и какая это дата
    let clickedStudent = markList.studentsList[rowNumber - 1];
    let clickedTopic = markList.marks.topics[colNumber];

    console.log('event.target.value');
    console.log(event.target.value);

    // Удаление оценки
    if (event.target.value == "null") {
        console.log("Удаляем");
        let mark = markList.marks.students[rowNumber - 1].marks.mark;
        // отправка запроса на удаление оценки
        $.ajax({
            url: 'ajax/delete_student_mark/',
            type: 'POST',
            data: {
                "student": JSON.stringify(clickedStudent),
                "subjectID": journalSelector.subjectID,
                "mark": mark,
                'topicID': clickedTopic.topicID
            },
            datatype: 'json',
            success: function () {
                console.log("Удалено");
            },
            error: function () {
                console.log("Не удалено");
            }
        })
    }
    // Присвоение или добавление оценки
    else if (event.target.value != null) {
        console.log("Добавляем");
        let mark = event.target.value;
        if (mark != null && mark <= 5 && mark >= 1) {
            $.ajax({
                url: 'ajax/change_student_mark/',
                type: 'POST',
                data: {
                    "student": JSON.stringify(clickedStudent),
                    "subjectID": journalSelector.subjectID,
                    "mark": mark,
                    'topicID': clickedTopic.topicID
                },
                datatype: 'json',
                success: function () {
                    console.log("Добавлено");
                },
                error: function () {
                    console.log("Не Добавлено");
                    event.target.value = null;
                }
            })
        } else {
            event.target.value = null;
        }
    }
});

// Вешаем событие уделаения последней лекции по предмету
document.addEventListener('click', function (event) {
    if (event.target.tagName == 'BUTTON' && event.target.className == 'btn btn-danger deleteAttendances') {
        console.log('Удаление лекции');
        let groupID = journalSelector.groupID,
            subjectID = journalSelector.subjectID;
        // Пробуем удалить работу
        $.ajax({
            url: 'ajax/delete_last_attendances/',
            data: {
                "groupID": groupID,
                "subjectID": subjectID,
            },
            dataType: 'json',
            type: 'POST',
            async: false,
            success: function () {
                // Удаляем tooltip
                let tooltip = event.target.closest('.tooltip');
                tooltip.remove();
                // Запрашиваем новые записи посещений
                attendanceList.requireAttendance(groupID, subjectID);
                // Создаем новую таблицу посещений
                attendanceList.generateDataList();
            },
            error: function () {
                notifications.showNotification('Ошибка', 'В данной лекции есть отметки отсутствия');
            }
        });
    }
})

// Навешиваем событие удаления последней проведенной работы
document.addEventListener('click', function (event) {
    if (event.target.tagName == 'BUTTON' && event.target.className == 'btn btn-danger deleteWork') {
        let groupID = journalSelector.groupID,
        subjectID = journalSelector.subjectID;
        // Пробуем удалить работу
        $.ajax({
            url: 'ajax/cancel_last_test_work/',
            data: {
                "groupID": groupID,
                "subjectID": subjectID,
            },
            dataType: 'json',
            type: 'POST',
            async: false,
            success: function () {
                // Удаляем tooltip
                let tooltip = event.target.closest('.tooltip');
                tooltip.remove();
                markList.requireMarks(groupID, subjectID);
                markList.generateDataList();
            },
            error: function () {
                notifications.showNotification('Ошибка', 'В данной работе есть оценки');
            }
        });
    }
})
