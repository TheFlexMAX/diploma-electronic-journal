/*
* Модальное окно
* */
// Get the modal
let btnChooser = document.getElementById('btnCaller');

let modalChooser = document.getElementById("modalTestChooser");

// Get the <span> element that closes the modal
let spanChooser = document.getElementsByClassName("close")[0];


// TODO: исправить в соответствии с DRY
// Логическая работа модального окна конструктора журнала
class TestPlanner {
    constructor(subject, courseID = null, groupID = null) {
        this.currentSubject = subject;
        this.currentCourseID = courseID;
        this.currentGroupID = groupID;
        this.tickets = [];
        this.ticketsContainer = document.getElementById('testList-ticketContainer');
    }

    // глубинное копирование переданных тикетов
    set setTickets(extTickets) {
        let CurrentClass = this;
        extTickets.forEach(function (ticket) {
            CurrentClass.push(JSON.parse(JSON.stringify(ticket)));
        })
    }

    set courseID(extCourseID) {
        this.currentCourseID = extCourseID;
    }

    set groupID(extGroupID) {
        this.currentGroupID = extGroupID;
    }

    get getGroupID() {
        return this.currentGroupID;
    }

    get getCourseID() {
        return this.currentCourseID;
    }

    get getSubjectID() {
        return this.currentSubject.pk;
    }

    getTicketByID(id) {
        this.tickets.forEach(function (ticket) {
            if (ticket.topic_pk == id) {
                return ticket;
            }
        })
    }

    // Удаляет один тикет по индентификатору
    deleteTicketByID(id) {
        let index = 0;
        this.tickets.forEach(function (ticket, i) {
            if (ticket.topic_pk == id) {
                index = i;
            }
        });
        console.log('До удаления элемента');
        console.log('this.tickets');
        console.log(this.tickets);
        this.tickets.splice(index, 1);
        console.log('После удаления элемента');
        console.log('this.tickets');
        console.log(this.tickets);
    }

    // Запрашивает запланированные работы из back-end
    requireTickets() {
        if (this.currentGroupID != null && this.currentGroupID != null) {
            let CurrentClass = this;
            $.ajax({
                url: 'ajax/require_tickets/',
                datatype: 'json',
                async: false,
                data: {
                    'subjectID': this.currentSubject.pk,
                    'groupID': this.currentGroupID
                },
                success: function (data) {
                    CurrentClass.resetTickets();
                    CurrentClass.tickets = JSON.parse(JSON.stringify(data));
                    // получили - отрисовали новые тикеты
                    // CurrentClass.clearTicketsContainer();
                    CurrentClass.updateTicketsContainer();
                }
            })
        }
    }

    //


    // Визуальная отрисовка запланированных работ
    updateTicketsContainer() {
        let tickets = this.tickets;
        let CurrentClass = this;
        tickets.forEach(function (ticket) {
            CurrentClass.appendGUITicket(ticket.title, ticket.control_type, ticket.topic_pk);
        })
    }

    // Визуальное затирание запланированных работ
    clearTicketsContainer() {
        this.ticketsContainer.innerHTML = null;
    }

    // устанавливает новый текущий объект
    setCurrentSubject() {

    }

    // Возвращает установленный текущий объект
    getCurrentSubject() {

    }

    // Создает новый элемент не запланированной работы
    createNewTicket() {
        // Получить данные из инпутов
        let inputContainer = document.getElementsByClassName('modal-body-costructor-controller-wide')[0];
        let titleInput = inputContainer.getElementsByTagName('input')[0];
        let controlTypeInput = inputContainer.getElementsByTagName('input')[1];
        let CurrentClass = this;
        let description = '';
        let isScheduled = false;
        // проверить заполнены ли поля
        if (titleInput.value != '' && controlTypeInput.value != '') {
            // Создать новую не запланированную работу и провести её
            $.ajax({
                url: 'ajax/create_new_ticket/',
                type: 'POST',
                async: false,
                data: {
                    'groupID': CurrentClass.getGroupID,
                    'subjectID': CurrentClass.getSubjectID,
                    'description': description,
                    'title': titleInput.value,
                    'controlType': controlTypeInput.value,
                    'isScheduled': isScheduled
                },
                // запрашиваем последнюю проведенную работу для предмета и журнала с которым работают
                success: function (data) {
                    // CurrentClass.appendGUITicket(titleInput.value, controlTypeInput.value);
                    let lastTopic;
                    $.ajax({
                        url: 'ajax/require_last_performed_topic_for_group/',
                        type: 'GET',
                        async: false,
                        data: {
                            'groupID': CurrentClass.getGroupID,
                            'subjectID': CurrentClass.getSubjectID,
                        },
                        // Зафиксировать работу, провести тест и отрисовать во фронте
                        success: function (data) {
                            // Очистить поля ввода
                            titleInput.value = "";
                            controlTypeInput.value = "";

                            lastTopic = JSON.parse(JSON.stringify(data));
                            // Создать пустые оценки для студентов
                            let subjectID = journalSelector.subjectID,
                                courseID = journalSelector.courseID,
                                groupID = journalSelector.groupID;
                            $.ajax({
                                url: 'ajax/give_a_test/',
                                method: 'POST',
                                async: false,
                                data: {
                                    "topicID": lastTopic.topicID,
                                    "subjectID": subjectID,
                                    "courseID": courseID,
                                    "groupID": groupID,
                                },
                                datatype: 'json',
                                success: function (data) {
                                    console.log('Провели работу');
                                    // закрыть модальное окно
                                    // отрисовать новую зафиксированную запись отметки в журнале с оценками
                                    markList.createNewMarks();
                                },
                                error: function () {
                                    // TODO: ничего с ним не делать
                                    // Сообщить, что удаление не произошло
                                    console.log('Не провели работу из-за сервера');
                                }
                            });
                        }
                    })
                }
            });
        }
        // TODO: вывести сообщение о просьбе заполнить поля
        else {

        }
    }

    // добавляет успешно добавленное заланированное мероприятие в визуальный интерфейс
    appendGUITicket(titleStr='', controlTypeStr='', pk = 0) {
        let container = this.ticketsContainer;

        let ticketContainer = document.createElement('div'),
            ticketInfo = document.createElement('div'),
            ticketID = document.createElement("span");
        let closeBtn = document.createElement('span');

        let title = document.createElement('p'),
            controlType = document.createElement('p');
        title.textContent = titleStr;
        controlType.textContent = controlTypeStr;
        ticketID.textContent = pk;
        closeBtn.textContent = '×';

        ticketInfo.appendChild(ticketID);
        ticketInfo.appendChild(title);
        ticketInfo.appendChild(controlType);
        ticketContainer.appendChild(ticketInfo);
        ticketContainer.appendChild(closeBtn);

        ticketID.className = 'card-detail-id-hide';
        ticketContainer.className = 'modal-body-testList-ticket ticket-choosable';
        ticketInfo.className = 'testList-ticket-info';
        closeBtn.className = 'testList-ticket-delete';

        container.appendChild(ticketContainer);
    }

    // добавляет логически ticket в конец
    appendTicket(titleStr='', controlTypeStr='', description=null) {
        this.tickets.push({
            "title": titleStr,
            "description": description,
            "control_type": controlTypeStr
        })
    }

    // удаляет тикеты визуально
    resetTicketsGUI() {
        this.ticketsContainer.innerHTML = "";
    }

    resetTicketsLogic() {
        this.tickets = [];
    }

    resetTickets() {
        this.resetTicketsGUI();
        this.resetTicketsLogic();
    }

    // удаляет данные логически
    resetLogic() {
        this.currentSubject = 0;
        this.currentGroupID = 0;
        this.currentSubject = 0;
        this.tickets = [];
    }

    // обнуляет полностью класс
    resetAll() {
        this.resetTicketsGUI();
        this.resetLogic();
    }
}


// When the user clicks on <span> (x), close the modal
spanChooser.onclick = function() {
    modalChooser.style.display = "none";
};

// Вызов модального окна выбора работы которую проводят на лекции
btnChooser.addEventListener("click", function (event) {
    if (event.target.tagName == 'BUTTON') {
        // вызвать модальное окно планирования оценок на учебный год
        modalChooser.style.display = "block";
        let subjectSelector = journalSelector.subjectSelector;
        // FIXME: уродливый костыль
        let pk = subjectSelector.value,
            name = subjectSelector.options[subjectSelector.selectedIndex].text;
        let subject = {
            pk: pk,
            name: name
        };
        // FIXME: уродливый костыль
        planner = new TestPlanner(subject, courseID = null, journalSelector.groupID);
        planner.requireTickets();
    }
})

// расширение модального окна в котором можно добавить внеплановое занятие
let unscheduledTicketBtn = document.getElementById('unscheduledTicketBtn');
unscheduledTicketBtn.addEventListener('click', function (event) {
    let unsheduledConstructor = document.getElementById('journalUnsheduledConstructor');
    if (unsheduledConstructor.classList.contains('hide')) {
        unsheduledConstructor.classList.remove('hide');
    } else {
        unsheduledConstructor.classList.add('hide');
    }
})

// Получает номер идентификатора карточки по которой произошло нажатие
function getIdFromTicket(ticketGUI) {
    let id = null;
    id = ticketGUI.getElementsByClassName('card-detail-id-hide')[0].textContent;
    return id;
}

// делает тикеты кликабельными для добавления в журнал
let sheduledTicketsContainer = document.getElementById('testList-ticketContainer');
sheduledTicketsContainer.addEventListener('click', function (event) {
    let ticketGUI = event.target.closest('.modal-body-testList-ticket');
    let id = getIdFromTicket(ticketGUI);
    // фиксация проведения проверочной работы
    let subjectID = journalSelector.subjectID,
        courseID = journalSelector.courseID,
        groupID = journalSelector.groupID;
    planner.getTicketByID(id);
    $.ajax({
        url: 'ajax/give_a_test/',
        method: 'POST',
        data: {
            "topicID": id,
            "subjectID": subjectID,
            "courseID": courseID,
            "groupID": groupID,
        },
        datatype: 'json',
        success: function (data) {
            planner.deleteTicketByID(id);
            // Визуально удаляет элемент
            ticketGUI.remove();
            // закрыть модальное окно
            modalChooser.style.display = "none";
            // отрисовать новую зафиксированную запись отметки в журнале с оценками
            markList.createNewMarks();
        },
        error: function () {
            // TODO: ничего с ним не делать
            // Сообщить, что удаление не произошло
            console.log('Не провели работу из-за сервера');
        }
    });
})

// Прячет модальное окнок
function closeModalTestChooser() {
    modalChooser.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
    if (event.target == modalChooser) {
        closeModalTestChooser();
    }
};

// Сокрытие модального окна при нажатии кнопки ESC
document.addEventListener('keydown', event => {
    if (event.key === 'Escape' || event.keyCode === 27) {
        closeModalTestChooser();
    }
});

/*
* Провести особую работу "ДКР, Аттестация, Зачет, Экзамен"
* */
let derRevBtn = document.getElementById("directorialReviewBtn");
derRevBtn.addEventListener('click', function(){
    let derRevBtn = document.getElementById("directorialReviewBtn");
    let subjectID = journalSelector.subjectID,
        courseID = journalSelector.courseID,
        groupID = journalSelector.groupID;
    $.ajax({
        url: 'ajax/give_directorial_review/',
        method: 'POST',
        data: {
            "subjectID": subjectID,
            "courseID": courseID,
            "groupID": groupID
        },
        datatype: 'json',
        success: function (data) {
            // закрыть модальное окно
            modalChooser.style.display = "none";
            // отрисовать новую зафиксированную запись отметки в журнале с оценками
            markList.createNewMarks();
        },
        error: function () {
            // TODO: ничего с ним не делать
            // Сообщить, что удаление не произошло
            console.log('Не провели работу из-за сервера');
        }
    });
})

let certifyBtn = document.getElementById("certifyBtn");
certifyBtn.addEventListener('click', function () {
    let derRevBtn = document.getElementById("certifyBtn");
    let subjectID = journalSelector.subjectID,
        courseID = journalSelector.courseID,
        groupID = journalSelector.groupID;
    $.ajax({
        url: 'ajax/give_certify_by_subject/',
        method: 'POST',
        data: {
            "subjectID": subjectID,
            "courseID": courseID,
            "groupID": groupID,
        },
        datatype: 'json',
        success: function (data) {
            // закрыть модальное окно
            modalChooser.style.display = "none";
            // отрисовать новую зафиксированную запись отметки в журнале с оценками
            markList.createNewMarks();
        },
        error: function () {
            // TODO: ничего с ним не делать
            // Сообщить, что удаление не произошло
            console.log('Не провели работу из-за сервера');
        }
    });
});

let creditBtn = document.getElementById("creditBtn");
creditBtn.addEventListener('click', function () {
    let derRevBtn = document.getElementById("creditBtn");
    let subjectID = journalSelector.subjectID,
        courseID = journalSelector.courseID,
        groupID = journalSelector.groupID;
    $.ajax({
        url: 'ajax/give_credit_by_subject/',
        method: 'POST',
        data: {
            "subjectID": subjectID,
            "courseID": courseID,
            "groupID": groupID,
        },
        datatype: 'json',
        success: function (data) {
            // закрыть модальное окно
            modalChooser.style.display = "none";
            // отрисовать новую зафиксированную запись отметки в журнале с оценками
            markList.createNewMarks();
        },
        error: function () {
            // TODO: ничего с ним не делать
            // Сообщить, что удаление не произошло
            console.log('Не провели работу из-за сервера');
        }
    });
});

let examBtn = document.getElementById("examBtn");
examBtn.addEventListener('click', function () {
    let derRevBtn = document.getElementById("examBtn");
    let subjectID = journalSelector.subjectID,
        courseID = journalSelector.courseID,
        groupID = journalSelector.groupID;
    $.ajax({
        url: 'ajax/give_exam_by_subject/',
        method: 'POST',
        data: {
            "subjectID": subjectID,
            "courseID": courseID,
            "groupID": groupID,
        },
        datatype: 'json',
        success: function (data) {
            // закрыть модальное окно
            modalChooser.style.display = "none";
            // отрисовать новую зафиксированную запись отметки в журнале с оценками
            markList.createNewMarks();
        },
        error: function () {
            // TODO: ничего с ним не делать
            // Сообщить, что удаление не произошло
            console.log('Не провели работу из-за сервера');
        }
    });
});

// кнопка добавления не запланированного тикета
let unscheduledTicketBtnAdd = document.getElementById('unscheduledTicketBtnAdd');
unscheduledTicketBtnAdd.addEventListener('click', function (event) {
    planner.createNewTicket();
})

/*
* Глобальные переменные
* */
let planner;
