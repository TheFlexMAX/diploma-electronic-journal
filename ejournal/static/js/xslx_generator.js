
// Главный функционал импортирования файлов
function btn_export() {
    let table1 = document.querySelector("#state_1");
    let sheet = XLSX2.utils.table_to_sheet(table1);
    //This is the code to modify the format

    // Данные динамической части документа
    let data = statements.stateTableCourses.data.students;

    // Устанавливаем глобальные стили для документ
    setGlobalStyles(sheet);

    // стилизируем заголовок документа
    setHeaderStyleCoursesReport(sheet);
    // стилизируем нижний заголовок документа
    setLowHeaderStyleCoursesReport(sheet, data)

    // Устанавливаем стили для динамических данных
    setStyleForDynamic(sheet, data)

    // Устанавливаем ширину ячеек
    sheet["!cols"] = [{
        wpx: 20
    }, {
        wpx: 60
    }, {
        wpx: 80
    }, {
        wpx: 100
    }, {
        wpx: 80
    }, {
        wpx: 90
    }, {
        wpx: 90
    }, {
        wpx: 90
    }]; //Cell Column Width

    openDownloadDialog(sheet2blob(sheet),'download.xlsx');
}

// покрывает переданную ячейчку обычной цельной обводкой
function fullBorderedCell(cell) {
    cell.s.border = {
        "top": {
            "style": "thin",
            "color": {
                "auto": 1
            }
        },
        "right": {
            "style": "thin",
            "color": {
                "auto": 1
            }
        },
        "bottom": {
            "style": "thin",
            "color": {
                "auto": 1
            }
        },
        "left": {
            "style": "thin",
            "color": {
                "auto": 1
            }
        },
    }
}

// Создатель пустых ячеек
function createEmptyCell() {
    return {
        t: "s",
        v: "",
        s: {
            alignment: {},
            bordered: {},
            font: {}
        }
    };
}

// полное центрирование ячейки
function fullCenteredCell(cell, isWrap = true) {
    console.log(cell);
    cell.s.alignment = {
        horizontal: "center",
        vertical: "center",
        wrapText: isWrap
    }
}

// указывает положение текста в ячейке
function setAlignmentInCell(cell, horizontal = "center", vertical = "center") {
    cell.s.alignment.horizontal = horizontal;
    cell.s.alignment.vertical = vertical;
}

// Устанавливает стили заголовка для сгенерированного отчета курсового
function setHeaderStyleCoursesReport(sheet) {
    // Устанавливаем глобальные стили всем элементам, кроме 2 последних
    let sheetLength = Object.keys(sheet).length - 2,
        counter = 0;

    // Создаем ячейку C13
    sheet["C13"] = createEmptyCell();
    sheet["G13"] = createEmptyCell();
    sheet["A14"] = createEmptyCell();
    sheet["B14"] = createEmptyCell();
    sheet["C14"] = createEmptyCell();
    sheet["D14"] = createEmptyCell();
    sheet["E14"] = createEmptyCell();
    sheet["H14"] = createEmptyCell();

    // Создает границы
    fullBorderedCell(sheet["A13"]);
    fullBorderedCell(sheet["B13"]);
    fullBorderedCell(sheet["C13"]);
    fullBorderedCell(sheet["D13"]);
    fullBorderedCell(sheet["E13"]);
    fullBorderedCell(sheet["F13"]);
    fullBorderedCell(sheet["G13"]);
    fullBorderedCell(sheet["H13"]);
    fullBorderedCell(sheet["A14"]);
    fullBorderedCell(sheet["B14"]);
    fullBorderedCell(sheet["C14"]);
    fullBorderedCell(sheet["D14"]);
    fullBorderedCell(sheet["E14"]);
    fullBorderedCell(sheet["F14"]);
    fullBorderedCell(sheet["G14"]);
    fullBorderedCell(sheet["H14"]);

    console.log('sheet');
    console.log(sheet);
    // центрируем текст
    fullCenteredCell(sheet["B7"]);
    fullCenteredCell(sheet["E7"]);
    fullCenteredCell(sheet["A13"]);
    fullCenteredCell(sheet["B13"]);
    fullCenteredCell(sheet["D13"]);
    fullCenteredCell(sheet["E13"]);
    fullCenteredCell(sheet["F13"]);
    fullCenteredCell(sheet["H13"]);
    fullCenteredCell(sheet["F14"]);
    fullCenteredCell(sheet["G14"]);

    fullCenteredCell(sheet["A13"]);
    fullCenteredCell(sheet["B13"]);
    fullCenteredCell(sheet["C13"]);
    fullCenteredCell(sheet["D13"]);
    fullCenteredCell(sheet["E13"]);
    fullCenteredCell(sheet["F13"]);
    fullCenteredCell(sheet["G13"]);
    fullCenteredCell(sheet["H13"]);
    fullCenteredCell(sheet["A14"]);
    fullCenteredCell(sheet["B14"]);
    fullCenteredCell(sheet["C14"]);
    fullCenteredCell(sheet["D14"]);
    fullCenteredCell(sheet["E14"]);
    fullCenteredCell(sheet["F14"]);
    fullCenteredCell(sheet["G14"]);
    fullCenteredCell(sheet["H14"]);

    // Выравниваем остальной текст
    setAlignmentInCell(sheet["A3"], "top");
    setAlignmentInCell(sheet["A5"], "top");
    setAlignmentInCell(sheet["A6"], "top");
    setAlignmentInCell(sheet["B7"], "top");
    setAlignmentInCell(sheet["E7"], "top");
    setAlignmentInCell(sheet["A8"], "top");
    setAlignmentInCell(sheet["A9"], "top");
    setAlignmentInCell(sheet["A11"], "top");


    // Начало заголовка оценок и зачеток
    sheet["A1"].s.font = {
        name: 'Times New Roman',
        sz: 12,
        bold: false,
        underline: false,
        color: {
            rgb: "000000"
        }
    }

    sheet["A2"].s.font = {
        name: 'Times New Roman',
        sz: 14,
        bold: true,
        underline: false,
        color: {
            rgb: "000000"
        }
    }

    sheet["A11"].s.font = {
        name: 'Times New Roman',
        sz: 11,
        bold: false,
        color: {
            rgb: "000000"
        }
    }

    sheet["A13"].s.font = {
        name: 'Times New Roman',
        sz: 8,
        bold: true,
        color: {
            rgb: "000000"
        }
    }

    sheet["B13"].s.font = {
        name: 'Times New Roman',
        sz: 8,
        bold: true,
        color: {
            rgb: "000000"
        }
    }


    sheet["D13"].s.font = {
        name: 'Times New Roman',
        sz: 8,
        bold: true,
        color: {
            rgb: "000000"
        }
    }

    sheet["E13"].s.font = {
        name: 'Times New Roman',
        sz: 8,
        bold: true,
        color: {
            rgb: "000000"
        }
    }

    sheet["F13"].s.font = {
        name: 'Times New Roman',
        sz: 8,
        bold: true,
        color: {
            rgb: "000000"
        }
    }

    sheet["F14"].s.font = {
        name: 'Times New Roman',
        sz: 8,
        bold: true,
        color: {
            rgb: "000000"
        }
    }

    sheet["G14"].s.font = {
        name: 'Times New Roman',
        sz: 8,
        bold: true,
        color: {
            rgb: "000000"
        }
    }

    sheet["H13"].s.font = {
        name: 'Times New Roman',
        sz: 8,
        bold: true,
        color: {
            rgb: "000000"
        }
    }
    // Конец заголовка оценок и зачеток
}

// Позволяем создавать стилизированные заголовки со смешением по ря
function headerStyler(cell, size = 14, bold = false, fontName = 'Times New Roman') {
    cell.s.font = {
        name: fontName,
        sz: size,
        bold: bold,
        underline: false,
        color: {
            rgb: "000000"
        }
    }
}

// Создает параметры строк для управления их высотой в документе
function createRows(sheet, rowNumbers = 0, rowsHeight = 20) {
    let sheetRows = [{
        hpt: 800,
        hpx: 800,
        hidden: false
    }];
    sheet["!rows"] = sheetRows;

    // // let's set row heights
    // for (let i = 0; i <= rowNumbers; i++) {
    //     sheet['!rows'].push({
    //         hpx: 20
    //     });
    // }
}

// TODO: Либо разобраться с тем, почему не работает, либо выпилить из проекта
// Устанавливает высоту для определенного номера строки
function setRowHeight(rows, number = 1, height = 20) {
    // поскольку строки - натурально число, а массив нет - отнимаем от номера ячейки 1
    number--;
    let counter = 0;
    for (let row in rows) {
        if (counter === number) {
            rows[row].hpt = height;
            return;
        }
        counter++;
    }
}

// Устанавливает стили для нижнего заголовка
function setLowHeaderStyleCoursesReport(sheet, data) {
    let dataLen = data.length;
    let spacer = dataLen + 16; // указывает на самый верхний элемент нижнего заголовка

    // Упрощает монотонную задачу
    function f(sheet, letter = "A", spacer = spacer, step = 0, size = 8, isBold = true) {
        let cellName = letter + (spacer + step).toString();
        if (!sheet[cellName]) {
            sheet[cellName] = createEmptyCell();
        }
        fullCenteredCell(sheet[cellName]);
        fullBorderedCell(sheet[cellName]);
        headerStyler(sheet[cellName], size, isBold);
    }

    // Общий заголовок
    let cellName = "A" + spacer.toString();
    fullCenteredCell(sheet[cellName]);
    headerStyler(sheet[cellName], 9, true);

    // Заголовок ячеек
    // Растягиваем строку заголовков ячеек
    createRows(sheet, spacer + 10);
    // setRowHeight(sheet["!rows"], spacer + 1, 800);

    f(sheet, "A", spacer, 1, 8, true);
    f(sheet, "B", spacer, 1, 8, true);
    f(sheet, "C", spacer, 1, 8, true);
    f(sheet, "D", spacer, 1, 8, true);
    f(sheet, "E", spacer, 1, 8, true);
    f(sheet, "F", spacer, 1, 8, true);
    f(sheet, "G", spacer, 1, 8, true);
    f(sheet, "H", spacer, 1, 8, true);

    f(sheet, "A", spacer, 2, 8, true);
    f(sheet, "B", spacer, 2, 8, true);
    f(sheet, "C", spacer, 2, 8, true);
    f(sheet, "D", spacer, 2, 8, true);
    f(sheet, "E", spacer, 2, 8, true);
    f(sheet, "F", spacer, 2, 8, true);
    f(sheet, "G", spacer, 2, 8, true);
    f(sheet, "H", spacer, 2, 8, true);

    // Оцінка за ECTS (колонка)
    f(sheet, "A", spacer, 3, 8, false);
    f(sheet, "A", spacer, 4, 8, false);
    f(sheet, "A", spacer, 5, 8, false);
    f(sheet, "A", spacer, 6, 8, false);
    f(sheet, "A", spacer, 7, 8, false);
    f(sheet, "A", spacer, 8, 8, false);
    f(sheet, "A", spacer, 9, 8, false);
    f(sheet, "B", spacer, 3, 8, false);
    f(sheet, "B", spacer, 4, 8, false);
    f(sheet, "B", spacer, 5, 8, false);
    f(sheet, "B", spacer, 6, 8, false);
    f(sheet, "B", spacer, 7, 8, false);
    f(sheet, "B", spacer, 8, 8, false);
    f(sheet, "B", spacer, 9, 8, false);

    f(sheet, "C", spacer, 3, 8, false);
    f(sheet, "C", spacer, 4, 8, false);
    f(sheet, "C", spacer, 5, 8, false);
    f(sheet, "C", spacer, 6, 8, false);
    f(sheet, "C", spacer, 7, 8, false);
    f(sheet, "C", spacer, 8, 8, false);
    f(sheet, "C", spacer, 9, 8, false);

    f(sheet, "D", spacer, 3, 8, false);
    f(sheet, "D", spacer, 4, 8, false);
    f(sheet, "D", spacer, 5, 8, false);
    f(sheet, "D", spacer, 6, 8, false);
    f(sheet, "D", spacer, 7, 8, false);
    f(sheet, "D", spacer, 8, 6, false);
    f(sheet, "D", spacer, 9, 6, false);
    f(sheet, "E", spacer, 3, 8, false);
    f(sheet, "E", spacer, 4, 8, false);
    f(sheet, "E", spacer, 5, 8, false);
    f(sheet, "E", spacer, 6, 8, false);
    f(sheet, "E", spacer, 7, 8, false);
    f(sheet, "E", spacer, 8, 6, false);
    f(sheet, "E", spacer, 9, 6, false);

    f(sheet, "F", spacer, 3, 8, false);
    f(sheet, "F", spacer, 4, 8, false);
    f(sheet, "F", spacer, 5, 8, false);
    f(sheet, "F", spacer, 6, 8, false);
    f(sheet, "F", spacer, 7, 8, false);
    f(sheet, "F", spacer, 8, 8, false);
    f(sheet, "F", spacer, 9, 8, false);

    f(sheet, "G", spacer, 3, 8, false);
    f(sheet, "G", spacer, 4, 8, false);
    f(sheet, "G", spacer, 5, 8, false);
    f(sheet, "G", spacer, 6, 8, false);
    f(sheet, "G", spacer, 7, 8, false);
    f(sheet, "G", spacer, 8, 8, false);
    f(sheet, "G", spacer, 9, 8, false);

    f(sheet, "H", spacer, 3, 8, false);
    f(sheet, "H", spacer, 4, 8, false);
    f(sheet, "H", spacer, 5, 8, false);
    f(sheet, "H", spacer, 6, 8, false);
    f(sheet, "H", spacer, 7, 8, false);
    f(sheet, "H", spacer, 8, 8, false);
    f(sheet, "H", spacer, 9, 8, false);

    f(sheet, "С", spacer, 11, 9, false);
    setAlignmentInCell(sheet["C" + (spacer+11).toString()], "top"); // Убрать влевую сторону
    // Устанавливаем нижний тонкий подчерк
    sheet["E" + (spacer+11).toString()].s.border = {
        "bottom": {
            "style": "thin",
            "color": {
                "auto": 1
            }
        }
    };

    console.log("sheet[G + (spacer+11).toString()]");
    console.log(sheet["G" + (spacer+11).toString()]);
    sheet["G" + (spacer+11).toString()].s.font = {
        name: 'Times New Roman',
        sz: 10,
        bold: false,
        underline: false,
        color: {
            rgb: "000000"
        }
    };
    sheet["G" + (spacer+11).toString()].s.border = {};

    console.log("sheet[G + (spacer+11).toString()]");
    console.log(sheet["G" + (spacer+11).toString()]);

    sheet["G" + (spacer+12).toString()].s.font = {
        name: 'Times New Roman',
        sz: 8,
        bold: false,
        underline: false,
        color: {
            rgb: "000000"
        }
    };
    sheet["G" + (spacer+12).toString()].s.border = {};
    sheet["G" + (spacer+12).toString()].s.alignment = {
        horizontal: "top",
        vertical: "center",
        wrapText: false
    };
}

// Указывает стили для динамической части таблицы
function setStyleForDynamic(sheet, data) {
    let dataLen = data.length;

    for (let i = 15; i <= dataLen + 14; i++) {
        let nameCell = 'A' + i.toString();
        fullCenteredCell(sheet[nameCell]);
        fullBorderedCell(sheet[nameCell]);
        sheet[nameCell].s.font.sz = 8;

        nameCell = 'B' + i.toString();
        fullBorderedCell(sheet[nameCell]);
        sheet[nameCell].s.alignment.horizontal = "top";
        sheet[nameCell].s.font.sz = 10;

        nameCell = 'C' + i.toString();
        sheet[nameCell] = createEmptyCell();
        fullCenteredCell(sheet[nameCell]);
        fullBorderedCell(sheet[nameCell]);
        sheet[nameCell].s.font.sz = 10;

        nameCell = 'D' + i.toString();
        fullCenteredCell(sheet[nameCell]);
        fullBorderedCell(sheet[nameCell]);
        sheet[nameCell].s.font.sz = 10;

        nameCell = 'E' + i.toString();
        fullCenteredCell(sheet[nameCell]);
        fullBorderedCell(sheet[nameCell]);
        sheet[nameCell].s.font.sz = 10;

        nameCell = 'F' + i.toString();
        fullCenteredCell(sheet[nameCell]);
        fullBorderedCell(sheet[nameCell]);
        sheet[nameCell].s.font.sz = 10;

        nameCell = 'G' + i.toString();
        fullCenteredCell(sheet[nameCell]);
        fullBorderedCell(sheet[nameCell]);
        sheet[nameCell].s.font.sz = 10;

        nameCell = 'H' + i.toString();
        fullCenteredCell(sheet[nameCell]);
        fullBorderedCell(sheet[nameCell]);
        sheet[nameCell].s.font.sz = 10;
    }
}

// устанавливает стили глобально для всего документ
function setGlobalStyles(sheet) {
    // Устанавливаем глобальные стили всем элементам, кроме 2 последних
    let sheetLength = Object.keys(sheet).length - 2,
        counter = 0;

    for (let cell in sheet) {
        if (counter === sheetLength)
            return
        else {
            sheet[String(cell)].s = {
                font: {
                    name: 'Times New Roman',
                    sz: 11,
                    bold: false,
                    underline: false,
                    color: {
                        rgb: "000000"
                    }
                },
                alignment: {
                    horizontal: "center",
                    vertical: "center",
                    wrapText: false
                },
            }
            counter++;
        }
    }
}
