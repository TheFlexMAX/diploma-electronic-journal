from django.conf.urls import url
from django.urls import path
from .views import *

urlpatterns = [
    path('', get_journal, name='journal'),
    path('designer/', get_journal_designer, name='journal_designer'),
    path('notification/', get_notification, name='get_notification'),
    path('welcome/', get_welcome, name='welcome'),
    path('record_book/', get_record_book_page, name='record_book_page'),
    path('statements/', get_statements_page, name='statements_page'),
    # для JSON запросов
    url(r'^ajax/get_courses/$', get_courses, name='get_courses'),
    url(r'^ajax/get_educated_courses/$', get_educated_courses, name='get_educated_courses'),
    url(r'^ajax/get_groups_by_courseID/$', get_groups_by_courseID, name='get_groups_by_courseID'),
    url(r'^ajax/get_subjects_by_groupID/$', get_subjects_by_groupID, name='get_subjects_by_groupID'),
    url(r'^ajax/get_subjects/$', get_subjeсts, name='get_subjects'),
    url(r'^ajax/new_lection/$', new_lection, name='new_lection'),
    url(r'^ajax/require_students_by_group/$', require_students_by_group, name='require_students_by_group'),
    url(r'^ajax/date_today/$', date_today, name='date_today'),
    url(r'^ajax/require_journal_attendance/$', require_journal_attendance, name='require_journal_attendance'),
    url(r'^ajax/require_journal_marks/$', require_journal_marks, name='require_journal_marks'),
    url(r'^ajax/get_last_required_journal/$', get_last_required_journal, name='get_last_required_journal'),
    url(r'^ajax/require_last_mark_students_in_journalList/$', require_last_mark_students_in_journalList, name='require_last_mark_students_in_journalList'),
    url(r'^ajax/create_new_marks/$', create_new_marks, name='create_new_marks'),
    url(r'^ajax/create_new_attendances/$', create_new_attendances, name='create_new_attendances'),
    url(r'^ajax/change_attendance_status/$', change_attendance_status, name='change_attendance_status'),
    url(r'^ajax/delete_last_attendances/$', delete_last_attendances, name='delete_last_attendances'),
    url(r'^ajax/change_student_mark/$', change_student_mark, name='change_student_mark'),
    url(r'^ajax/delete_student_mark/$', delete_student_mark, name='delete_student_mark'),
    url(r'^ajax/cancel_last_test_work/$', cancel_last_test_work, name='cancel_last_test_work'),
    url(r'^ajax/require_tickets/$', require_tickets, name='require_tickets'),
    url(r'^ajax/require_last_performed_topic_for_group/$', require_last_performed_topic_for_group, name='require_last_performed_topic_for_group'),
    url(r'^ajax/create_new_ticket/$', create_new_ticket, name='create_new_ticket'),
    url(r'^ajax/give_a_test/$', give_a_test, name='give_a_test'),
    url(r'^ajax/give_directorial_review/$', give_directorial_review, name='give_a_test'),
    url(r'^ajax/give_certify_by_subject/$', give_certify_by_subject, name='give_a_test'),
    url(r'^ajax/give_credit_by_subject/$', give_credit_by_subject, name='give_a_test'),
    url(r'^ajax/give_exam_by_subject/$', give_exam_by_subject, name='give_a_test'),
    # JSON запросы для оценок
    url(r'^ajax/get_subjects_for_student/$', get_subjects_for_student, name='get_subjects_for_student'),
    # JSON запросы для конструктора журналов
    url(r'^designer/ajax/require_teachers_subjects/$', require_teachers_subjects, name='require_teachers_subjects'),
    url(r'^designer/ajax/get_courses/$', get_courses_request, name='get_courses'),
    url(r'^designer/ajax/get_groups_by_course/$', get_groups_by_course, name='get_groups_by_course'),
    url(r'^designer/ajax/require_groups_on_course/$', require_groups_on_course, name='require_groups_on_course'),
    url(r'^designer/ajax/require_tickets/$', require_tickets, name='require_tickets'),
    url(r'^designer/ajax/create_new_ticket/$', create_new_ticket, name='create_new_ticket'),
    url(r'^designer/ajax/delete_ticket/$', delete_ticket, name='delete_ticket'),
    # Для ведомостей
    url(r'^statements/ajax/get_courses/$', get_courses, name='get_courses'),
    url(r'^statements/ajax/get_educated_courses/$', get_educated_courses, name='get_educated_courses'),
    url(r'^statements/ajax/get_groups_by_courseID/$', get_groups_by_courseID, name='get_groups_by_courseID'),
    # url(r'^statements/ajax/genStatements/$', genStatements, name='genStatements'),
    url(r'^statements/ajax/genStatementsFinalByCourse/$', genStatementsFinalByCourse, name='genStatements'),
    url(r'^statements/ajax/genStatementsFinalBySpec/$', genStatementsFinalBySpec, name='genStatements'),
    url(r'^statements/ajax/get_subjects_by_groupID/$', get_subjects_by_groupID, name='get_subjects_by_groupID'),
    # Для зачеток


    # url(r'^ajax/get_subjects/$', get_subjets(), name='get_subjects'),
]
