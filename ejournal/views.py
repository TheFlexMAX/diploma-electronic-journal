from django.shortcuts import render
from django.views.generic import View
from django.http import (JsonResponse,
                         HttpResponse)
from django.core import serializers
import json
from django.db.models import ProtectedError
from django.db.models import Avg, Sum
from django.utils.timezone import now, localtime
from django.contrib import messages
from django.contrib.auth.decorators import login_required

# Модели участвуемые в работе
from .models import Mark, Subject, Attendance, Topic, Control_Type, RequiredJournal
from accounts.models import Course, Group, User, RecordBook

# Формы для отображения и обработки данных
from .forms import *

# native python 3
from datetime import datetime
from datetime import date, time, timedelta


# Получение списка групп для выпадающего списка
def get_list_groups():
    group_list = Group.objects.all()
    return group_list

# Получение списка курсов для выпадающего списка
def get_courses():
    courses_list = Course.objects.all()
    return courses_list

# Получения списка предметов для выпадающего списка
def get_subjects():
    subject_list = Subject.objects.all()
    return subject_list


# Выбирает список курсов которые ведет преподаватель
def get_educated_courses(request):
    user = request.user
    courses = Course.objects.actual_courses_for_user(user)
    data = []
    for course in courses:
        data.append({
            "name": str(course),
            "pk": course.id
        })
    return JsonResponse(data, safe=False)


# Получение всех студентов с оценками в виде таблицы журанала
# @login_required разкоментит когда права доступа будут настроены
def get_journal(request):
    user = request.user
    courses = Course.objects.actual_courses_for_user(user=user)

    return render(
        request,
        'journal.html',
        context= {
            "courses": courses
        }
    )


# Возвращает странцу предметов которые ведет пользователь
def get_journal_designer(request):
    user = request.user
    user_model = User.objects.get(id = user.id)
    # выбрать предметы которве ведет пользователь и отсортировать в порядке курса
    subjects = Subject.objects.filter(lector=user_model).order_by('semestr_academic')
    courses = get_courses()
    context = {
        "subjects": subjects,
        "courses": courses
    }
    return render(request, 'journalDesigner.html', context=context)


# Возвращает страницу уведолений пользователя
def get_notification(request):
    return render(request, 'notification.html', context={})


# Возвращает страницу приветствия пользователя
def get_welcome(request):
    #TODO: собирать новости и данный для пользователя
    return render(request, 'welcome.html', context={})


# Возвращает страницу зачетной книжки студента
# TODO: сделать работу в зависимости от того, кто вызывает метод
def get_record_book_page(request):
    # Собрать данные о оценках студента по его семестрам, годам
    tables = []
    context = {
        'tables': [],
        'recordbook': ""
    }
    if request.user.is_student:
        student = request.user
        record_book_model = RecordBook.objects.none()
        try:
            # Пробуем получить зачетку пользователя
            record_book_model = RecordBook.objects.get(student=student)
        except RecordBook.DoesNotExist:
            # Если её нет, то выводим закглушку на клиенте
            return render(request, 'recordBook.html', context=context)

        marks = Mark.objects.filter(user = student).order_by('date_added')
        # попробовать получить оценки за 12 курсов
        for i in range(12):
            i += 1
            records_by_course = []
            try:
                subjects_by_course = Subject.objects.filter(course_of_teaching = i, specialization=record_book_model.specialization)
                # формируем предметы
                subject_number = 0;
                for subject in subjects_by_course:
                    # собираем темы за год
                    # Оценки полученные по преподаваемому предмету
                    marks_by_subject = marks.filter(subject=subject).order_by('date_added')
                    # Отбираем оценки за первую половину учебного года
                    first_marks_half = marks_by_subject.filter()

                    # Отбираем оценки за вторую половину учебного года
                    # проверяем есть ли у него оценки по этому предмету. Если их нет - скорее всего лекций не было
                    marks_exist = True
                    if not marks_by_subject:
                        marks_exist = False

                    # если есть оценки - записываем в словарь
                    if marks_exist:
                        # Найти последнюю дату проведения работы для его группы(взять дату последнего проведения Topic)
                        exam_date = marks_by_subject.first().date_added
                    subject_number += 1;
                    # else:
                    #     continue

                    # TODO: если студентам проводился экзамен - вывести оценку экзамена
                    # Средняя оценка студента полученная по предмету
                    mark_counter = 0
                    mark_sum = 0
                    for mark in marks_by_subject:
                        mark_counter += 1
                        if mark.mark:
                            mark_sum += mark.mark
                    # фикс ошибки деления на ноль
                    if mark_sum == 0 or mark_counter == 0:
                        avg_mark = ''
                        exam_date = ''
                    else:
                        avg_mark = mark_sum / mark_counter
                    # Дата получения последней оценки

                    records_by_course.append({
                        'subject_number': subject_number,
                        'subject_name': subject.name,
                        'lecturer': subject.lector.get_full_name(),
                        'mark_number': avg_mark,
                        'mark_str': '',
                        'type_of_mark': 'поточна',
                        'date': exam_date,
                    })

                if subject_number != 0:
                    tables.append({
                        'course': i,
                        'semestr': '',
                        'starting_year': '',
                        'ending_year': '',
                        'fields': records_by_course,
                    })

            except Subject.DoesNotExist:
                pass

        context = {
            'tables': tables,
            'recordbook': record_book_model.number
        }
    return render(request, 'recordBook.html', context=context)


# Возвращает страницу проработки ведомостей
def get_statements_page(request):
    # TODO: передать список обучающихся курсов в текущий момент
    # т.е курсы должны быть активны, и браться за период от текущего момента, до
    # 12 лет назад
    state_1_form = StatementsCoursesExam(user=request.user)
    state_2_form = StatementsFinalByCourse()
    data = {
        "statements_courses_exam_form": state_1_form,
        "state_2_form": state_2_form
    }
    return render(request, 'statements.html', context=data)



'''
Проба обработать асинхронный запрос:
'''
# получить список курсов
def get_courses_request(request):
    сourses = Course.objects.all()
    data = []
    for course in list(сourses):
        data.append({
            "pk": course.id,
            "name": str(course)
        })
    return JsonResponse(data, safe=False)


# Получить список предметов
def get_subjeсts(request):
    groupID = request.GET.get('groupId', None)
    subjects = Subject.objects.all()
    data = serializers.serialize('json', Subject.objects.all())
    return JsonResponse(data, safe=False)


# Возвращаем список групп для определенного идентификатора курса
def get_groups_by_courseID(request):
    courseID = request.GET.get('courseID')
    course_model = Course.objects.get(id=courseID)
    groups = Group.objects.filter(course_id=course_model).order_by("number")
    data = []
    for group in groups:
        data.append({
            'pk': group.id,
            'number': group.number
        })
    return JsonResponse(data, safe=False)


def genStatements(request):
    """
    Генерирует набор данных, необходимый для отрисовки таблицы отчетности по курсовым\экзаменам
    :param request:
    :return:
    """
    if request.method == 'POST' and "CoursesExamStatements" in request.POST.get('form_name'):
        if request.is_ajax():
            statementCourseExamForm = StatementsCoursesExam(data=request.POST, user=request.user)

            if statementCourseExamForm.is_valid():
                cleanData = statementCourseExamForm.cleaned_data
                # Начинаем выборку данных на таблицу
                data = genCoursesExamStatements(request, cleanData)
                return JsonResponse(data, safe=False)
            else:
                print(statementCourseExamForm.errors)
                return JsonResponse(statementCourseExamForm.errors, status=400)



def genStatementsFinalByCourse(request):
    # if request.method == 'POST' and "CoursesFinalStatements" in request.POST.get('form_name'):
    #     if request.is_ajax():
    statementCourseExamForm = StatementsFinalByCourse(data=request.POST)

    if statementCourseExamForm.is_valid():
        cleanData = statementCourseExamForm.cleaned_data
        # Начинаем выборку данных на таблицу
        data = genStatementsFinalForCourse(request, cleanData)
        return JsonResponse(data, safe=False)
    else:
        return JsonResponse(statementCourseExamForm.errors, status=400)
        # return JsonResponse(statementCourseExamForm.errors, status=500)


def genStatementsFinalForCourse(request, data):
    course_number = int(data.get('course_number'))
    semestr_half = data.get('semestr_half')

    courses = []
    courses_all = Course.objects.all()
    for course_in_all in courses_all:
        if course_in_all.on_course_now() == int(course_number):
            courses.append(course_in_all)
    specs = []
    for course in courses:
        specs.append(course.specialization_id)

    semsetr = 0
    if semestr_half == "second":
        semsetr = course_number * 2
    else:
        semsetr = (course_number * 2) - 1

    groups = Group.objects.filter(course_id__in=courses) # группы, которые находятся на курсе
    # выбрать все предметы для выбранного курса
    subjects_model = Subject.objects.filter(specialization__in=specs, semestr_academic=semsetr)

    # пробуем получить нашши типы проведенных работ
    controls_types = []
    try:
        control_type = Control_Type.objects.filter(title="Атестація").first()
        if not control_type:
            control_type = Control_Type.objects.create()
            control_type.title = "Атестація"
            control_type.save()
        controls_types.append(control_type)
    except Control_Type.DoesNotExist:
        pass

    try:
        control_type = Control_Type.objects.filter(title="ДКР").first()
        if not control_type:
            control_type = Control_Type.objects.create()
            control_type.title = "ДКР"
            control_type.save()
        controls_types.append(control_type)
    except Control_Type.DoesNotExist:
        pass

    try:
        control_type = Control_Type.objects.filter(title="Залік").first()
        if not control_type:
            control_type = Control_Type.objects.create()
            control_type.title = "Залік"
            control_type.save()
        controls_types.append(control_type)
    except Control_Type.DoesNotExist:
        pass

    try:
        control_type = Control_Type.objects.filter(title="Екзамен").first()
        if not control_type:
            control_type = Control_Type.objects.create()
            control_type.title = "Екзамен"
            control_type.save()
        controls_types.append(control_type)
    except Control_Type.DoesNotExist:
        pass

    types_of_check_knowledge = []
    for cont_type in controls_types:
        data_by_specs = []
        for spec in specs:
            data_by_spec = []
            groups_by_spec = Group.objects.filter(course_id__in=courses, course_id__specialization_id=spec)
            # получить предметы, которые читаются на выбранном курсе и выбранном семестре для текущей специализации
            subjects = subjects_model.filter(specialization=spec, course_of_teaching=course_number)
            # собираем оценки по проведенному контрольному типу
            subjects_data = []

            # проверяем, сколько было проведено особых типов
            max_provide_topics = 0
            for subj in subjects:
                topics_and_marks = []
                counter = 1
                # ответвление для заполнения пустых значений в оценках и предметах
                must_be_topics = 0
                if cont_type.title == 'Атестація':
                    must_be_topics = 4
                    if semestr_half == "second":
                        counter = 3
                elif cont_type.title == 'ДКР':
                    must_be_topics = 2
                    if semestr_half == "second":
                        counter = 2
                elif cont_type.title == 'Залік':
                    must_be_topics = 2
                    if semestr_half == "second":
                        counter = 2
                elif cont_type.title == 'Екзамен':
                    must_be_topics = 1
                
                topics = Topic.objects.filter(subject=subj, linked_group__in=groups, is_performed=True, control_type=cont_type).order_by("-date_added")
                # выбираем оценки, которые стоят у студентов
                marks = []

                for topic in topics:
                    topic = Topic.objects.get(id=topic.id)
                    marks_by_topic = Mark.objects.filter(topic=topic).order_by("group").order_by("user__second_name")

                    mark_tmp = []
                    # собираем оценки в удобном формате
                    for mark in marks_by_topic:
                        mark_tmp.append({
                            'mark': str(mark.mark),
                            'subject': str(mark.subject),
                            'mark_owner': mark.user.id
                        })

                    tmp = {
                        "topics_and_marks": {
                            "topic": str(topic.control_type) + ' ' + str(counter),
                            "pk": topic.pk
                        },
                        "marks": mark_tmp
                    }
                    counter += 1
                    topics_and_marks.append(tmp)

                # добиваем пустыми темами данные
                subjects_data.append({
                    "subj_name": subj.name,
                    "topics_and_marks": topics_and_marks,
                })

            # собираем студентов в удобном формате
            studs_tmp = []
            studs = User.objects.filter(group__in=groups_by_spec).order_by("group").order_by("second_name")
            for stud in studs:
                studs_tmp.append({
                    'id': stud.id,
                    'first_name': stud.first_name,
                    'second_name': stud.second_name,
                    'middle_name': stud.middle_name
                })

            data_by_specs.append({
                "spec_name": spec.specialty_name,
                "spec_number": spec.specialty_code,
                "subjects": subjects_data,
                "studs": studs_tmp
            })

        types_of_check_knowledge.append({
            "control_type": str(cont_type),
            "data_by_specs": data_by_specs,
        })

    data_res = {
        "types_of_check_knowledge": types_of_check_knowledge,
        "course_number": course_number,
    }

    return data_res


# def genStatementsFinalForCourse(request, data):
#     course_number = int(data.get('course_number'))
#     semestr_half = data.get('semestr_half')
#
#
#     courses = []
#     courses_all = Course.objects.all()
#     for course_in_all in courses_all:
#         if course_in_all.on_course_now() == int(course_number):
#             courses.append(course_in_all)
#     specs = []
#     for course in courses:
#         specs.append(course.specialization_id)
#
#     semsetr = 0
#     print(f"semestr_half: {semestr_half}")
#     if semestr_half == "second":
#         semsetr = course_number * 2
#     else:
#         semsetr = (course_number * 2) - 1
#
#     groups = Group.objects.filter(course_id__in=courses) # группы, которые находятся на курсе
#     # выбрать все предметы для выбранного курса
#     subjects_model = Subject.objects.filter(specialization__in=specs, semestr_academic=semsetr)
#     tables = []
#     for spec in specs:
#         groups_by_spec = Group.objects.filter(course_id__in=courses, course_id__specialization_id=spec)
#         table = {
#             "spec_name": spec.specialty_name,
#             "subjects": [],
#             "studs": [],
#             "studs_and_marks": []
#         }
#         # получить предметы, которые читаются на выбранном курсе и выбранном семестре для текущей специализации
#         subjects = subjects_model.filter(specialization=spec, course_of_teaching=course_number)
#
#
#         subjects_and_topics = []
#         for subj in subjects:
#             topics_and_marks = []
#             topics = Topic.objects.filter(subject=subj, linked_group__in=groups, is_performed=True)
#             # выбираем оценки, которые стоят у студентов
#             marks = []
#             for topic in topics:
#                 marks_by_topic = Mark.objects.filter(topic=topic).order_by("user")
#                 tmp = {
#                     "topic": {
#                         "control_type": str(topic.control_type),
#                         "pk": topic.pk
#                     },
#                     "marks": serializers.serialize('json', marks_by_topic, fields=('mark'))
#                 }
#                 topics_and_marks.append(tmp)
#             subjects_and_topics.append({
#                 "subject": subj.name,
#                 "topics_and_marks": topics_and_marks
#             })
#
#         table['subjects'] = subjects_and_topics
#
#         # собираем оценки студентов по проведенным темам
#         students = User.objects.filter(group__in=groups_by_spec).order_by("group").order_by("second_name")
#         stud_and_marks = []
#         for stud in students:
#             marks_for_stud = []
#             stud_res = {
#                 "first_name": stud.first_name,
#                 "second_name": stud.second_name,
#                 " выключ": stud.middle_name
#             }
#             for subj in table['subjects']:
#                 if subj['topics_and_marks']:
#                     for top_and_marks in subj['topics_and_marks']:
#                         topic_pk = top_and_marks['topic']['pk']
#                         try:
#                             mark = Mark.objects.get(topic__id=topic_pk, user=stud)
#                             marks_for_stud.append({"mark": mark.mark, "subject": str(mark.subject)})
#                         except Mark.DoesNotExist:
#                             marks_for_stud.append({"mark": "", "subject": str(subj["subject"])})
#                 else:
#                     subj['topics_and_marks'].append({
#                         "topic": {
#                             "control_type": '',
#                             "pk": 0
#                         },
#                         "marks": []
#                     })
#                     marks_for_stud.append({"mark": "", "subject": str(subj["subject"])})
#
#             stud_and_marks.append({
#                 "stud": stud_res,
#                 "marks": marks_for_stud
#             })
#
#
#         table['studs_and_marks'] = stud_and_marks
#
#         tables.append(table)
#     return tables


def genStatementsFinalBySpec(request):
    if request.method == 'POST' and "SpecFinalStatements" in request.POST.get('form_name'):
        if request.is_ajax():
            statementCourseExamForm = StatementsFinalByCourse(data=request.POST)

            if statementCourseExamForm.is_valid():
                cleanData = statementCourseExamForm.cleaned_data
                # Начинаем выборку данных на таблицу
                data = genStatementsFinalForCourse(request, cleanData)
                return JsonResponse(data, safe=False)
            else:
                print(statementCourseExamForm.errors)
                return JsonResponse(statementCourseExamForm.errors, status=400)


def genCoursesExamStatements(request, data):
    """
    Генерирует набор данных, необходимый для генерации курсовой ведомости
    :param: request:
    :param: data:
    :return:
    """
    examiners = data.get('examiners').split(',') # выбираем список преподавателей
    docType = data.get('control_type')
    subjectID = data.get('subject')
    groupID = data.get('group')
    courseID = data.get('course_number')
    event_date = data.get('event_date')
    semestr_half = data.get('semestr_half')

    ECTS_all = {
        "A": 0,
        "B": 0,
        "C": 0,
        "D": 0,
        "E": 0,
        "FX": 0,
        "F": 0,
    } # Суммарное количество оценок по ECTS
    national_all = {
        "five": 0,
        "four": 0,
        "three": 0,
        "two": 0,
        "one": 0
    } # Суммарное количество оценок по Национальному стандарту оценивания

    # Проверяем какой тип ведомости хочет получить пользователь
    if docType == "Exam":
        docType = "Eкзамен"
        # генерируем документ по оценке за экзамен
        group = Group.objects.get(id=groupID.id)
        subject = Subject.objects.get(id=subjectID.id)

        # Получаем список студентов, которые обучаются в группе
        students = User.objects.filter(group=group)

        # Сделать выборку оценки по экзамену
        topic = Topic.objects.filter(subject=subject,
                                      linked_group=group,
                                      control_type__title="Екзамен").order_by("-date_provide").first()
        # Получаем оценки студентов, которые они получили за экзамен
        marks = Mark.objects.filter(topic=topic)
        # формируем результирующие данные перебирая студентов и находя среднюю оценку для каждого из них
        # Подсчитать количество каждого типа оценок

        students = []
        # формируем отчетные данные
        for mark in marks:
            mark_ects = mark.ects_identify()
            national_mark = mark.national_identify()
            # Подсчитываем количество оценок, для нижней суммарной таблицы
            # по национальному критерию
            if '5' == national_mark:
                national_all["five"] += 1
            elif '4' == national_mark:
                national_all["four"] += 1
            elif '3' == national_mark:
                national_all["three"] += 1
            elif '2' == national_mark:
                national_all["two"] += 1
            else:
                national_all["one"] += 1
            # Повторяем операцию для стандарта ECTS
            for (key, value) in ECTS_all.items():
                if key == mark_ects_identify(mark_ects):
                    ECTS_all[key] += 1

            # Пробуем получить его номер зачетки
            rb = RecordBook.objects.none()
            try:
                rb = RecordBook.objects.get(student=mark.user)
            except RecordBook.DoesNotExist:
                rb = ""
            # Получаем его оценку и приводим к необходимому виду
            students.append({
                "full_name": str(mark.user.full_first_small_other()),
                "record_book": str(rb),
                "ticket_number" : -1, # Возвращает всегда -1 т.к. в приложении не фиксируются номера билетов
                "eval_result": {
                    "ECTS": mark_ects,
                    "national": national_mark
                }
            })

        # Получаем данные, для формирования результата
        course = courseID
        specialization = course.specialization_id.specialty_code + " " + "\"" + str(course.specialization_id) + "\""
        res_marks = []
        data = {
            "students": students,
            "statements_header": {
                "edu_period": 0,
                "control_type": docType,
                "specialization": str(specialization),
                "group": str(group.only_course()),
                "course": course.on_course_now(),
                "subject": str(subject),
                "lectors": examiners,
                "event_date": str(event_date.day) + " \"" + month_to_ukr_str(event_date.month, is_inflected=True) + "\" " + str(event_date.year),
            },
            "res_marks": {
                "ECTS_all": ECTS_all,
                "national_all": national_all
            }
        }
        return data
    elif docType == "Offset":
        # Генерируем документ на основе средней оценки
        group = Group.objects.get(id=groupID.id)
        subject = Subject.objects.get(id=subjectID.id)

        # Получаем список студентов, которые обучаются в группе
        students = User.objects.filter(group=group)

        today = datetime.today().date()
        last_date = today
        first_date = today

        last_date = last_date.replace(day = 31, month = 12, year = today.year)
        first_date = first_date.replace(day = 1, month = 9, year = today.year)
        # Сделать выборку всех оценок за определенный семестр
        if semestr_half == 1:
            # Выбираем данные за период с начала 1.09 - 31.12

            # Проверяем, какой сейчас идет семестр обучения
            if today.month < 9:
                # Если сейчас идет второй семестр - собираем данные за прошлый год
                last_date.year -= 1
                first_date.year -= 1

            # Если сейчас идет первый семестр - собираем данные за этот год
            topics = Topic.objects.filter(subject=subject,
                                         linked_group=group,
                                         date_provide__range=(first_date, last_date)).order_by("date_provide")
        else:
            docType = "Залік"
            # Выбираем данные за период с 01.01 - 31.08
            last_date = last_date.replace(day = 31, month = 8, year = today.year)
            first_date = first_date.replace(day = 1, month = 1, year = today.year)

            # Проверяем, какой сейчас идет семестр обучения
            if today.month >= 9:
                # Если сейчас идет второй семестр - собираем данные за прошлый год
                last_date.year -= 1
                first_date.year -= 1

            # Если сейчас идет первый семестр - собираем данные за этот год
            topics = Topic.objects.filter(subject=subject,
                                          linked_group=group,
                                          date_provide__range=(first_date, last_date)).order_by("date_provide")

            # Запрашиваем все оценки, которые относятся к этой теме и к этой группе, соответственно
            marks = Mark.objects.filter(topic__in=topics, subject=subject)
            # Запрашиваем студентов, которые учатся в группе для последующей сортировке данных
            students = User.objects.filter(group=group).order_by("second_name")
            # формируем результирующие данные перебирая студентов и находя среднюю оценку для каждого из них

            # Подсчитываем количество оценок каждого типа
            students_arr = []
            for stud in students:
                # Пробуем получить его номер зачетки
                rb = RecordBook.objects.none()
                try:
                    rb_model = RecordBook.objects.filter(student=stud).order_by("issue_date").first()
                    rd = str(rb_model)
                except RecordBook.DoesNotExist:
                    rb = ""

                # Подсчитываем среднюю оценку пользователя
                users_marks = marks.filter(user=stud)
                avgMark = 0
                for um in users_marks:
                    if um.mark:
                        avgMark += um.mark
                if marks.filter(user=stud).count() != 0:
                    avgMark /= marks.filter(user=stud).count()

                # Ищем, куда прибавить значение в отчете для результирующей оценки
                nationalAVG = mark_national_identify(avgMark)
                if '5' == nationalAVG:
                    national_all["five"] += 1
                elif '4' == nationalAVG:
                    national_all["four"] += 1
                elif '3' == nationalAVG:
                    national_all["three"] += 1
                elif '2' == nationalAVG:
                    national_all["two"] += 1
                else:
                    national_all["one"] += 1
                # Повторяем операцию для стандарта ECTS
                for (key, value) in ECTS_all.items():
                    if key == mark_ects_identify(avgMark):
                        ECTS_all[key] += 1

                # Получаем его оценку и приводим данные к нужному виду
                students_arr.append({
                    "full_name": str(stud.full_first_small_other()),
                    "record_book": rb,
                    "ticket_number" : -1, # Возвращает всегда -1 т.к. в приложении не фиксируются номера билетов
                    "eval_result": {
                        "ECTS": mark_ects_identify(avgMark),
                        "national": mark_national_identify(avgMark, report=True)
                    }
                })

            ''' формируем заголовок отчета '''
            # Получаем курс, для формирования заголовка
            course = courseID
            specialization = course.specialization_id.specialty_code + " " + "\"" + str(course.specialization_id) + "\""
            #TODO: Получить данные о том, за какой период обучения пытаются сформировать отчет

            # Данные к отправке
            data = {
                "statements_header": {
                    "edu_period": 0,
                    "control_type": docType,
                    "specialization": str(specialization),
                    "group": str(group.only_course()),
                    "course": course.on_course_now(),
                    "subject": str(subject),
                    "lectors": examiners,
                    "event_date": str(event_date.day) + " \"" + month_to_ukr_str(event_date.month, is_inflected=True) + "\" " + str(event_date.year),
                },
                "students": students_arr,
                "res_marks": {
                    "ECTS_all": ECTS_all,
                    "national_all": national_all
                }
            }
        return data

def month_to_ukr_str(month_number = 0, is_inflected=False):
    """
    Функция, возвращающая название месяца, по его номеру на украниском языке
    :param month_number:
    :return:
    """
    str = ""
    if month_number == 1:
        str = "Січень"
    elif month_number == 2:
        str =  "Лютий"
    elif month_number == 3:
        str =  "Березень"
    elif month_number == 4:
        str =  "Квітень"
    elif month_number == 5:
        str =  "Травень"
    elif month_number == 6:
        str =  "Червень"
    elif month_number == 7:
        str =  "Липень"
    elif month_number == 8:
        str =  "Серпень"
    elif month_number == 9:
        str =  "Вересень"
    elif month_number == 10:
        str =  "Жовтень"
    elif month_number == 11:
        str =  "Листопад"
    else:
        str =  "Грудень"

    # если нужно просклонять
    if is_inflected:
        if month_number != 2 or month_number != 11:
            str = str[:-3] + "ня"
        elif month_number == 2:
            str = str[:-2] + "ого"
        else:
            str = str + "а"

    return str



def calculate_res_marks(data):
    """
    Подсчитывает количество каждого типа оценок
    :param data:
    :return:
    """

def mark_ects_identify(data):
    """
    Определяет переданную оценку по стандарту ECTS в Украине на моент 07.05.2020
    :param data:
    :return:
    """
    if data:
        m = data
        if m >= 4.5:
            return "A"
        elif 4.5 > m >= 4:
            return "B"
        elif 4 > m >= 3.5:
            return "C"
        elif 3.5 > m >= 3:
            return "D"
        elif 3 > m >= 2.5:
            return "E"
        elif 2.5 > m >= 2:
            return "FX"
        else:
            return "F"
    return "F"

def mark_national_identify(data, report=False):
    """
    Определяет критерий оценивания по национальному стандарту Украины на 06.05.2020
    Возвращает строку в необходимом для отчета виде
    :return:
    """
    if report:
        if data:
            m = data
            if m >= 4.5:
                return "відмінно"
            elif 4.5 > m >= 3.5:
                return "добре"
            elif 3.5 > m >= 2.5:
                return "задовільно"
            elif 2.5 > m >= 2:
                return "незадовільно (з можливістю повторного складання)"
            else:
                return "незадовільно (з обов'язковим повторним вивченням)"
        return "незадовільно (з обов'язковим повторним вивченням)"

    if not report:
        if data:
            m = data
            if m >= 4.5:
                return "5"
            elif 4.5 > m >= 3.5:
                return "4"
            elif 3.5 > m >= 2.5:
                return "3"
            elif 2.5 > m >= 2:
                return "2"
            else:
                return "1"
        return "1"

# Возвращает список предметов которые может преподать преподаватель выбранной группе сейчас
def get_subjects_by_groupID(request):
    user = request.user
    groupID = request.GET.get('groupID')
    group_model = Group.objects.get(id=groupID)

    today = datetime.today().date()
    # Должен выбрать предметы которые читается выбранной группе учитывая то, на каком сейчас они курсе (1, 2, 3 и т.д.)
    course_date_enrollment = group_model.course_id.date_enrollment
    # searchableDateBgn = date(today.year - readable_course, 9, 1)
    # searchableDateBgn = searchableDateBgn.replace(day = 1, month = 9)
    if today.month >= 9 and today.month <=12:
        today = date(today.year + 1, today.month, today.day)

    course_of_study = today.year - course_date_enrollment.year
    # TODO: сделать в будущем отсеивание по семестрам
    # TODO: сделать отсеивание также по специализации
    subjects = Subject.objects.filter(lector=user, course_of_teaching=course_of_study)
    data = []
    for subject in subjects:
        data.append({
            'pk': subject.id,
            'name': subject.name,
        })
    return JsonResponse(data, safe=False)


# Возвращает список групп которые относятся к одному потоку студентов
def get_groups_by_course(request):
    courseID = request.GET.get('courseID')
    groups = list(Group.objects.filter(id=courseID).order_by('date_of_creation'))
    data = []
    for group in groups:
        data.append({
            "pk": group.id,
            "number": group.number
        })
    return JsonResponse(data, safe=False)


# Возвращает группы которые сейчас находится на запрашиваемом курсе (например те кто сейчас на первом курсе, втором и т.д.)
def require_groups_on_course(request):
    required_course = int(request.GET.get('required_course'))
    subject_pk = int(request.GET.get('pk'))

    # Получаем объект предмета для того, чтобы узнать каким факультетам он читается
    specialization = Subject.objects.get(id=subject_pk).specialization

    # Указываем годовой предел искомого диапозона
    today = datetime.today().date()
    last_date = datetime.today().date()
    last_date = last_date.replace(day = 31, month = 12, year = today.year - required_course)
    # проверить какой семестр сейчас идет
    if today.month <= 9 and today.month >=12:
        # в первом семестре уменьшаем поисковый диапазон даты на единицу
        required_course -= 1
    first_date = today.replace(day = 1, month = 1, year = today.year - required_course)
    # во втором случае ищем переданный курс

    groups = Group.objects.filter(course_id__date_enrollment__range=(first_date, last_date),
                                  course_id__specialization_id=specialization).order_by('date_of_creation')
    data = []
    for group in groups:
        data.append({
            "id": group.id,
            "number": str(group)
        })
    return JsonResponse(data, safe=False)

# Запрос студентов от пользователя по группе
def require_students_by_group(request):
    groupID = request.GET.get('groupID', None)
    students = User.objects.filter(group=groupID).order_by('first_name').order_by('second_name')
    data = []
    for st in students:
        data.append({
            "pk": st.id,
            "second_name": st.second_name,
            "first_name": st.first_name,
        })
    return JsonResponse(data, safe=False)


# Запрос сегодняшней даты
def date_today(request):
    data = {
        "date": datetime.now().date()
    }

    return JsonResponse(data, safe=False)

def get_uniq_dates_from_data(data):
    unic_dates = []
    # получаем уникальные даты из даннії
    for q in data[0]:
        unic_dates.append(q.date_added)

    unic_dates.sort()
    return unic_dates


# Запрос посещений конкретного студента конкретных лекций
def require_journal_attendance(request):
    groupID = request.GET.get('groupID', None)
    subjectID = request.GET.get('subjectID', None)
    students = list(User.objects.filter(group__id=groupID).order_by('first_name').order_by('second_name'))
    attandances = []
    for stud in students:
        attandances.append(list(Attendance.objects.filter(subject__id=subjectID).filter(student__id=stud.id).order_by('date_added')))

    data=[]
    for attandance in attandances:
        datablock = []
        for a in attandance:
            datablock.append({
                "id": a.id,
                "date_added": a.date_added,
                "presence": a.presence,
            })
        data.append(datablock)
    data.append({
        "unic_dates": get_uniq_dates_from_data(attandances)
    })

    # записать запрос пользователя
    user = request.user
    create_record_require_journal(user, subjectID, groupID)
    return JsonResponse(data, safe=False)


# создает или обновленяет запись о последнем запросе журнала
def create_record_require_journal(user, subjectID, groupID):
    group_model = Group.objects.get(id=groupID)
    course_model = Course.objects.get(id=group_model.course_id.id)
    subject_model = Subject.objects.get(id=subjectID)
    try:
        last_required_journal = RequiredJournal.objects.get(user=user)
        # обновляем данные о последнем запросе пользователя
        last_required_journal.group = group_model
        last_required_journal.course = course_model
        last_required_journal.subject = subject_model
        last_required_journal.date_request = now()
        last_required_journal.save()
    except RequiredJournal.DoesNotExist:
        new_required_journal = RequiredJournal.objects.create(
            user=user,
            group=group_model,
            course=course_model,
            subject=subject_model,
            date_request=now()
        )


# Запрос оценок для журнала оценок
def require_journal_marks(request):
    groupID = request.GET.get('groupID', None)
    subjectID = request.GET.get('subjectID', None)

    students_model = User.objects.filter(group__id=groupID).order_by('first_name').order_by('second_name')
    # Запрашиваем темы упорядоченные по дате добавления
    students = [] # студенты и их оценки
    for stud in students_model:
        marks = []
        marks_model = Mark.objects.filter(user=stud, subject__id=subjectID).order_by('topic__date_added')
        for mark in marks_model:
            marks.append({
                "markID": mark.id,
                "topic": mark.topic.title,
                "mark": mark.mark
            })
        students.append({
            'stud_id': stud.id,
            'first_name': stud.first_name,
            'second_name': stud.second_name,
            'marks': marks
        })

    # список тем которые прошли
    topics = []
    topics_model = Topic.objects.filter(subject__id=subjectID, linked_group__id=groupID, is_performed=True).order_by('date_provide')
    for topic in topics_model:
        topics.append({
            'topicID': topic.id,
            'title': topic.title,
            'control_type': topic.control_type.title,
            'date_provide': topic.date_provide
        })
    data = {
        'students': students,
        'topics': topics
    }

    return JsonResponse(data, safe=False)


# Возвращает данные о журнале который в последний раз запрашивали
def get_last_required_journal(request):
    user = request.user
    try:
        last_required_journal = RequiredJournal.objects.get(user=user)
        data = {
            'courseID': last_required_journal.course.id,
            'groupID': last_required_journal.group.id,
            'subjectID': last_required_journal.subject.id
        }
        return JsonResponse(data, status=200)
    except RequiredJournal.DoesNotExist:
        return JsonResponse(data={}, status=500)


# Запрашивает последнюю добавленную работу которую проводили группе
def require_last_mark_students_in_journalList(request):
    groupID = request.GET.get('groupID', None)
    subjectID = request.GET.get('subjectID', None)

    students_model = User.objects.filter(group__id=groupID).order_by('first_name').order_by('second_name')
    # Запрашиваем темы упорядоченные по дате добавления
    students = [] # студенты и их оценки
    for stud in students_model:
        marks = []
        marks_model = Mark.objects.filter(user=stud, subject__id=subjectID).order_by('-topic__date_added').first()
        marks = {
            "markID": marks_model.id,
            "topic": marks_model.topic.title,
            "mark": marks_model.mark
        }
        students.append({
            'stud_id': stud.id,
            'first_name': stud.first_name,
            'second_name': stud.second_name,
            'marks': marks
        })

    # список тем которые прошли
    topics_model = Topic.objects.filter(subject__id=subjectID, linked_group__id=groupID, is_performed=True).order_by('-date_provide').first()
    topics = {
        'topicID': topics_model.id,
        'title': topics_model.title,
        'control_type': topics_model.control_type.title,
        'date_provide': topics_model.date_provide
    }
    data = {
        'students': students,
        'topics': topics
    }

    return JsonResponse(data, safe=False)


# Возвращает последнюю проведенную работу для студентов
def require_last_performed_topic_for_group(request):
    groupID = request.GET.get('groupID')
    subjectID = request.GET.get('subjectID')
    user = request.user
    topic_model = Topic.objects.filter(subject__id=subjectID, linked_group__id=groupID, is_performed=True, added_user=user).order_by('-date_provide').first()
    data = {
        "topicID": topic_model.id,
        "title": topic_model.title,
        'control_type': topic_model.control_type.title,
        'date_provide': topic_model.date_provide
    }
    return JsonResponse(data)


# Добавляет новую запись лекции
def new_lection(request):
    groupID = request.GET.get('groupID', None)
    subjectID = request.GET.get('subjectID', None)
    if (request.user.is_active):
        # TODO: сделать проверку на то что это преподаватель
        newSubject = Subject()


# Меняет данные студента о посещении лекции во время того как их отмечают
def update_attendance_for_student(request):
    #TODO: Реализовать метод который будет менять посещение студента в конкретный день по конкретному предмету
    pass


# отправлять на обработку строку в формате JSON
def get_students_list_of_dict(students_list):
    students = []
    # парсим строку json стандартным питоновским методом
    for student in students_list:
        students.append(json.loads(student))
    # Возвращает список студентов в виде словаря
    return students

# Проверяет не было ли превышено максимальное кол-во пар по одному предмету которые может провести преподаватель
def can_create_attendance(subjectID, groupID, user):
    subject_model = Subject.objects.get(id = subjectID)
    group_model = Group.objects.get(id = groupID)
    today_min = datetime.combine(date.today(), time.min)
    today_max = datetime.combine(date.today(), time.max)
    count_attendances = Attendance.objects.filter(subject=subject_model, student__group=group_model, date_added__range=(today_min, today_max)).count()

    count_students = User.objects.filter(group=group_model).count()
    # Считаем какое кол-во пар было проведено
    if count_attendances / count_students != 7:
        return True
    return False

# Добавить посещения
def create_new_attendances(request):
    groupID = request.POST.get('groupID')
    subjectID = request.POST.get('subjectID')
    user = request.user
    temp_list_students = request.POST.getlist('students[]')
    students_dict = get_students_list_of_dict(temp_list_students)

    if can_create_attendance(subjectID, groupID, user):
        students = []
        # регистрируем новую лекцию на студента
        date = now()
        for student_dict in students_dict:
            student_pk = student_dict.get('pk')

            student_model = User.objects.get(pk=student_pk)
            subject_model = Subject.objects.get(pk=subjectID)

            newAttendance = Attendance.objects.create(student=student_model, subject=subject_model, date_added=date)
            newAttendance.save()
        return JsonResponse(data={}, status=200)
    # Отправить сообщение о превышенном кол-ве добавленных пар
    return JsonResponse(data={}, status=500)

# Создает новую запись для студента если нет оценки которую можно изменить
def create_new_mark():
    pass

# Добавить оценку для студента
def create_new_mark_for_student(request):
    #TODO: реализовать добавление оценки конкретному студенту по конкретному предмету в конкретную дату
    pass


# Изменение оценки студента по конкретному предмету в конкретный день
def change_student_mark(request):
    student = json.loads(request.POST.get('student'))
    subjectID = request.POST.get('subjectID')
    mark = request.POST.get('mark')
    topicID = request.POST.get('topicID')

    # Пробуем получить тему по которой пытаются изменить оценку
    topic_model = None
    try:
        topic_model = Topic.objects.get(id=topicID)
    except Topic.DoesNotExist:
        return HttpResponse(500)

    # Проверяем есть ли у студента оценка по переданной теме по переданному предмету
    try:
        mark_model = Mark.objects.get(user__id=student.get('pk'),
                                      subject__id=subjectID,
                                      topic=topic_model)
        mark_model.mark = mark
        mark_model.save()
        return HttpResponse(200)
    except Mark.DoesNotExist: # Если запись не найдена - создаем новую запись
        student_model = User.objects.get(pk=int(student.get('pk')))
        subject_model = Subject.objects.get(id=subjectID)
        student_mark_model = Mark.objects.create(user=student_model,
                            subject=subject_model,
                            mark=mark,
                            topic=topic_model)
        return HttpResponse(200)
    return HttpResponse(500)


# Удаление оценок при работе с журналом преподавателя
def delete_student_mark(request):
    student = json.loads(request.POST.get('student'))
    subjectID = request.POST.get('subjectID')
    mark = request.POST.get('mark')
    topicID = request.POST.get('topicID')

    # Пробуем получить тему по которой пытаются изменить оценку
    topic_model = None
    try:
        topic_model = Topic.objects.get(id=topicID)
    except Topic.DoesNotExist:
        return HttpResponse(500)

    try:
        student_model = User.objects.get(id=student.get('pk'))
        subject_model = Subject.objects.get(id=subjectID)
        student_mark_model = Mark.objects.get(user=student_model,
                          subject=subject_model,
                          topic=topic_model)
        student_mark_model.mark=None
        student_mark_model.save()
        return HttpResponse(200)
    except ProtectedError: # Если запись не найдена - создаем новую запись
        error_message = "Оценка, студент или предмет не был найден"
        return HttpResponse(500)


# Отменяет проведение последней работы проведенное преподавателем по лекции
def cancel_last_test_work(request):
    subjectID = request.POST.get('subjectID')
    groupID = request.POST.get('groupID')
    user = request.user
    today = datetime.today().date()
    subject_model = Subject.objects.get(id=subjectID)
    group_model = Group.objects.get(id=groupID)

    # Получить лекцию с последней проведенной работой
    last_topic_model = Topic.objects.filter(linked_group=group_model, is_performed=True, added_user=user).order_by('-date_provide').first()

    # Проверить не является ли последняя проведенная тема особой работой (ДКР, Аттестация, Зачет, Экзамен)
    control_type_title = last_topic_model.control_type.title
    if control_type_title == "Атестація" or control_type_title == 'ДКР' or control_type_title == 'Залік' or control_type_title == 'Екзамен':
        # удаляем зависимые оценки
        marks = Mark.objects.filter(subject=subject_model, topic=last_topic_model)
        # проверить есть ли где-то проставленные оценки
        for mark in marks:
            if mark.mark != None:
                return JsonResponse(data={}, status=500) # запрещаем удаление
        # оценки нигде не проставлены - удалить все записи оценок
        for mark in marks:
            mark.delete()
        last_topic_model.linked_group = None
        last_topic_model.control_type = None
        last_topic_model.save()
        Topic.objects.get(id=last_topic_model.id).delete()
    else:
        # проверить совпадает ли сегодняшняя дата с датой последней проведенной работы
        if last_topic_model.date_provide.date() == today:
            marks = Mark.objects.filter(subject=subject_model, topic=last_topic_model)
            # проверить есть ли где-то проставленные оценки
            for mark in marks:
                if mark.mark != None:
                    return JsonResponse(data={}, status=500) # запрещаем удаление
            # оценки нигде не проставлены - удалить все записи оценок
            for mark in marks:
                mark.delete()
            # Проверить плановая ли работа
            if last_topic_model.is_scheduled == False:
                last_topic_model.delete()
            else:
                # перевести значение темы в состояние не проведено, убрать дату проведения
                last_topic_model.is_performed = False
                last_topic_model.date_provide = None
                last_topic_model.save()
        else:
            return JsonResponse(data={}, status=500) # что-то странное произошло
    return JsonResponse(data={}, status=200)


# Изменить посещение студента лекции в соответсвии с переданным состоянием
def change_attendance_status(request):
    student = request.POST.get('student')
    student = json.loads(student)
    subjectID = request.POST.get('subjectID')
    presence = json.loads(request.POST.get('presence'))
    # или по ID посещения
    attendanceID = request.POST.get('attendanceID')

    # проверить есть ли уже запись
    date = now()
    try:
        attendance_model = Attendance.objects.get(id=attendanceID,
                                                  student__id=student.get('pk'),
                                                  subject__id=subjectID)
        attendance_model.presence = presence
        attendance_model.save()
        return HttpResponse(200)
    except User.DoesNotExist:
        user_model = User.objects.get(student__id=student.get('pk'))
        subject_model = Subject.objects.get(id=subjectID)
        attendance_model = Attendance.objects.create(student=user_model, subject=subject_model, presence=presence, date_added=date)
        return HttpResponse(200)

    return HttpResponse(500)


# Удаляет проведение последней лекции по предмету
def delete_last_attendances(request):
    subjectID = request.POST.get('subjectID')
    groupID = request.POST.get('groupID')
    user = request.user
    today = datetime.today().date()
    subject_model = Subject.objects.get(id=subjectID)
    group_model = Group.objects.get(id=groupID)
    # Получить список студентов
    students = User.objects.filter(group=group_model)

    attendances = []
    # Для каждого студента запросить его последнее посещение переданного занятия
    for student in students:
        attendances.append(Attendance.objects.filter(student=student, subject=subject_model).order_by('-date_added').first())

    # Посмотреть все ли посещения были проставлены сегодня
    # Посмотреть нет ли отметок "Отсутствия кого-либо на лекциях"
    for attendance in attendances:
        if attendance.date_added.date() == today and attendance.presence != False:
            continue
        else:
            return JsonResponse(data={}, status=500)

    # Если все подходит - удаляем посещения
    for attendance in attendances:
        attendance.delete()
    return JsonResponse(data={}, status=200)


# Добавляет новые оценки группе
def create_new_marks(request):
    user = request.user
    groupID = request.POST.get('groupID')
    subjectID = request.POST.get('subjectID')
    topicID = request.POST.get('topicID')

    # получаем студентов
    students_model = None
    try:
        students_model = User.objects.filter(group__id=groupID).order_by('first_name').order_by('second_name')
    except User.DoesNotExist:
        return HttpResponse(500)

    # пробуем найти тему к которой привязывают их новые оценки
    topic_model = None
    try:
        # Сделать проверку владелец ли предмета пытается провести тему
        topic_model = Topic.objects.get(id=topicID, added_user=user)
    except Topic.DoesNotExist:
        return HttpResponse(500)

    # пробуем достать предмет
    subject_model = None
    try:
        subject_model = Subject.objects.filter(id=topicID)
    except Topic.DoesNotExist:
        return HttpResponse(500)

    date = now()
    # создаем для студентов новые оценки и инициализиурем их при помощи null
    for stud in students_model:
        # Проверяем не была ли уже созданная для них оценка с этой темой
        try:
            Mark.objects.get(user=stud, subject=subject_model, topic=topic_model)
        except Mark.DoesNotExist:
            new_mark = Mark.objects.create(user=stud, subject=subject_model, date_added=date, mark=None, topic=topic_model)
            new_mark.save()

    return HttpResponse(status=200)


# Получить список оценок по студенту и предмету
def get_list_marks_by_student_and_subject(student, subject):
    marks = list(Mark.objects.filter(user=student, subject=subject).order_by('date_added'))
    
    # запрашиваем для оценок данные о теме, за которую они были поставлены

    data = []
    # формируем оценки пользователя
    for mark in marks:
        topic_title = mark.topic.title
        if not (topic_title == "Атестація" or topic_title == "Залік" or topic_title == "Екзамен"):
            data.append({
                'mark': mark.mark,
                'date_added': mark.date_added,
                'topic': mark.topic.title,
                'control_type': mark.topic.control_type.title
            })
    return data


# Получения списка студентов
def get_subjects_for_student(request):
    user = request.user
    student_specializationID = user.group.course_id.id
    subjects_list = list(Subject.objects.filter(specialization=student_specializationID))
    data = []
    for subject in subjects_list:
        data.append({
            'subject': {
                'id': subject.id,
                'name': subject.name
            },
            'marks': get_list_marks_by_student_and_subject(user, subject)
        })

    # по его группе узнать какая у него специализация

    # по специализации получить предметы
    # взять все предметы которые когда-либо посещал студент
    # Получить по взятым предметам его оценки
    return JsonResponse(data, safe=False)


# Возвращает список предметов которые ведет преподаватель
def require_teachers_subjects(request):
    user = request.user
    user_model = User.objects.get(id = user.id)
    subjects = Subject.objects.filter(lector=user_model).order_by('semestr_academic')
    data = []
    for subject in subjects:
        data.append({
            "pk": subject.id,
            "name": subject.name,
            "semestr_academic": subject.semestr_academic,
            "course_of_teaching": subject.course_of_teaching,
            "specialization": subject.specialization.first_letters()
        })
    return JsonResponse(data, safe=False)


# Возвращает запланированные проверочные работы для конструктора журналов
def require_tickets(request):
    user = request.user
    subjectID = request.GET.get('subjectID')
    groupID = request.GET.get('groupID')
    data = []

    topics = list(Topic.objects.filter(added_user=user,
                                       subject__id=subjectID,
                                       linked_group__id=groupID,
                                       is_scheduled=True,
                                       is_performed=False))
    for topic in topics:
        data.append({
            "topic_pk": topic.id,
            "title": topic.title,
            "description": topic.description,
            "control_type": topic.control_type.title
        })

    return JsonResponse(data, safe=False)


# создает для студентов пустые оценки когда им дается тема
def create_marks_for_test(request):
    user = request.user
    groupID = request.POST.get('groupID')
    subjectID = request.POST.get('subjectID')
    topicID = request.POST.get('topicID')

    # получаем студентов
    students_model = None
    try:
        students_model = User.objects.filter(group__id=groupID).order_by('first_name').order_by('second_name')
    except User.DoesNotExist:
        return False

    # пробуем найти тему к которой привязывают их новые оценки
    topic_model = None
    try:
        # Сделать проверку владелец ли предмета пытается провести тему
        topic_model = Topic.objects.get(id=topicID, added_user=user)
    except Topic.DoesNotExist:
        return False

    # пробуем достать предмет
    subject_model = None
    try:
        subject_model = Subject.objects.get(id=subjectID)
    except Subject.DoesNotExist:
        return False

    date = now()
    # создаем для студентов новые оценки и инициализиурем их при помощи null
    for stud in students_model:
        # Проверяем не была ли уже созданная для них оценка с этой темой
        try:
            mark = Mark.objects.get(user__id=stud.id, subject=subject_model, topic=topic_model)
            if not mark:
                new_mark = Mark.objects.create(user=stud, subject=subject_model, date_added=date, mark=None, topic=topic_model)
                new_mark.save()
        except Mark.DoesNotExist:
            new_mark = Mark.objects.create(user=stud, subject=subject_model, date_added=date, mark=None, topic=topic_model)
            new_mark.save()
    return True


# фиксирование факта проведения проверочной работы у студентов
def give_a_test(request):
    user = request.user
    subjectID = request.POST.get('subjectID')
    groupID = request.POST.get('groupID')
    topicID = request.POST.get('topicID')

    # Найти сам тест
    # дать ему статус "Проведено"
    topic_model = None
    if create_marks_for_test(request):
        try:
            topic_model = Topic.objects.get(id=topicID)
        except Topic.DoesNotExist:
            return HttpResponse(500)

        topic_model.date_provide = now()
        topic_model.is_performed = True
        topic_model.save()

        return HttpResponse(200)
    else:
        HttpResponse(500)


def give_directorial_review(request):
    """ Проводит директорскую работу """
    user = request.user
    subjectID = request.POST.get('subjectID')
    groupID = request.POST.get('groupID')
    topicID = request.POST.get('topicID')

    topic_model = Topic.objects.none()
    try:
        control_type = Control_Type.objects.filter(title="ДКР").first()
        if not control_type:
            control_type = Control_Type.objects.create()
            control_type.title = "ДКР"
            control_type.save()
    except Control_Type.DoesNotExist:
        control_type = Control_Type.objects.create()
        control_type.title = "ДКР"
        control_type.save()

    # проверяем проводились ли для этой группе ДКР
    topics = Topic.objects.none()
    try:
        topics = Topic.objects.filter(title="ДКР", linked_group__id=groupID).order_by('-date_added')
    except Topic.DoesNotExist:
        topics = Topic.objects.none()

    if len(topics) < 2:
        topic_new = Topic.objects.create(control_type=control_type,
                                         subject=Subject.objects.get(id=subjectID),
                                         title="ДКР",
                                         added_user=user,
                                         is_performed=True,
                                         linked_group=Group.objects.get(id=groupID))
        topic_new.save()

        if create_marks_for_students(request, topic_new):
            return JsonResponse(data={}, status=200)

    return JsonResponse(data={}, status=500)


def create_marks_for_students(request, topic):
    user = request.user
    groupID = request.POST.get('groupID')
    subjectID = request.POST.get('subjectID')

    # получаем студентов
    students_model = None
    try:
        students_model = User.objects.filter(group__id=groupID).order_by('first_name').order_by('second_name')
    except User.DoesNotExist:
        return False

    # пробуем достать предмет
    subject_model = None
    try:
        subject_model = Subject.objects.get(id=subjectID)
    except Subject.DoesNotExist:
        return False

    date = now()
    # создаем для студентов новые оценки и инициализиурем их при помощи null
    for stud in students_model:
        # Проверяем не была ли уже созданная для них оценка с этой темой
        try:
            mark = Mark.objects.get(user__id=stud.id, subject=subject_model, topic=topic)
            if not mark:
                new_mark = Mark.objects.create(user=stud, subject=subject_model, date_added=date, mark=None, topic=topic)
                new_mark.save()
        except Mark.DoesNotExist:
            new_mark = Mark.objects.create(user=stud, subject=subject_model, date_added=date, mark=None, topic=topic)
            new_mark.save()
    return True


def give_certify_by_subject(request):
    """ Проводит аттестацию для группы по предмету """
    user = request.user
    subjectID = request.POST.get('subjectID')
    groupID = request.POST.get('groupID')

    topic_model = Topic.objects.none()
    try:
        control_type = Control_Type.objects.filter(title="Атестація").first()
        if not control_type:
            control_type = Control_Type.objects.create()
            control_type.title = "Атестація"
            control_type.save()
    except Control_Type.DoesNotExist:
        control_type = Control_Type.objects.create()
        control_type.title = "Атестація"
        control_type.save()

    # проверяем проводились ли для этой группы Атестація
    topics = Topic.objects.none()
    try:
        topics = Topic.objects.filter(title="Атестація", linked_group__id=groupID).order_by('-date_added')
    except Topic.DoesNotExist:
        topics = Topic.objects.none()

    if len(topics) < 3:
        topic_new = Topic.objects.create(control_type=control_type,
                                         subject=Subject.objects.get(id=subjectID),
                                         title="Атестація",
                                         added_user=user,
                                         is_performed=True,
                                         linked_group=Group.objects.get(id=groupID))
        topic_new.save()

        if create_marks_for_students(request, topic_new):
            return JsonResponse(data={}, status=200)

    return JsonResponse(data={}, status=500)


def give_credit_by_subject(request):
    """ Проводит зачет для группы по предмету """
    user = request.user
    subjectID = request.POST.get('subjectID')
    groupID = request.POST.get('groupID')
    topic_model = Topic.objects.none()
    try:
        control_type = Control_Type.objects.filter(title="Залік").first()
        if not control_type:
            control_type = Control_Type.objects.create()
            control_type.title = "Залік"
            control_type.save()
    except Control_Type.DoesNotExist:
        control_type = Control_Type.objects.create()
        control_type.title = "Залік"
        control_type.save()

    # проверяем проводились ли для этой группы Атестація
    topics = Topic.objects.none()
    try:
        topics = Topic.objects.filter(title="Залік", linked_group__id=groupID).order_by('-date_added')
    except Topic.DoesNotExist:
        topics = Topic.objects.none()

    topic_new = Topic.objects.create(control_type=control_type,
                                     subject=Subject.objects.get(id=subjectID),
                                     title="Залік",
                                     added_user=user,
                                     is_performed=True,
                                     linked_group=Group.objects.get(id=groupID))
    topic_new.save()

    if create_marks_for_students(request, topic_new):
        return JsonResponse(data={}, status=200)
    return JsonResponse(data={}, status=500)


def give_exam_by_subject(request):
    """ Проводит зачет для группы по предмету """
    user = request.user
    subjectID = request.POST.get('subjectID')
    groupID = request.POST.get('groupID')
    topic_model = Topic.objects.none()
    try:
        control_type = Control_Type.objects.filter(title="Екзамен").first()
        if not control_type:
            control_type = Control_Type.objects.create()
            control_type.title = "Екзамен"
            control_type.save()
    except Control_Type.DoesNotExist:
        control_type = Control_Type.objects.create()
        control_type.title = "Екзамен"
        control_type.save()

    # проверяем проводились ли для этой группы Атестація
    topics = Topic.objects.none()
    try:
        topics = Topic.objects.filter(title="Екзамен", linked_group__id=groupID).order_by('-date_added')
    except Topic.DoesNotExist:
        topics = Topic.objects.none()

    topic_new = Topic.objects.create(control_type=control_type,
                                     subject=Subject.objects.get(id=subjectID),
                                     title="Екзамен",
                                     added_user=user,
                                     is_performed=True,
                                     linked_group=Group.objects.get(id=groupID))
    topic_new.save()

    if create_marks_for_students(request, topic_new):
        return JsonResponse(data={}, status=200)
    return JsonResponse(data={}, status=500)


# создает новую запланированную работу для журнала (место куда поставится оценка)
def create_new_ticket(request):
    user = request.user
    subjectID = request.POST.get('subjectID')
    groupID = request.POST.get('groupID')
    is_scheduled = request.POST.get('isScheduled')

    title = request.POST.get('title')
    description = request.POST.get('description')
    control_type = request.POST.get('controlType')
    # попробовать получить controlType, если его нет - создать, если есть - создавать объект как есть
    try:
        control_type_model = Control_Type.objects.get(title=control_type)
    except Control_Type.DoesNotExist: # Если запись не найдена - создаем новую запись
        newControlType = Control_Type.objects.create(title=control_type)
        newControlType.save()

    control_type_model = Control_Type.objects.get(title=control_type)

    # пробуем получить ссылку на предмет
    subject_model = None
    linked_group_model = None
    control_type_model = None
    try:
        subject_model = Subject.objects.get(id=subjectID)
        # пробуем получить ссылку на группу
        try:
            linked_group_model = Group.objects.get(id=groupID)
            # пробуем получить ссылку на контрольный тип
            try:
                control_type_model = Control_Type.objects.get(title=control_type)
            except Control_Type.DoesNotExist:
                return HttpResponse(500)
        except Group.DoesNotExist:
            return HttpResponse(500)
    except Subject.DoesNotExist:
        return HttpResponse(500)

    new_topic = None
    if is_scheduled.upper() == "TRUE":
        # Создать новое запланированное событие
        Topic.objects.create(added_user=user,
                             subject=subject_model,
                             linked_group=linked_group_model,
                             description=description,
                             control_type=control_type_model,
                             title=title)
    else:
        # Создать новое не запланированное событие
        Topic.objects.create(added_user=user,
                             subject=subject_model,
                             linked_group=linked_group_model,
                             description=description,
                             control_type=control_type_model,
                             title=title,
                             is_scheduled=False,
                             is_performed=True)
    # return JsonResponse(serializers.serialize(new_topic), safe=False)
    return HttpResponse(200)


# Удаление запланированного события из базы данных для пользователя
def delete_ticket(request):
    user = request.user
    topic_id = request.POST.get('topic_pk')

     # пробуем получить ссылку на предмет
    try:
        Topic.objects.get(id=topic_id).delete()
    except Topic.DoesNotExist:
         return HttpResponse(500)
    return HttpResponse(200)
