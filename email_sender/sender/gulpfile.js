let gulp = require('gulp'),
    sass = require('gulp-sass'),
    inky = require('inky'),
    inlineCss = require('gulp-inline-css'),
    inlinesource = require('gulp-inline-source');


//STYLES
gulp.task('styles', function () {
  return gulp.src('./scss/styles.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./dist'));
});

//CONVERT INKY
gulp.task('inky', gulp.series('styles', function() {
    return gulp.src('./templates/**/*.html')
        .pipe(inlinesource({
            preserveMediaQueries: true,
            removeLinkTags: false
        }))
        .pipe(inky())
        .pipe(inlineCss({
            preserveMediaQueries: true,
            removeLinkTags: false
        }))
        .pipe(gulp.dest('./dist'));
}));

//INLINE CSS
gulp.task('inline', function () {
  return gulp.src('./dist/*.html')
        .pipe(inlineCss())
        .pipe(gulp.dest('./dist/inlined'));
});


//WATCH
gulp.task('default', function() {
    gulp.watch('./scss/**/*.scss', gulp.parallel('styles'));
    gulp.watch('./templates/**/*.html', gulp.parallel('inky'));
});